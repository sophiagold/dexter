#!/bin/bash
# chmod u+x compile.sh

# last compiled with
# ligo --version
# Commit SHA: 568813a536c4f71a0607174590683a2dda6d5d08
# Commit Date: 2020-09-26 19:33:57 +0000

# this script depends on:
# - ligo
# - dos2unix

ligo compile-contract ./dexter.ligo main | dos2unix > contracts/dexter.tz
ligo compile-contract --michelson-format=json ./dexter.ligo main | dos2unix > contracts/dexter.json

ligo compile-contract ./attack.ligo main | dos2unix > contracts/attack.tz
