// Dexter
// a decentralized Tezos exchange for XTZ and FA1.2
// copyright: camlCase 2019-2020
// version: 0.13.0.0
// license: GPLv3
// description: This file is not considered the official version of Dexter.
// It is a just a prototype for testnet development and exploring new ideas
// quickly. The tested version which includes proofs and has been audited by
// a security team is located at '../morley' and '../morley/dexter.tz'.

// =============================================================================
// Entrypoints
// =============================================================================

type entrypoint is
| Approve         of (address * (nat * nat))
| AddLiquidity    of (address * nat * nat * timestamp)
| RemoveLiquidity of ((address * (address * nat)) * (tez * (nat * timestamp)))
| XtzToToken      of (address * (nat * timestamp))
| TokenToXtz      of ((address * address) * (nat * (tez * timestamp)))
| SetBaker        of (option(key_hash) * bool)
| SetManager      of address
| Default         of unit
| UpdateTokenPool of key_hash
| UpdateTokenPoolInternal of nat
// TokenToToken is not included because the gas costs to call it are greater
// than what is available on Carthagenet. It is possible to originate this
// contract on Carthagenet with TokenToToken, but this entrypoint will remain
// uncallable until gas costs are reduced or gas limits are increased.
// | TokenToToken    of ((address * (nat * address)) * (address * (nat * timestamp)))

// the transfer entrypoint of the FA1.2 contract
type token_contract_transfer is (address * (address * nat));
type get_balance is (address * contract(nat))

// =============================================================================
// Storage
// =============================================================================

type baker_address is key_hash;

type account is record
  balance   : nat;
  allowances: map(address, nat);
end

// this is just to force big_maps as the first item of a pair on the top
// so we can still use the old big map route without big map id for
// convenience
type s is record
  self_is_updating_token_pool: bool;
  freeze_baker: bool;
  lqt_total: nat;
  manager: address;
  token_address: address;
  token_pool: nat;
  xtz_pool: tez;
end

type storage is record
  s: s;
  accounts: big_map(address, account);
end

// =============================================================================
// Constants
// =============================================================================

const empty_allowances : map(address,nat) = map end;

const empty_ops : list(operation) = list end;

// =============================================================================
// Type Synonyms
// =============================================================================

type return is (list(operation) * storage);

// =============================================================================
// Functions
// =============================================================================

function mutez_to_natural(const a: tez): nat is
  block {skip} with a / 1mutez

function natural_to_mutez(const a: nat): tez is
  block {skip} with a * 1mutez

// get an account from the big_map, if one does not exist for a particular
// address then create one.
function get_account(const a: address ; const m: big_map(address, account)): account is
  block { skip } with (
    case (m[a]) of
      | None          -> record balance = 0n; allowances = empty_allowances; end
      | Some(account) -> account
    end
  );

function ceildiv(const numerator: nat ; const denominator: nat): nat is
  block {
    var result : nat := 0n;
    const o_ediv_result : option((nat * nat)) = ediv(numerator,denominator);
    case (o_ediv_result) of
      | None          -> failwith("DIV by 0")
      | Some(ediv_result) -> block {
          if (ediv_result.1 = 0n) then {
            result := ediv_result.0;
          } else {
            result := ediv_result.0 + 1n;
          }
        }
    end;
  } with (
    result
  );

// =============================================================================
// Entrypoint Functions
// =============================================================================

function approve(const spender  : address;
                 const allowance: nat;
                 const current_allowance: nat;
                 var storage      : storage):
                 return is
  block {
    if (storage.s.self_is_updating_token_pool) then {
      failwith("self_is_updating_token_pool must be false")
    } else skip;

    if (amount > 0mutez) then {
      failwith("amount must be zero.");
    } else skip;
    
    // get the sender's account
    // if the account does not exist, fail, we do not want to create accounts here
    // creating accounts should be done in add_liquidity
    const account: account = get_account(sender, storage.accounts);

    var sender_allowances: map(address, nat) := account.allowances;
    var spenders_allowance: nat := 0n; 

    case sender_allowances[spender] of
    | Some(allowance) -> {
        spenders_allowance := allowance
      }
    | None -> skip
    end;

    if (current_allowance = spenders_allowance) then {
      sender_allowances[spender] := allowance;
      storage.accounts[sender]  := record balance = account.balance; allowances = sender_allowances; end;
    } else {
      failwith("current_allowance must equal account.allowances[spender].")
    };
  } with (empty_ops, storage);

// it is assumed that the exchange contract has permission from the FA1.2 token
// to manage the assets of the user. It is the responsibility of the dApp
// developer to handle permissions.
function add_liquidity(const owner               : address;
                       const min_lqt_minted      : nat;
                       const max_tokens_deposited: nat;
                       const deadline            : timestamp;
                       var   storage             : storage):
                       return is
  block {
    // add_liquidity performs a transfer to the token contract, we need to
    // return the operations
    var op_list: list(operation) := nil;

    if (storage.s.self_is_updating_token_pool) then {
      failwith("self_is_updating_token_pool must be false")
    } else skip;

    if (now < deadline) then skip else {
      failwith("The current time must be less than the deadline.");
    };

    if (max_tokens_deposited > 0n) then skip else {
      failwith("max_tokens_deposited must be greater than zero.");
    };   

    if (amount > 0mutez) then skip else {
      failwith("The amount of XTZ sent to the contract to be greater than zero.");
    };

    if (min_lqt_minted > 0n) then skip else {
      failwith("min_lqt_minted must be greater than zero.");
    };
    
    if (storage.s.lqt_total > 0n) then {
      // lqt_total greater than zero
      // use the existing exchange rate

      const xtz_pool             : nat = mutez_to_natural(storage.s.xtz_pool);
      const nat_amount           : nat = mutez_to_natural(amount);

      const tokens_deposited : nat = ceildiv(nat_amount * storage.s.token_pool, xtz_pool);
      
      if (tokens_deposited > 0n) then skip else {
        failwith("tokens_deposited must be greater than zero.");
      };

      if (max_tokens_deposited >= tokens_deposited) then skip else {
        failwith("max_tokens_deposited must be greater than or equal to tokens_deposited.");
      };

      const lqt_minted           : nat = nat_amount * storage.s.lqt_total  / xtz_pool;
      
      if (lqt_minted >= min_lqt_minted) then skip else {
        failwith("lqt_minted must be greater than min_lqt_minted.");
      };
      
      const account: account   = get_account(owner, storage.accounts);
      const new_balance: nat   = account.balance + lqt_minted;
      storage.accounts[owner] := record balance = new_balance; allowances = account.allowances; end;
      storage.s.lqt_total     := storage.s.lqt_total + lqt_minted;
      storage.s.token_pool    := storage.s.token_pool + tokens_deposited;
      storage.s.xtz_pool      := storage.s.xtz_pool + amount;

      // send FA1.2 from owner to exchange
      const token_contract: contract(token_contract_transfer) = get_entrypoint("%transfer", storage.s.token_address);
      const op1: operation = transaction((owner, (self_address, tokens_deposited)), 0mutez, token_contract);
      op_list := list op1; end;

    } else {
      // initial add liquidity
      if (amount >= 1tz) then skip else {
        failwith("The initial liquidity amount must be greater than or equal to zero.");
      };

      const tokens_deposited     : nat = max_tokens_deposited;
      const initial_liquidity    : nat = mutez_to_natural(storage.s.xtz_pool + amount);

      storage.s.lqt_total     := initial_liquidity;
      storage.accounts[owner] := record balance = initial_liquidity; allowances = empty_allowances; end;
      storage.s.token_pool    := tokens_deposited;
      storage.s.xtz_pool      := storage.s.xtz_pool + amount;

      // send FA1.2 tokens from owner to exchange
      const token_contract: contract(token_contract_transfer) = get_entrypoint("%transfer", storage.s.token_address);
      const op1: operation = transaction((owner, (self_address, tokens_deposited)), 0mutez, token_contract);
      op_list := list op1; end;
    }
  } with (op_list, storage);

function remove_liquidity(const owner                : address;
                          const to_                  : address;
                          const lqt_burned           : nat;
                          const min_xtz_withdrawn    : tez;
                          const min_tokens_withdrawn : nat;
                          const deadline             : timestamp;
                          var   storage              : storage):
                          return is
  block {
    var op_list: list(operation) := nil;

    if (storage.s.self_is_updating_token_pool) then {
      failwith("self_is_updating_token_pool must be false")
    } else skip;

    if (now < deadline) then skip else {
      failwith("The current time must be less than the deadline.");
    };

    if (amount > 0mutez) then {
      failwith("amount must be zero.");
    } else skip;
    
    if (min_xtz_withdrawn > 0mutez) then skip else {
      failwith("min_xtz_withdrawn must be greater than zero.");
    };

    if (min_tokens_withdrawn > 0n) then skip else {
      failwith("min_tokens_withdrawn must be greater than zero.");
    };

    if (lqt_burned > 0n) then skip else {
      failwith("lqt_burned must be greater than zero.");
    };

    // balance if owner, otherwise allowance
    var owner_lqt_balance: nat := 0n;
    // only used if the sender is not the owner
    var sender_allowance: nat := 0n;

    case storage.accounts[owner] of
    | None -> failwith("The owner does not have any liquidity in this contract.")
    | Some(account) -> {
        owner_lqt_balance := account.balance;
        if sender =/= owner then {
          case account.allowances[sender] of
            | None -> failwith("The sender does not have an allowance from the give owner in this contract.")
            | Some(allowance) -> {
                sender_allowance := allowance;
                if (lqt_burned > sender_allowance) then {
                  failwith("lqt_burned cannot be greater than the sender's allowance.");
                } else skip;
              }
          end;
        } else skip;
      }
    end;

    if (lqt_burned > owner_lqt_balance) then {
      failwith("lqt_burned cannot be greater than the owner's balance.");
    } else skip;

    const xtz_withdrawn        : tez = natural_to_mutez(lqt_burned * mutez_to_natural(storage.s.xtz_pool) / storage.s.lqt_total);

    if (xtz_withdrawn >= min_xtz_withdrawn) then skip else {
      failwith("The amount of xtz withdrawn must be greater than or equal to min_xtz_withdrawn.");
    };              

    const tokens_withdrawn: nat = lqt_burned * storage.s.token_pool / storage.s.lqt_total;

    if (tokens_withdrawn >= min_tokens_withdrawn) then skip else {
      failwith("The amount of tokens withdrawn must be greater than or equal to min_tokens_withdrawn.");
    };

    const account: account = get_account(owner, storage.accounts);

    // calculate new_balance, convert int to nat
    const new_balance_int: int = account.balance - lqt_burned;
    var new_balance: nat := 0n;
    if (new_balance_int >= 0) then {
      new_balance := abs(new_balance_int);
    } else {
      failwith("account.balance - lqt_burned resulted in a negative number.")
    };

    storage.accounts[owner] := record balance = new_balance; allowances = account.allowances; end;
                                      
    // calculate lqt_total, convert int to nat
    const lqt_total: nat = abs(storage.s.lqt_total - lqt_burned);

    // calculate token_pool, convert int to nat
    const token_pool_int: int = storage.s.token_pool - tokens_withdrawn;
    if (token_pool_int >= 0) then {
      storage.s.token_pool := abs(token_pool_int);
    } else {
      failwith("storage.s.token_pool - tokens_withdrawn resulted in a negative number.")
    };

    // update allowance if owner is not the sender
    if (owner =/= sender) then {
      const new_sender_allowance: int = sender_allowance - lqt_burned;
      if (new_sender_allowance >= 0) then {
        var account: account       := get_account(owner, storage.accounts);
        account.allowances[sender] := abs(new_sender_allowance);
        storage.accounts[owner]    := record balance = account.balance; allowances = account.allowances; end;        
      } else {
        failwith("sender_allowance - lqt_burned resulted in a negative number")
      };
    } else skip;

    // update xtz_pool
    storage.s.xtz_pool      := storage.s.xtz_pool - xtz_withdrawn;
                    
    // send xtz_withdrawn to to_ address
    const to_contract: contract(unit) = get_contract(to_);
    const op1: operation = transaction(unit, xtz_withdrawn, to_contract);

    // send tokens_withdrawn to to address
    // if tokens_withdrawn if greater than storage.s.token_pool, this will fail
    const token_contract: contract(token_contract_transfer) = get_entrypoint("%transfer", storage.s.token_address);
    const op2: operation = transaction((self_address, (to_, tokens_withdrawn)), 0mutez, token_contract);
    op_list := list op1; op2; end
  } with (op_list, storage);

function xtz_to_token(const to_              : address;
                      const min_tokens_bought: nat;
                      const deadline         : timestamp;
                      var storage            : storage):                      
                      return is
  block {
    var op_list: list(operation) := nil;

    if (storage.s.self_is_updating_token_pool) then {
      failwith("self_is_updating_token_pool must be false")
    } else skip;

    if (now < deadline) then skip else {
      failwith("The current time must be less than the deadline.");
    };

    if (amount = 0mutez) then {
      failwith("amount must be greater than zero.");
    } else skip;

    if (min_tokens_bought = 0n) then {
      failwith("min_tokens_bought must be greater than zero.");
    } else skip;

    if (storage.s.xtz_pool = 0tez) then {
      failwith("xtz_pool must be greater than zero.");
    } else skip;

    if (storage.s.token_pool = 0n) then {
      failwith("token_pool must be greater than zero.");
    } else skip;

    const xtz_pool             : nat = mutez_to_natural(storage.s.xtz_pool);
    const nat_amount           : nat = mutez_to_natural(amount);
    const tokens_bought        : nat = (nat_amount * 997n * storage.s.token_pool) / (xtz_pool * 1000n + (nat_amount * 997n));

    if (tokens_bought >= min_tokens_bought) then skip else {
      failwith("tokens_bought must be greater than or equal to min_tokens_bought.");
    };

    const new_token_pool_int: int = storage.s.token_pool - tokens_bought;
    var new_token_pool: nat := 0n;
    if (new_token_pool_int >= 0) then {
      storage.s.token_pool := abs(new_token_pool_int);
    } else {
      failwith("token_pool - tokens_bought is negative.")
    };

    // update xtz_pool
    storage.s.xtz_pool      := storage.s.xtz_pool + amount;    

    // send tokens_withdrawn to to address
    // if tokens_bought is greater than storage.s.token_pool, this will fail
    const token_contract: contract(token_contract_transfer) = get_entrypoint("%transfer", storage.s.token_address);
    const op: operation = transaction((self_address, (to_, tokens_bought)), 0mutez, token_contract);

    // append internal operations
    op_list := list op; end;
  } with (op_list, storage);

function token_to_xtz(const owner         : address; // the address of the owner of FA1.2
                      const to_           : address;
                      const tokens_sold   : nat;
                      const min_xtz_bought: tez;
                      const deadline      : timestamp;
                      var storage         : storage):                      
                      return is
  block {
    var op_list: list(operation) := nil;

    if (storage.s.self_is_updating_token_pool) then {
      failwith("self_is_updating_token_pool must be false")
    } else skip;

    if (now < deadline) then skip else {
      failwith("The current time must be less than the deadline.");
    };

    if (amount > 0mutez) then {
      failwith("amount must be zero.");
    } else skip;

    if (tokens_sold > 0n) then {
      failwith("tokens_sold must be greater than zero.");
    } else skip;

    if (min_xtz_bought > 0mutez) then {
      failwith("min_xtz_bought must be greater than zero.");
    } else skip;

    if (storage.s.xtz_pool = 0tez) then {
      failwith("xtz_pool must be greater than zero.");
    } else skip;

    if (storage.s.token_pool = 0n) then {
      failwith("token_pool must be greater than zero.");
    } else skip;

    const xtz_bought           : tez = natural_to_mutez((tokens_sold * 997n * mutez_to_natural(storage.s.xtz_pool)) / (storage.s.token_pool * 1000n + (tokens_sold * 997n)));

    if (xtz_bought >= min_xtz_bought) then skip else {
      failwith("xtz_bought must be greater than or equal to min_xtz_bought.");      
    };

    storage.s.token_pool      := storage.s.token_pool + tokens_sold;
    storage.s.xtz_pool        := storage.s.xtz_pool - xtz_bought;

    // send xtz_bought to to_ address
    const to_contract: contract(unit) = get_contract(to_);
    const op1: operation = transaction(unit, xtz_bought, to_contract);

    // send tokens_sold to the exchange address
    // this assumes that the exchange has an allowance for the token and owner in FA1.2
    const token_contract: contract(token_contract_transfer) = get_entrypoint("%transfer", storage.s.token_address);
    const op2: operation = transaction((owner, (self_address, tokens_sold)), 0mutez, token_contract);

    // append internal operations
    op_list := list op1; op2; end;
  } with (op_list, storage);

// this is to allow the baker to send XTZ awards to this contract
function default_(const storage: storage): return is
  block {
    // update xtz_pool
    storage.s.xtz_pool := storage.s.xtz_pool + amount;  
    const op_list: list(operation) = nil;
  } with (op_list, storage);

function set_baker(const baker       : option(key_hash) ;
                   const freeze_baker: bool ;
                   var   storage: storage):
                   return is
  block {
    var op_list: list(operation) := nil;

    if (storage.s.self_is_updating_token_pool) then {
      failwith("self_is_updating_token_pool must be false")
    } else skip;

    if (amount > 0mutez) then {
      failwith("amount must be zero.");
    } else skip;

    if (sender = storage.s.manager) then {
      if (storage.s.freeze_baker = False) then {
        op_list := list (set_delegate(baker)); end;
        storage.s.freeze_baker := freeze_baker;
      } else {
        failwith("The baker is frozen.");
      }     
    } else {
      failwith("only the manager can set the baker.");
    };
  } with (op_list, storage);

function set_manager(const new_manager: address;
                     var storage      : storage):
                     return is
  block {
    const op_list: list(operation) = nil;

    if (storage.s.self_is_updating_token_pool) then {
      failwith("self_is_updating_token_pool must be false")
    } else skip;

    if (amount > 0mutez) then {
      failwith("amount must be zero.");
    } else skip;

    if (sender = storage.s.manager) then {
      storage.s.manager := new_manager;
    } else {
      failwith("Only the manager can change the manager.");
    }
  } with (op_list, storage);


function update_token_pool(const sender_key_hash : key_hash; var storage : storage) : return is
  block {
    const sender_as_contract : contract(unit) = Tezos.implicit_account(sender_key_hash);
    const sender_address : address = Tezos.address(sender_as_contract);
    var op_list: list(operation) := nil;    
    if (sender = sender_address) then {
      const dexter_update_token_pool_internal: contract(nat) = Tezos.self("%updateTokenPoolInternal");
      const token_get_balance: contract(get_balance) = get_entrypoint("%getBalance", storage.s.token_address);
      const op1: operation = transaction((self_address, dexter_update_token_pool_internal), 0mutez, token_get_balance);
      op_list := list op1; end;
      storage.s.self_is_updating_token_pool := True;
    } else {
      failwith("UnsafeUpdateTokenPool");
    }
  } with (op_list, storage);

function update_token_pool_internal(const token_pool: nat ; var storage : storage) : return is
  block {
    var op_list: list(operation) := nil;
    if (not storage.s.self_is_updating_token_pool or sender =/= storage.s.token_address) then {
      failwith("This entrypoint may only be called by dexter itself via getBalance of tokenAddress.");
    }  else {
      storage.s.token_pool := token_pool;
      storage.s.self_is_updating_token_pool := False;
    }
  } with (op_list, storage);

// currently unused while gas limits are insufficient
function token_to_token(const intermediate_token_exchange : address;
                        const min_tokens_bought           : nat;
                        const owner                       : address; 
                        const to_                         : address;
                        const tokens_sold                 : nat;
                        const deadline                    : timestamp;
                        var   storage                     : storage):
                        return is
  block {
    var op_list: list(operation) := nil;

    if (storage.s.self_is_updating_token_pool) then {
      failwith("self_is_updating_token_pool must be false")
    } else skip;

    if (now < deadline) then skip else {
      failwith("The current time must be less than the deadline.");
    };

    if (amount > 0mutez) then {
      failwith("amount must be zero.");
    } else skip;

    if (tokens_sold > 0n) then {
      failwith("tokens_sold must be greater than zero.");
    } else skip;

    if (min_tokens_bought = 0n) then {
      failwith("min_tokens_bought must be greater than zero.");
    } else skip;

    if (storage.s.xtz_pool = 0tez) then {
      failwith("xtz_pool must be greater than zero.");
    } else skip;

    if (storage.s.token_pool = 0n) then {
      failwith("token_pool must be greater than zero.");
    } else skip;

    const xtz_bought: tez = natural_to_mutez((tokens_sold * 997n * balance) / natural_to_mutez(storage.s.token_pool * 1000n + (tokens_sold * 997n)));
    storage.s.token_pool   := storage.s.token_pool + tokens_sold;
    storage.s.xtz_pool     := storage.s.xtz_pool - xtz_bought;

    // send xtz_bought to to_ address

    const intermediate_token_exchange_contract: contract((address * nat * timestamp)) = get_entrypoint("%xtzToToken", intermediate_token_exchange);
    const op1: operation = transaction((to_, min_tokens_bought, deadline), xtz_bought, intermediate_token_exchange_contract);

    // send tokens_sold to the exchange address
    // this assumes that the exchange has an allowance for the token and owner in FA1.2
    const token_contract: contract(token_contract_transfer) = get_entrypoint("%transfer", storage.s.token_address);      
    const op2: operation = transaction((owner, (self_address, tokens_sold)), 0mutez, token_contract);

    // append internal operations
    op_list := list op1; op2; end;
  } with (op_list, storage);


// =============================================================================
// Main
// =============================================================================

function main (const entrypoint : entrypoint ; const storage : storage) : (list(operation) * storage) is
  (case entrypoint of
  | Approve(xs)         -> approve(xs.0,xs.1.0,xs.1.1,storage)
  | AddLiquidity(xs)    -> add_liquidity(xs.0,xs.1,xs.2,xs.3,storage)
  | RemoveLiquidity(xs) -> remove_liquidity(xs.0.0,xs.0.1.0,xs.0.1.1,xs.1.0,xs.1.1.0,xs.1.1.1,storage)
  | XtzToToken(xs)      -> xtz_to_token(xs.0,xs.1.0,xs.1.1,storage)
  | TokenToXtz(xs)      -> token_to_xtz(xs.0.0,xs.0.1,xs.1.0,xs.1.1.0,xs.1.1.1,storage)
  | SetBaker(xs)        -> set_baker(xs.0, xs.1, storage)
  | SetManager(xs)      -> set_manager(xs, storage)
  | Default(xs)         -> default_(storage)
  | UpdateTokenPool(xs) -> update_token_pool(xs, storage)
  | UpdateTokenPoolInternal(xs) -> update_token_pool_internal(xs, storage)
  // | TokenToToken(xs)    -> token_to_token(xs.0.0,xs.0.1.0,xs.0.1.1,xs.1.0,xs.1.1.0,xs.1.1.1,storage)    
  end);
