# Dexter Contracts

Dexter is a Tezos on-chain, decentralized exchange for the native Tezos token
`tez` (or `XTZ`) and assets built on the Tezos block chain. Currently it supports tokens
from [FA1.2 contracts](https://gitlab.com/tzip/tzip/blob/master/proposals/tzip-7/tzip-7.md),
but will support others in the future.

This tutorial assumes you are familiar with the using the Tezos client. If you 
are not, please read our [first](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/01) 
and [second](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/02) 
Michelson tutorials. They show you how to download the Tezos client, create 
accounts on the testnet, transfer tokens and originate contracts.

# Originating the Dexter Contract from the Command Line

All of the example are written for using the compiled `tezos-client` on the 
testnet.

Dexter is composed of a single contract and needs an address of a
FA1.2 token contract to originate it.

Clone the dexter repository.

```bash
git clone git@gitlab.com:camlcase-dev/dexter.git
```

There is a simple FA1.2 token at [fa1.2](https://gitlab.com/camlcase-dev/fa1.2).

The dexter contract is at 
[../morley/dexter.tz](../morley/dexter.tz).

If you want to compile it yourself, read the instructions at 
[../morley/README.md](../morley/README.md).

## Originating an FA1.2 Token Contract

First, lookup the address of the account with which you want to originate the 
FA1.2 token and give ownership of the coins to.

```bash
$ tezos-client list known addresses

simon: tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3 (unencrypted sk known)
bob: tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i (unencrypted sk known)
alice: tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV (unencrypted sk known)
```

I have three accounts on the testnet. I am going to originate a token with
1,000,000 tokens. I will place all of them in Alice's custody. Copy the command 
below and replace `alice` with the name of your account and 
`"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"` with your account's address. You 
can change the amount of tokens to whatever number you like. Make sure the two 
numbers match or there will be untransferrable tokens in the contract.

I am calling this token `Tezos Gold`, but there is no storage field in FA1.2 for
the name or the symbol of the token.

```bash
tezos-client originate contract tezosGold \
             transferring 0 from alice \
             running path/to/fa1.2.tz \
             --init 'Pair {Elt "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair 100000000000000 {})} 100000000000000' \ 
             --burn-cap 5
```

If you want to originate another contract stored as `tezosGold` in your local 
machine, you can overwrite it by adding `--force` to the end of the command. 
Just remember that it will disassociate the old contract address from 
the symbol, but that contract will still exist on the Tezos blockchain.

Now Alice can transfer Tezos Gold tokens to another account. In the command 
below, the first account address is the owner Alice and the second one is the 
to address Simon. The numerical value is the number of Tezos Gold tokens that 
Alice will send to Simon.

```bash
tezos-client transfer 0 \ 
             from alice \
             to tezosGold \
             --entrypoint 'transfer' \
             --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 100)' \
             --burn-cap 1
```

Finally, you can check the Tezos Gold balance of each account.

```bash
tezos-client get big map value \ 
             for '"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"' \
             of type 'address' in tezosGold
```

You can create multiple token contracts. Just copy the first command and replace 
the values as you like.

## Originate the Dexter Exchange Contract

When we originate the Dexter exchange contract, we need to include the 
address of the FA1.2 token contract we just originated. We can look up the 
address in the command line.

```bash
tezos-client list known contracts
```

returns:
```
tezosGold: KT1KxdSabTUDVXvnXkyW7udQvp6FjG9FcrBm
```

Now we can originate the exchange contract. Replace `alice` with the name of 
your account, the first address is the baker manager address and the second 
address is the tezos contract address. In our case the baker manager is `alice`.

```
tezos-client originate contract tezosGoldExchange \ 
             transferring 0 from alice \ 
             running path/to/dexter.tz \
             --init 'Pair {} (Pair (Pair False (Pair False 0)) (Pair (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" "KT1KxdSabTUDVXvnXkyW7udQvp6FjG9FcrBm") (Pair 0 0)))' --burn-cap 20 --force
```

Let's add some liquidity. In order to do so, Alice must first approve an 
allowance for Tezos Gold Exchange to spend her Tezos Gold tokens. The first value 
in the parameter pair is the address of whom we want to give an allowance to and 
the second value is the amount of the allowance.

```bash
tezos-client transfer 0 \ 
             from alice \
             to tezosGold 
             --entrypoint 'approve' \
             --arg 'Pair "KT1VbT8n6YbrzPSjdAscKfJGDDNafB5yHn1H" 200' \
             --burn-cap 1
```

In the command below, `transfer 10` means Alice is  adding 10 tez to the 
liquidity pool. The next number is the minimum amount of 
liquidity you want to mint (if that number is not reached it will fail and 
return the tez to you). The liquidity is an internal number, referred to as DEX 
in the contract, that the exchange tracks. Then the next number is the maximum 
number of Tezos Gold tokens you want to add to the liquidity pool. When the 
liquidity pool is empty it will add the maximum amount if the account has that 
many tokens. And finally the date is the deadline for which you would like the 
transaction to occur by. 

The values in the parameter are: address of who will own the liquidity from
tezosGoldExchange, the minimum amount of liquidity the user wants to create,
the maximum number of FA1.2 tokens they want to deposit, and the deadline.

```bash
tezos-client transfer 10 
             from alice \
             to tezosGoldExchange \
             --entrypoint 'addLiquidity' \
             --arg 'Pair (Pair "tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z" 1) (Pair 123456 "2030-01-01T12:00:00Z")' \
             --burn-cap 1
```

In this transaction, Alice has initiated the liquidity pool with 10 tez and 100 
Tezos Gold tokens meaning that she has valued Tezos Gold at 10-1. Her ownership
of liquidity in this exchange contract is represented by an internal liquidity
token called LQT.

We can query her ownership with the following command.

```bash
tezos-client get big map value \
             for '"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"' \
             of type 'address' \
             in tezosGoldExchange
```

returns:
```
Pair (Pair {} 10000000) None
```

Alice holds 10000000 LQT tokens.

With another account you can try buying some Tezos Gold tokens from the exchange. 
`transfer 5` means Simon spends one tez. The address is who will receive the 
Tezos Gold tokens, in this case Simon. `5` means he wants to receive at least 1 
Tezos Gold for his five tez and the date is the deadline for the transaction to 
occur. If the deadline has passed or Simon's minimum request has not been met, 
the transaction will fail.

```bash
tezos-client transfer 5 \
             from simon \
             to tezosGoldExchange \
             --entrypoint 'xtzToToken' \
             --arg 'Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" (Pair 1 "2030-01-29T18:00:00Z")' \
             --burn-cap 1
```

After the xtzToToken transaction, you can confirm Simon's current balance of
Tezos Gold with the following command:

```bash
tezos-client get big map value for '"tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3"' \
             of type 'address' in tezosGold
```

We can also purchase XTZ with Tezos Gold tokens. Make sure `transfer` is set to `0` or you
are giving free XTZ to the exchange contract. The first number after 
`transfer 0` is the number of tokens you want to sell, then the minimum amount 
of XTZ (in mutez) that you want to receive (one tez is 1,000,000 mutez), and 
finally the deadline by which the transfer should occur.

```bash
tezos-client transfer 0 
             from simon \
             to tezosGoldExchange \
             --entrypoint 'tokenToXtz' \
             --arg 'Pair (Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3") (Pair 40 (Pair 1 "2030-01-01T12:00:00Z"))' \
             --burn-cap 1
```

The last thing we can do is remove liquidity from the exchange and receive an 
equivalent amount of XTZ and Tezos Gold. The first value is the address of the
LQT (the internal exchange token for keeping track of liquidity) owner, the 
second is the address of who will receive the XTZ and the Tezos Gold, the third 
is the amount of LQT you want to get rid of, the fourth is the minimum amount 
of XTZ (in mutez) you want to withdraw (one tez is 1,000,000 mutez) and the fifth
is the minimum amount of Tezos Gold tokens you with to withdraw. If the deadline 
is passed of if any of the minimums are not met, then the transaction will fail.

```bash
$ tezos-client transfer 0 from alice to tezosGoldExchange \
               --entrypoint 'removeLiquidity' \
               --arg 'Pair (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 5000000)) (Pair 1 (Pair 1 "2030-06-29T18:00:21Z"))' \
               --burn-cap 1
```


# Appendix A: FA1.2 Token Contract Commands

## Originate a FA1.2 Token Contract

Reference:

```bash
$ tezos-client originate contract <token-contract-name> \
               transferring 0 from <owner> \
               running path/to/fa1.2.tz \
               --init 'Pair {Elt "<owner>" (Pair <token-total-amount> {})} <token-total-amount>' \
               --burn-cap 5
```

Example:

```bash
$ tezos-client originate contract tezosGold \
               transferring 0 from alice \
               running path/to/fa1.2.tz \
               --init 'Pair {Elt "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair 1000000 {})} 1000000' \ 
               --burn-cap 2
```

## Transfer FA1.2 Tokens

Reference:

```bash
tezos-client transfer 0 from <sender> \
             to <token-contract-name> \
             --entrypoint 'transfer' \
             --arg 'Pair "<owner>" (Pair "<destination>" <value>)' \
             --burn-cap 1
```

The sender must be the owner of the tokens or have an allownace from the owner 
of the tokens.

Example:

```bash
tezos-client transfer 0 from alice \
             to tezosGold \ 
             --entrypoint 'transfer' \
             --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 100)' \ 
             --burn-cap 1
```

## View FA1.2 Account Balance

Reference:

```bash
tezos-client get big map value for '"<owner>"' \
             of type 'address' in <token-contract-name>
```

Example:

```bash
tezos-client get big map value for '"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"' \
             of type 'address' in tezosGold
```

## Approve FA1.2 Allowance

Reference:

```bash
tezos-client transfer 0 from alice \
             to tezosGold \ 
             --entrypoint 'approve' \ 
             --arg 'Pair "<spender>" <allowance>' \
             --burn-cap 1
```

Example:

```bash
tezos-client transfer 0 from alice \
             to tezosGold \ 
             --entrypoint 'approve' \ 
             --arg 'Pair "KT1VbT8n6YbrzPSjdAscKfJGDDNafB5yHn1H" 200' \
             --burn-cap 1
```

# Appendix B: Dexter Exchange

## Originate a Dexter Exchange

Reference:
```bash
tezos-client originate contract <exchange-contract-name> \
             transferring 0 from alice \
             running container:contracts/dexter.tz \
             --init 'Pair {} (Pair (Pair False (Pair False 0)) (Pair (Pair "<manager-address>" "<token-address>") (Pair 0 0)))'
             --burn-cap 10
```

Example:
```bash
tezos-client originate contract tezosGoldExchange \
             transferring 0 from alice \
             running container:contracts/dexter.tz \
             -- init 'Pair {} (Pair (Pair False (Pair False 0)) (Pair (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" "KT1PuoBCrK7bu9MP7LKZRhjXKZSwvgQkDrCN") (Pair 0 0)))' \
             --burn-cap 10
```


## Add Liquidity

Reference:
```bash
tezos-client transfer <xtz> 
             from <sender> \ 
             to <exchange-contract-name> \
             --arg 'Pair (Pair "<owner>" <min_lqt_created>) (Pair <max_tokens_deposited> "<deadline>")' \
             --burn-cap 1
```

Example:
```bash
tezos-client transfer 10 
             from alice \
             to tezosGoldExchange \
             --entrypoint 'addLiquidity' \
             --arg 'Pair (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 1) (Pair 100 "2030-06-29T18:00:21Z")' \
             --burn-cap 1
```

## Remove Liquidity

Reference:
```bash
tezos-client transfer 0 from <sender> \
             to <exchange-contract-name> \
             --entrypoint 'removeLiquidity' \
             --arg 'Pair (Pair "<owner>" (Pair "<to>" <lqt_burned>)) (Pair <min_xtz_withdrawn> (Pair <min_tokens_withdrawn> "<deadline>"))' \
             --burn-cap 1
```

If the sender is not the owner, then they need to have an allowance approval 
from the owner in order to burn the LQT amount specified.

Example:
```bash
tezos-client transfer 0 from alice \
             to tezosGoldExchange \
             --entrypoint 'removeLiquidity' \
             --arg 'Pair (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 5000000)) (Pair 1 (Pair 1 "2030-06-29T18:00:21Z"))' \
             --burn-cap 1
```

## XTZ to FA1.2 Token

Reference:
```bash
tezos-client transfer <xtz> from <buyer> \ 
             to <exchange-contract-name> \
             --entrypoint 'xtzToToken' \
             --arg 'Pair "<to>" (Pair <min-tokens-required> "<deadline>")' \
             --burn-cap 1
```

Example:
```bash
tezos-client transfer 2 from bob \ 
             to tezosGoldExchange \
             --entrypoint 'xtzToToken' \
             --arg 'Pair "tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i" (Pair 1 "2030-06-29T18:00:21Z")' \
             --burn-cap 1
```

## FA1.2 Token to XTZ

Reference:
```bash
tezos-client transfer 0 from <sender> \
             to <exchange-contract-name> \
             --entrypoint 'tokenToXtz' \
             --arg 'Pair (Pair "<owner>" "<to>") (Pair <tokens_sold> (Pair <min_xtz_bought> "<deadline>"))' \
             --burn-cap 1
```

Example:
```bash
tezos-client transfer 0 from simon \
             to tezosGoldExchange \
             --entrypoint 'tokenToXtz' \
             --arg 'Pair (Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3") (Pair 40 (Pair 1 "2030-06-29T18:00:21Z"))' \
             --burn-cap 1
```

## FA1.2 Token to Token

Reference:
```bash
tezos-client transfer 0 from <sender> \
             to <exchange-contract-name> \
             --entrypoint 'tokenToken' \
             --arg 'Pair (Pair "<outputDexterContract>" (Pair <minTokensBought> "<owner>")) (Pair "<to>" (Pair <tokensSold> "<deadline>"))' \
             --burn-cap 1
```

Example:
```bash
tezos-client transfer 0 from simon \
             to tezosGoldExchange \
             --entrypoint 'tokenToToken' \
             --arg 'Pair (Pair "KT1QcD2WsVVqk1nknUm6wyErS2h329K59T42" (Pair 1 "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV")) (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair 1000 "2030-01-01T12:00:00Z"))' \
             --burn-cap 1
```

## Default 

Give free XTZ to a dexter contract. 

Example:
```bash
tezos-client transfer 5 from simon \
             to tezosGoldExchange \
             --entrypoint 'default' \
             --arg 'Unit' \
             --burn-cap 1
```

## Update token pool

The token pool no longer matches the amount held in the FA1.2 contract 
(because of unique behavior or someone gave Dexter tokens for free).

```bash
tezos-client transfer 5 from simon \
             to tezosGoldExchange \
             --entrypoint 'updateTokenPool' \
             --arg '<key-hash-of-sender>' \
             --burn-cap 1
```


Example:
```bash
tezos-client transfer 5 from simon \
             to tezosGoldExchange \
             --entrypoint 'updateTokenPool' \
             --arg '"tz1QGsyN9TyFHJbKTcF3FtKbm5Gye3QqXw3t"' \
             --burn-cap 1
```

## Set Baker

The manager can set a baker to no baker or a new one. They can also freeze the
right to change the baker again.

```bash
tezos-client transfer 5 from simon \
             to tezosGoldExchange \
             --entrypoint 'setBaker' \
             --arg 'Pair (Some "<key-hash-of-baker>") False' \
             --burn-cap 1
```

Example:
```bash
tezos-client transfer 5 from simon \
             to tezosGoldExchange \
             --entrypoint 'setBaker' \
             --arg 'Pair (Some "tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q") False' \
             --burn-cap 1
```

## Approve

Give a third party rights to burn liquidity for a liquidity owner.
You must provide the third party's current allowance for security
purposes

```bash
tezos-client transfer 5 from simon \
             to tezosGoldExchange \
             --entrypoint 'approve' \
             --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair <allowance> <currentAllowance>)' \
             --burn-cap 1
```

Example:
```bash
tezos-client transfer 5 from simon \
             to tezosGoldExchange \
             --entrypoint 'approve' \
             --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair 1000000 0)' \
             --burn-cap 1
```

## Get Exchange Total Liquidity

Reference:
```bash
tezos-client get contract storage for <exchange-contract-name>
```

Example (tzBTC):
```bash
tezos-client get contract storage for KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf

Pair 123
     (Pair (Pair False (Pair False 51690820426))
           (Pair (Pair "KT1B5VTw8ZSMnrjhy337CEvAm4tnT8Gu8Geu" "KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
                 (Pair 809200198 68673273307)))

8.09200198 tzBTC
68,673.273307 XTZ
51690820426 liquidity tokens
```

## Get Exchange Total XTZ

Reference:
```bash
tezos-client get balance for <exchange-contract-name>
```

Example:
```bash
tezos-client get balance for tezosGoldExchange
```

## Get Exchange Total Tokens

Reference:
```bash
tezos-client get big map value for '"<exchange-contract-address>"' \
             of type 'address' in tezosGold
```

Example:
```bash
tezos-client get big map value \
             for '"KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe"' \
             of type 'address' in tezosGold
```

## Get Big Map Id

The values of a big map are not returned when you query a contracts storage. When
you query a contract's it will return a big map id.

For a FA1.2 contract, its big map id is the left item of the first pair.

```bash
tezos-client get contract storage for tezosGold
```
returns:
```
Pair 1641 ...

1641
```

For a Dexter exchange, its big map id is also the left item of the first pair.
```bash
tezos-client get contract storage for tezosGoldExchange
```

returns:
```
Pair 1963 ...
           
1963
```

Then you can get the FA1.2 of a contract and the Dexter liquidity of a contract.

Get the hash.
```bash
$ tezos-client show address alice
Hash: tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV
```

Get the script expression of the hash
```bash
$ tezos-client hash data '"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"' of type address
Script-expression-ID-Hash: expruVJXegzJ1h5Q7wU9rXBVyXS1Sqj6No6tfqck3NNZmRrKmFQB1b
```

FA1.2
```bash
$ tezos-client get element expruVJXegzJ1h5Q7wU9rXBVyXS1Sqj6No6tfqck3NNZmRrKmFQB1b of big map 1641
```

Dexter
```bash
$ tezos-client get element expruVJXegzJ1h5Q7wU9rXBVyXS1Sqj6No6tfqck3NNZmRrKmFQB1b of big map 1963
```
