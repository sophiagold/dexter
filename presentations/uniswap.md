https://en.wikipedia.org/wiki/Market_maker
that quotes both a buy and a sell price in a financial instrument or commodity 
held in inventory, hoping to make a profit on the bid-offer spread, or turn.



constant product market making formula which sets the exchange rate based off of 
the relative size of the ETH and ERC20 reserves, and the amount with which an 
incoming trade shifts this ratio. Selling ETH for ERC20 tokens increases the 
size of the ETH reserve and decreases the size of the ERC20 reserve. This shifts 
the reserve ratio, increasing the ERC20 token's price relative to ETH for 
subsequent transactions. The larger a trade relative to the total size of the 
reserves, the more price slippage will occur.

On-chain prices can change between when a transaction is signed and when it is 
included in a block. Traders can bound price fluctuations by specifying the 
minimum amount bought on sell orders, or the maximum amount sold on buy orders. 
This acts as a limit order that will automatically cancel if it is not filled. 
It is also possible to set transaction deadlines which will cancel orders if 
they are not executed fast enough.
