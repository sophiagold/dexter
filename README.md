# Dexter

Dexter is a decentralized exchange on the Tezos blockchain. 

This repository has a specification and implemenation of the dexter contracts. 
A Dexter contract creates an exchange market for an XTZ and a FA1.2 token pair. 

There are two documents that explain what Dexter is and how to use it:

- [Informal Specification of Dexter](./docs/dexter-informal-specification.md)
- [How to use Dexter via the command line](./docs/dexter-cli.md)

There are two versions of the Dexter implementation. One in Ligo for development
and experimentation and one in morley for optimization. The implementation in
Morley is the definitive version for use on the mainnet and follows a formal
specification. The Ligo version should not be used on mainnet.

Nomadic Labs has written a formal specification and a [proof](https://gitlab.com/nomadic-labs/mi-cho-coq/-/merge_requests/71)
that the Michelson contract output by the [Morley](./morley) implementation 
satisfies this specification.

Before originating a Dexter exchange contract for an FA1.2 contract on the 
mainnet,  prepare the [Token integration checklist](./docs/token-integration.md)
for the target FA1.2 contract.

## Merge Requests

The Michelon dexter contracts produced by the morley project are frozen. We only accept
MRs that do not change the output code at [morley/dexter.tz](./morley/dexter.tz).

## Related Projects

- [dexter-frontend](https://gitlab.com/camlcase-dev/dexter-frontend)
- [dexter-indexer](https://gitlab.com/camlcase-dev/dexter-indexer)
- [FA1.2](https://gitlab.com/camlcase-dev/fa1.2)
- [tzip-7: FA1.2 specification](https://gitlab.com/tzip/tzip/-/blob/2bedaa85c445732b9bf8914bf29cb9dc5fb2cf4f/proposals/tzip-7/tzip-7.md)
