# Changelog for dexter

# 1.0.0.0 -- 2020-09-29
* Nomadic Labs has written a formal specification and a proof that that the
  Michelson contract output by the executable dexter-contracts satisfies the
  specification. The Michelson contracts for dexter version 1 are frozen.
  We only accept PRs that do not change the output Michelson contracts.

# 0.1.4.0 -- 2020-09-28
* Add missing condition checks to various entrypoints.

# 0.1.3.0 -- 2020-09-28
* Fix order of operations match the specification for tokenToXtz.

# 0.1.2.0 -- 2020-09-25
* Make order of operations match the specification in removeLiquidity, tokenToToken
  and tokenToXtz.

# 0.1.1.0 -- 2020-09-22
* Use strict inequality assertLt when asserting that now is less than the deadline.

# 0.1.0.0 -- 2020-09-08
* Address Trail of Bits security audit issue TOB-DEXTER-010:
  When calculating tokens deposited in addLiquidity, add one after division
  to if remaind is zero to solve rounding error.
* Freeze for proofs.

# 0.0.33.0 -- 2020-09-01
* Address Trail of Bits security audit issue TOB-DEXTER-001:
  UpdateTokenPool requires a KeyHash. This KeyHash must match the sender.
  Only implicit accounts have KeyHashes thus preventing contracts from
  calling UpdateTokenPool and potentially inserting operations.

# 0.0.32.0 -- 2020-08-26
* Address Trail of Bits security audit issue TOB-DEXTER-001:
  UpdateTokenPool cannot be called if selfIsUpdatingTokenPool is True.
  In token-integration mention expected behavior of transfer.

# 0.0.31.1 -- 2020-08-26
* Address Trail of Bits security audit issue TOB-DEXTER-001:
  Update token_integration.md: clarify the total supply further.

# 0.0.31.0 -- 2020-08-26
* Address Trail of Bits security audit issue TOB-DEXTER-005:
  In Dexter.Contract.Approve explain where the solution
  comes from.
* Address Trail of Bits security audit issue TOB-DEXTER-001:
  Update token_integration.md: specify a minimal expected amount
  of total supply.

# 0.0.30.1 -- 2020-08-25
* Address Trail of Bits security audit issue TOB-DEXTER-001:
  Add documenation for integrating with an FA1.2 tokens and mention
  restrictions based on how getBalance is implemented.

# 0.0.30.0 -- 2020-08-05
* Address Trail of Bits security audit issue TOB-DEXTER-001:
  Fix vulnerability with updateTokenPoolInternal.
* Rename calledByDexter to selfIsUpdatingTokenPool for clarity.
* If selfIsUpdatingTokenPool is true in any other entrypoint the
  UpdateTokenPool or UpdateTokenPoolInternal, if will fail.

# 0.0.29.0 -- 2020-08-05
* Address Trail of Bits security audit issue TOB-DEXTER-C-002:
  Use a Haskell linter to improve code readability.
* Apply hlint to code in src and clean up accordingly except for a few cases with
  lorentz. We prefer a little bit of duplication in the lorentz code for clarity

# 0.0.28.0 -- 2020-07-29
* Address TOB-DEXTER-004: Morley contracts are not properly tested for
  SetManager and SetBaker.
* Add property tests for SetManger and SetBaker.
* Remove unused function setAllowance.
* Rename transferSenderTokensToDexter to transferOwnerTokensToDexter.
* Update information specification.

# 0.0.27.0 -- 2020-07-29
* Address TOB-DEXTER-004: Morley contracts are not properly tested for
  TokenToXtz.
* Add property tests for TokenToToken.

# 0.0.26.0 -- 2020-07-28
* Address TOB-DEXTER-004: Morley contracts are not properly tested for
  TokenToXtz.
* Add property tests for TokenToXtz.

# 0.0.25.0 -- 2020-07-27
* Address TOB-DEXTER-004: Morley contracts are not properly tested for
  XtzToToken.
* Add property tests for XtzToToken.

# 0.0.24.0 -- 2020-07-27
* Address TOB-DEXTER-004: Morley contracts are not properly tested for
  Approve.
* Address TOB-DEXTER-011: Lack of “amount sent” protection can lead to trapped
  tezos.
* Add property tests for Approve.
* Prevent amount greater than zero XTZ being sent to Approve, SetBaker,
  SetManager, XtzToToken, and TokenToToken.

# 0.0.23.0 -- 2020-07-26
* Address TOB-DEXTER-004: Morley contracts are not properly tested for
  RemoveLiquidity.
* Address TOB-DEXTER-011: Lack of “amount sent” protection can lead to trapped
  tezos for RemoveLiquidity.
* Add property tests for RemoveLiquidity.
* Prevent amount greater than zero XTZ being sent to RemoveLiquidity.
* Fix bug: RemoveLiquidity was transferring owner tokens to receiver
  It should transfer them from Dexter to the receiver.
* Fix prop test in AddLiquidity.
* Use hspec.parallel to speed up tests.

# 0.0.22.0 -- 2020-07-22
* Address TOB-DEXTER-004: Morley contracts are not properly tested for
  AddLiquidity.
* Address TOB-DEXTER-009: Arithmetic rounding allows minting of liquidity
  tokens without payment of tokens.
* Prevent tokensDeposited from being zero in AddLiquidity.
* Clean up code.

# 0.0.21.0 -- 2020-07-22
* Address TOB-DEXTER-004: Morley contracts are not properly tested for
  UpdateTokenPool and UpdateTokenPoolInternal.
* Add property tests for UpdateTokenPool and UpdateTokenPoolInternal.
* Add fa-one-point-two morley contract as a dependency for testing.
* Add Lorentz.Test.Extension which adds more testing functionality.

# 0.0.20.0 -- 2020-07-16
* Add the setManager entrypoint.

# 0.0.19.0 -- 2020-07-13
* Address Trail of Bits security audit issue TOB-DEXTER-004
  4. Morley contracts are not properly tested.
* Add property tests for Default entrypoint.

# 0.0.18.0 -- 2020-07-08
* Address Trail of Bits security audit issue TOB-DEXTER-C-004
  Avoid using unsafe functions in production code.
* Remove mkMutezUnsafe and mkMTextUnsafe.
* Remove use of TemplateHaskell.
* Create a data type that holds error messages and Michelson code that use mutez values.
  The mutez values are calculated at runtime
* Fix test suite.

# 0.0.17.0 -- 2020-07-03
* Manually annotate storage value names (morley does not do this automatically).

# 0.0.16.0 -- 2020-07-03
* Address Trail of Bits security audit issue TOB-DEXTER-008
  Call injection allows price corruption.
* Add xtzPool as a storage value. BALANCE is not a good operation to depend on. Now dexter tracks how much XTZ it has.
* AddLiquidity, RemoveLiquidity, XtzToToken, TokenToXtz ,TokenToToken and Default now use and update the xtzPool value.

# 0.0.15.0 -- 2020-07-02
* Address Trail of Bits security audit issue TOB-DEXTER-007
  Improper use of Haskell type system to enforce type correctness in the stack.
* Replaced forcedCoerce_ from TokenToToken with contructorT.
* Replace type synonyms Owner, Receiver, Sender with strict types that do alone for accidental confusion.

# 0.0.14.0 -- 2020-06-16
* Address Trail of Bits security audit issue TOB-DEXTER-002
  tokenToXtz sends the tokens to the user.
* In tokenToXtz, send the tokens to the Dexter contract. Previously
  they were sent to the to address by mistake.

# 0.0.13.0 -- 2020-06-16
* Address Trail of Bits security audit issue TOB-DEXTER-001
  updateTokenPool can be abused to drain Dexter’s assets.
* Add calledByDexter to StorageFields for security purposes.
* Remove Approve from FA12 param type.
* Add GetBalanceParams type and use it in GetBalance constructor of FA12.
* Add UpdateTokenPoolInternal constructor to Params.
* Add Dexter.Contract.UpdateTokenPool module with updateTokenPool and
  updateTokenPoolInternal functions.
* This resolves error 1 and 5 from the Trail of Bits June 15, 2020 Dexter
  assesment report.

# 0.0.12.0 -- 2020-06-10
* Address Trail of Bits security audit issue TOB-DEXTER-005
  dexter. Approve of FA12 sum type is not compatible with FA1.2. Approve.
* Fix error with FA1.2 interaction. Transfer entrypoint expects (address, (address, natural).

# 0.0.11.0 -- 2020-06-08
* Make tests compile.

# 0.0.10.0 -- 2020-06-08
* Implement the token to token entrypoint.
* Change to_ to to.
* Update informal specification with details about the token to token
  entrypoint.

# 0.0.9.0 -- 2020-06-04
* Set tokenPool in initLiquidity, fix related tests. (bug)
* Fix cases were owner and to_ addresses were flipped. (bug)

# 0.0.8.0 -- 2020-06-01
* Force BigMap to appear in the first left of a top level pair in storage so we can
  use the simple POST query from tezos-client.

# 0.0.7.0 -- 2020-05-31
* Implement tokenToXtz.
* Add unit tests for tokenToXtz based on dexter-integration.

# 0.0.6.0 -- 2020-05-31
* Implement xtzToToken.
* Add unit tests for xtzToToken based on dexter-integration.

# 0.0.5.1 -- 2020-05-28
* Reorganize file structure.

# 0.0.5.0 -- 2020-05-28
* Add unit tests for removeLiquidity.
* Rename getOwnerBalanceOrSenderAllowance to getOwnerBalanceAndCheckSenderAllowance.
* Fix returned natural value of getOwnerBalanceAndCheckSenderAllowance.

# 0.0.4.0 -- 2020-05-27
* Add unit and property tests for addLiquidity entrypoint.
* Fix comparison error in addLiquidity.

# 0.0.3.0 -- 2020-05-17
* Implement removeLiquidity entrypoint.

# 0.0.2.0 -- 2020-05-17
* Implement addLiquidity entrypoint.

# 0.0.1.0 -- 2020-05-11
* Implement skeleton of the Dexter contract in morley/lorentz.
* Implement entrypoints: default, setBaker, updateTokenPool, approve.
* Add empty implementations of addLiquidity, removeLiquidity, xtzToToken, tokenToXtz.
* Output Dexter to .tz michelson file.
