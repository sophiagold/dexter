{-# LANGUAGE OverloadedStrings #-}

module Main where

-- tezos-client typecheck script dexter.tz

import qualified Data.Map as Map
import qualified Dexter.Contract as Dexter
import           Lorentz.ContractRegistry
import Prelude hiding (one)

main :: IO ()
main = do
  case (Dexter.defaultMutezValues, Dexter.defaultErrorMessages) of
    (Just mutezValues, Right errorMessages) -> do            
      let runtimeValues =
            Dexter.RuntimeValues
            mutezValues
            errorMessages

      let contractRegistry =
            ContractRegistry $ Map.fromList
              [ "Dexter" ?:: ContractInfo
                { ciContract      = Dexter.contract runtimeValues
                , ciIsDocumented  = True
                , ciStorageParser = Nothing
                , ciStorageNotes  = Just Dexter.storageNotes
                }    
              ]

      -- Print nameOfContract mOutputFile forceOneLine useMicheline
      runContractRegistry contractRegistry (Print "Dexter" (Just "dexter.tz") False False)

      -- this causes a runtime failure
      -- runContractRegistry contractRegistry (Print "Dexter" (Just "dexter.json") True True)

    _ -> fail "Unable to make runtime values"

