{-|
Module      : Dexter.Contract.Types
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.Test where

import qualified Data.Map.Strict as Map

import qualified Dexter.Contract as Dexter
import           Lorentz

import           Util.Named ((.!))

-- =============================================================================
-- Make Dexter parameter functions
-- =============================================================================

mkAddLiquidityParams :: Address -> Natural -> Natural -> Timestamp -> Dexter.AddLiquidityParams
mkAddLiquidityParams owner minLqtMinted maxTokensDeposited deadline =
  ( #owner              .! (#owner .! owner)
  , #minLqtMinted       .! minLqtMinted
  , #maxTokensDeposited .! maxTokensDeposited
  , #deadline           .! deadline
  )

mkApproveParams :: Address -> Natural -> Natural -> Dexter.ApproveParams
mkApproveParams spender allowance currentAllowance =
  ( #spender              .! (#spender .! spender)
  , #allowance            .! allowance
  , #currentAllowance     .! currentAllowance
  )
  
mkRemoveLiquidityParams :: Address -> Address -> Natural -> Mutez -> Natural -> Timestamp -> Dexter.RemoveLiquidityParams
mkRemoveLiquidityParams owner receiver lqtBurned minXtzWithdrawn minTokensWithdrawn deadline =
  ( #owner              .! (#owner .! owner)
  , #to                 .! (#receiver .! receiver)
  , #lqtBurned          .! lqtBurned
  , #minXtzWithdrawn    .! minXtzWithdrawn
  , #minTokensWithdrawn .! minTokensWithdrawn
  , #deadline           .! deadline
  )

mkXtzToTokenParams :: Address -> Natural -> Timestamp -> Dexter.XtzToTokenParams
mkXtzToTokenParams receiver minTokensBought deadline =
  ( #to              .! (#receiver .! receiver)
  , #minTokensBought .! minTokensBought
  , #deadline        .! deadline
  )

mkTokenToXtzParams :: Address -> Address -> Natural -> Mutez -> Timestamp -> Dexter.TokenToXtzParams
mkTokenToXtzParams owner receiver tokensSold minXtzBought deadline =
  ( #owner        .! (#owner .! owner)
  , #to           .! (#receiver .! receiver)
  , #tokensSold   .! tokensSold
  , #minXtzBought .! minXtzBought
  , #deadline     .! deadline
  )

mkTokenToTokenParams :: Address -> Natural -> Address -> Address -> Natural -> Timestamp -> Dexter.TokenToTokenParams
mkTokenToTokenParams outputDexterContract minTokensBought owner receiver tokensSold deadline =
  ( #outputDexterContract .! outputDexterContract
  , #minTokensBought      .! minTokensBought
  , #owner                .! (#owner .! owner)
  , #to                   .! (#receiver .! receiver)
  , #tokensSold           .! tokensSold
  , #deadline             .! deadline
  )
  
-- | Set the lqtBurned value in RemoveLiquidityParams
setLqtBurned :: Natural -> Dexter.RemoveLiquidityParams -> Dexter.RemoveLiquidityParams
setLqtBurned lqtBurned_ (owner_, to, _, minXtzWithdrawn_, minTokensWithdrawn_, deadline_) =
  (owner_, to, #lqtBurned .! lqtBurned_, minXtzWithdrawn_, minTokensWithdrawn_, deadline_)

-- =============================================================================
-- Dexter storage functions
-- =============================================================================

-- | init new Dexter storage for a manager and an FA1.2 contract
initStorage :: Address -> Address -> Dexter.Storage
initStorage manager token_ =
  Dexter.Storage
    (BigMap Map.empty)
    (Dexter.StorageFields
      False
      False
      0
      manager
      token_
      0
      zeroMutez
    )  

-- | A Dexter account with zero balance and no approvals
emptyAccount :: Dexter.Account
emptyAccount = (#balance .! 0, #approvals .! Map.empty)

-- | Set an account balance
setAccountBalance :: Natural -> Dexter.Account -> Dexter.Account
setAccountBalance n (_, approvals) = (#balance .! n, approvals)

-- | Set the value of hte sotrage's lqtTotal
setLqtTotal :: Natural -> Dexter.Storage -> Dexter.Storage
setLqtTotal newLqtTotal c = c { Dexter.fields = setLqtTotal' (Dexter.fields c) }
  where
    setLqtTotal' f = f { Dexter.lqtTotal = newLqtTotal } 

-- | Set the value of the storage's tokenPool
setTokenPool :: Natural -> Dexter.Storage -> Dexter.Storage
setTokenPool newTokenPool c = c { Dexter.fields = setTokenPool' (Dexter.fields c) }
  where
    setTokenPool' f = f { Dexter.tokenPool = newTokenPool } 

-- | Set the value of the storage's xtzPool
setXtzPool :: Mutez -> Dexter.Storage -> Dexter.Storage
setXtzPool newXtzPool c = c { Dexter.fields = setXtzPool' (Dexter.fields c) }
  where
    setXtzPool' f = f { Dexter.xtzPool = newXtzPool } 

setNewManager :: Address -> Dexter.Storage -> Dexter.Storage
setNewManager newManager c = c { Dexter.fields = setManager' (Dexter.fields c) }
  where
    setManager' f = f { Dexter.manager = newManager } 

setFreezeBaker :: Bool -> Dexter.Storage -> Dexter.Storage
setFreezeBaker freeze c = c { Dexter.fields = setFreezeBaker' (Dexter.fields c) }
  where
    setFreezeBaker' f = f { Dexter.freezeBaker = freeze } 

-- | insert a liquidity owner into the storage
insertLiquidityOwner :: Address -> Natural -> Map Address Natural -> Dexter.Storage -> Dexter.Storage
insertLiquidityOwner key value approvals c =
  case Dexter.accounts c of
    (BigMap bigMap) ->
      c { Dexter.accounts = BigMap (Map.insert key (#balance .! value, #approvals .! approvals) bigMap) }

