{-|
Module      : Dexter.Contract.XtzToToken
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.XtzToToken
  ( xtzToToken
  , calculateTokensBought
  ) where

import Dexter.Contract.Core
import Lorentz

-- stackType type synonyms to improve readability
type Denominator      = Natural
type XtzIn            = Natural
type Numerator        = Natural
type TokensBought     = Natural

-- | An address can sell XTZ to a dexter contract to get FA1.2 token.
-- The rate is determined by the pool sizes. A 0.3% fee is incurred and
-- split evenly amongst the liquidty providers.
xtzToToken ::
  (forall s1. RuntimeValues s1) ->
  Entrypoint XtzToTokenParams Storage
xtzToToken runtimeValues = do
  -- assert that selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)
  -- assert now < deadline
  getField #deadline; now; assertLt (deadlinePassed . errorMessages $ runtimeValues)
  -- assert amount > 0
  failIfAmountIsZero runtimeValues
  -- assert minTokensBought > 0
  getField #minTokensBought; push @Natural 0; assertLt (minTokensBoughtIsZero . errorMessages $ runtimeValues)
  -- assert xtzPool > 0
  dip (do stGetField #xtzPool; push @Mutez zeroMutez; assertLt (xtzPoolIsZero . errorMessages $ runtimeValues))
  -- assert tokenPool > 0
  dip (do stGetField #tokenPool; push @Natural 0; assertLt (tokenPoolIsZero . errorMessages $ runtimeValues))

  calculateTokensBought runtimeValues; stackType @[TokensBought, XtzToTokenParams, Storage]

  -- storage.tokenPool = tokenPool - tokensBought
  dup; duupX @4; stToField #tokenPool; sub; intToNatural
  dip (dig @2); stSetField #tokenPool
  stackType @[Storage, TokensBought, XtzToTokenParams]

  -- storage.xtzPool = xtzPool + amount
  stGetField #xtzPool; amount; add; stSetField #xtzPool
  stackType @[Storage, TokensBought, XtzToTokenParams]

  -- transfer token from Dexter to the to address
  dip (dip (do toField #to; fromNamed #receiver; selfCalling @Parameter CallDefault; address; toNamed #owner))
  stackType @[Storage, Natural, Owner, Address]
  transferDexterTokensTo
  dip nil; cons; pair

-- | Calculate the tokens the user has bought based on the tokenPool and xtzPool
-- sizes. The liquidity providers keep 0.3% of the XTZ sent in.
calculateTokensBought ::
  (forall s1. RuntimeValues s1) ->
  XtzToTokenParams & Storage & s :-> TokensBought & XtzToTokenParams & Storage & s
calculateTokensBought runtimeValues = do
  -- tokensBought = (xtzIn * 997 * tokenPool) / (xtzPool * 1000 + xtzIn * 997)
  -- calculate the denominator
  duupX @2; stToField #xtzPool; mutezToNatural; push @Natural 1000; mul;
  amount; mutezToNatural; dup; dip (do push @Natural 997; mul; add;)
  stackType @(XtzIn : Denominator : XtzToTokenParams : Storage : _)

  -- calculate the numerator
  push @Natural 997; mul; duupX @4; stToField #tokenPool; mul
  stackType @(Numerator : Denominator : XtzToTokenParams : Storage : _)

  ediv; ifNone (do push (divByZero . errorMessages $ runtimeValues); failWith) car
  stackType @(TokensBought : XtzToTokenParams : Storage : _)

  dup; dip (do dip (getField #minTokensBought); assertGe (tokensBoughtLessThanMin . errorMessages $ runtimeValues) )

  
transferDexterTokensTo :: Storage : Natural : Owner : Address : s :-> Operation : Storage : s
transferDexterTokensTo = do
  -- get FA1.2 address from storage
  stGetField #tokenAddress; contractCalling @FA12 (Call @"Transfer"); ifNone failWith nop
  -- send 0 mutez to the FA1.2 contract
  push @Mutez zeroMutez
  stackType @( Mutez : ContractRef TransferParams : Storage : Natural : Owner : Address : _)
  dig @3; dig @5; dig @5; fromNamed #owner; dip pair; pair
  transferTokens
