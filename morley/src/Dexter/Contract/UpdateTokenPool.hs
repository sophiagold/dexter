{-|
Module      : Dexter.Contract.UpdateTokenPool
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.UpdateTokenPool
  ( updateTokenPool
  , updateTokenPoolInternal
  ) where

import Dexter.Contract.Core
  ( FA12
  , Parameter
  , ErrorMessages(..)
  , RuntimeValues(..)
  , Storage
  , UpdateTokenPoolInternalParams
  , failIfAmountIsNotZero
  , failIfSelfIsUpdatingTokenPool
  , unsafeUpdateTokenPool
  )

import Lorentz

-- | The token pool value in Dexter may become out of sync with its true value
-- in FA1.2. Originally we tried to integrate a call to getBalance everewhere we
-- needed it, but it caused the gas costs to increase excessively. For security
-- purposes, updateTokenPool sets the selfIsUpdatingTokenPool flag to True, then calls
-- FA1.2 to get the balance and calls updateTokenPoolInternal with the balance.
-- If selfIsUpdatingTokenPool is True, then updateTokenPoolInternal will succesfully
-- update tokenPool, otherwise it will fail. This is to prevent attackers from
-- calling other entyrpoints in FA1.2 that have the same type signature as
-- getBalance and try to alter the tokenPool value in ways that were not intended.
updateTokenPool
  :: (forall s1. RuntimeValues s1) 
  -> Entrypoint KeyHash Storage
updateTokenPool runtimeValues = do
  -- assert amount == 0
  failIfAmountIsNotZero runtimeValues
  
  -- assert that the KeyHash is an implicit acccount that matches the sender's
  -- address
  implicitAccount; address; sender; assertEq (unsafeUpdateTokenPool . errorMessages $ runtimeValues)

  -- assert that selfIsUpdatingTokenPool == False
  failIfSelfIsUpdatingTokenPool runtimeValues

  push @Bool True; stSetField #selfIsUpdatingTokenPool  
  stGetField #tokenAddress; contractCalling @FA12 (Call @"GetBalance"); ifNone failWith nop

  push @Mutez zeroMutez
  
  selfCalling @Parameter (Call @"UpdateTokenPoolInternal")
  selfCalling @Parameter CallDefault; address; pair

  transferTokens

  dip nil; cons; pair

  
updateTokenPoolInternal
  :: (forall s1. RuntimeValues s1)
  ->  Entrypoint Natural Storage
updateTokenPoolInternal runtimeValues = do
  failIfAmountIsNotZero runtimeValues
  
  -- assert that selfIsUpdatingTokenPool is true, this is to prevent attacks being
  -- called directly from FA1.2
  duupX @2; stToField #selfIsUpdatingTokenPool; assert (notSelfIsUpdatingTokenPool . errorMessages $ runtimeValues)
  
  -- assert that the sender is the tokenAddress
  duupX @2; stToField #tokenAddress; sender
  assertEq (notCalledFromToken . errorMessages $ runtimeValues)
  stackType @[UpdateTokenPoolInternalParams, Storage]  

  -- set the tokenPool
  stSetField #tokenPool
  push @Bool False; stSetField #selfIsUpdatingTokenPool
  nil; pair
