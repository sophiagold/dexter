{-|
Module      : Dexter.Contract.TokenToToken
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.TokenToToken
  ( tokenToToken
  ) where

import Dexter.Contract.Core
  ( Parameter
  , ErrorMessages(..)
  , RuntimeValues(..)
  , Storage
  , TokenToTokenParams
  , XtzToTokenParams
  , failIfAmountIsNotZero
  , failIfSelfIsUpdatingTokenPool
  , mutezToNatural
  , naturalToMutez
  , transferOwnerTokensToDexter
  )

import Lorentz

-- | An address can exchange one FA1.2 token for another one if they provide
-- the address of a second dexter contract. 
tokenToToken
  :: (forall s1. RuntimeValues s1)
  -> Entrypoint TokenToTokenParams Storage
tokenToToken runtimeValues = do
  -- assert selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)
  -- assert amount == 0
  failIfAmountIsNotZero runtimeValues
  -- assert now < deadline
  getField #deadline; now; assertLt (deadlinePassed . errorMessages $ runtimeValues)
  -- assert tokensSold > 0
  getField #tokensSold; push @Natural 0; assertLt (tokensSoldIsZero . errorMessages $ runtimeValues)  
  -- assert minTokensBought > 0
  getField #minTokensBought; push @Natural 0; assertLt (minTokensBoughtIsZero . errorMessages $ runtimeValues)
  -- assert xtzPool > 0
  dip (do stGetField #xtzPool; push @Mutez zeroMutez; assertLt (xtzPoolIsZero . errorMessages $ runtimeValues))
  -- assert tokenPool > 0
  dip (do stGetField #tokenPool; push @Natural 0; assertLt (tokenPoolIsZero . errorMessages $ runtimeValues))

  -- xtzBought = (tokens_sold * 997n * storage.s.xtz_pool) / (storage.s.token_pool * 1000n + (tokens_sold * 997n))
  -- calculate denominator
  getField #tokensSold; push @Natural 997; mul
  duupX @3; stToField #tokenPool; push @Natural 1000; mul; add
  stackType @[Natural, TokenToTokenParams, Storage]  

  -- calculate numerator
  -- always convert mutez to natural before performing multiplication!
  duupX @2; toField #tokensSold; push @Natural 997; mul; duupX @4; stToField #xtzPool; mutezToNatural; mul
  stackType @[Natural, Natural, TokenToTokenParams, Storage]

  -- calculate XtzBought
  ediv; ifNone (do push (divByZero . errorMessages $ runtimeValues); failWith) car; naturalToMutez
  stackType @[Mutez, TokenToTokenParams, Storage]

  -- storage.tokenPool = tokenPool + tokensSold
  duupX @2; toField #tokensSold; duupX @4; stToField #tokenPool; add; dip (dig @2); stSetField #tokenPool
  stackType @[Storage, Mutez, TokenToTokenParams]

  -- storage.xtzPool = xtzPool - xtzBought
  stGetField #xtzPool; duupX @3; swap; sub; stSetField #xtzPool
  stackType @[Storage, Mutez, TokenToTokenParams]

  -- send xtzBought to to address
  duupX @3; toField #outputDexterContract; contractCalling @Parameter (Call @"XtzToToken"); ifNone failWith nop
  dig @2
  stackType @[Mutez, ContractRef XtzToTokenParams, Storage, TokenToTokenParams]

  -- construct XtzToTokenParams
  duupX @4;  
  constructT @XtzToTokenParams
    ( fieldCtor $ do getField #to; toNamed #to
    , fieldCtor $ do getField #minTokensBought; toNamed #minTokensBought
    , fieldCtor $ do getField #deadline; toNamed #deadline
    )
  dip drop

  stackType @[XtzToTokenParams, Mutez, ContractRef XtzToTokenParams, Storage, TokenToTokenParams]
  transferTokens
  stackType @[Operation, Storage, TokenToTokenParams]

  -- Storage : Natural : Address
  swap; dip (do swap; getField #tokensSold; dip (toField #owner))
  transferOwnerTokensToDexter
  stackType @[Operation, Storage, Operation]

  nil; swap; cons; dip swap; swap; cons; pair
