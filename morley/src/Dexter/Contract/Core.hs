{-|
Module      : Dexter.Contract.Types
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.Core where

import Lorentz
import Michelson.Typed   (Notes(..))
import Michelson.Untyped (ann, noAnn)
import Prelude           (Show, (<*>), (<$>))
import Michelson.Text    (mkMText)
import Tezos.Core        (mkMutez)

-- =============================================================================
-- Safe Type Synonyms
-- =============================================================================

-- These provide informative names for type synonyms while providing type safety
-- (cannot accidentally confuse two addresses that have different purposes)

-- | The owner of liquidity (XTZ and Token) that is held by dexter.
type Owner      = ("owner"     :! Address)

-- | An address that sent a request to dexter.
type Sender     = ("sender_"   :! Address)

-- | An address that may have rights to burn liquidty on behalf of an owner.
type Spender    = ("spender"   :! Address)

-- | An address that will receive XTZ or Token from dexter.
type Receiver   = ("receiver"  :! Address)

-- | Amount of Token held by dexter.
type TokenPool  = ("tokenPool" :! Natural)

-- | Amount of XTZ purchased by an address.
type XtzBought  = ("xtzBought" :! Mutez)

-- | Amount of XTZ held by dexter.
type XtzPool    = ("xtzPool"   :! Mutez)

-- | Amount of XTZ held by dexter as a natural number. Useful for some calculations.
type XtzPoolNat = ("xtzPoolNat"   :! Natural)


-- =============================================================================
-- Entrypoint Parameters
-- =============================================================================

type ApproveParams =
  ( "spender"          :! Spender -- the third party address that will be granted liquidity burn rights
  , "allowance"        :! Natural -- the amount the third party will be allowed to burn
  , "currentAllowance" :! Natural -- a confirmation of the spender's current amount, a security measure to 
  )

type AddLiquidityParams =
  ( "owner"              :! Owner     -- the address of whom will be accredited as the owner of the minted liquidity.
  , "minLqtMinted"       :! Natural   -- minimum amount of liquidity the sender will mint if totalLiquidity is greater than zero
  , "maxTokensDeposited" :! Natural   -- maximum number of tokens deposited. Deposits max amount if totalLiquidity is zero. If exceed the transaction will fail
  , "deadline"           :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type RemoveLiquidityParams =
  ( "owner"              :! Owner     -- the address of whose liquidity will be burned.
  , "to"                 :! Receiver  -- the address which will receive the withdrawn XTZ and token.
  , "lqtBurned"          :! Natural   -- the amount of lqt to burn (redeem).
  , "minXtzWithdrawn"    :! Mutez     -- the minimum amount of XTZ that to should receive. If not met the transaction will fail.
  , "minTokensWithdrawn" :! Natural   -- the minimum amount of Token that to should receive. If not met the transaction will fail.
  , "deadline"           :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type XtzToTokenParams =
  ( "to"               :! Receiver  -- the address which will receive the purchased token.
  , "minTokensBought"  :! Natural   -- the minimum amount of Token that to should receive. If not met the transaction will fail.
  , "deadline"         :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type TokenToXtzParams =
  ( "owner"        :! Owner     -- the address which is selling their token to Dexter.
  , "to"           :! Receiver  -- the address which will receive the purchased XTZ.
  , "tokensSold"   :! Natural   -- the amount of Tokens sold.
  , "minXtzBought" :! Mutez     -- minimum amount of XTZ the sender wants to purchase, if not met the transaction will fail.
  , "deadline"     :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type TokenToTokenParams =
  ( "outputDexterContract" :! Address   -- address of other dexter contract of token they want to receive
  , "minTokensBought"      :! Natural
  , "owner"                :! Owner     -- the address which is selling their token to Dexter.
  , "to"                   :! Receiver  -- the address which will receive the purchased XTZ.
  , "tokensSold"           :! Natural   -- the amount of Tokens sold.
  , "deadline"             :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type SetBakerParams =
  ( Maybe KeyHash -- the address of the baker
  , Bool          -- if set to True than the baker can never be changed again
  )

type UpdateTokenPoolInternalParams =
  Natural -- the total amount of Token owned by the Dexter contract

-- =============================================================================
-- Contract Parameter
-- =============================================================================

data Parameter
  = Approve         ApproveParams
  | AddLiquidity    AddLiquidityParams
  | RemoveLiquidity RemoveLiquidityParams
  | XtzToToken      XtzToTokenParams
  | TokenToXtz      TokenToXtzParams  
  | TokenToToken    TokenToTokenParams  
  | UpdateTokenPool KeyHash
  | UpdateTokenPoolInternal UpdateTokenPoolInternalParams
  | SetBaker        SetBakerParams
  | SetManager      Address
  | Default         ()
  deriving (Generic, IsoValue)

instance ParameterHasEntryPoints Parameter where
  type ParameterEntryPointsDerivation Parameter = EpdPlain

-- =============================================================================
-- Contract Storage
-- =============================================================================

type Approvals = Map Address Natural

type Account =
  ( "balance"   :! Natural
  , "approvals" :! Approvals
  )

data StorageFields =
  StorageFields
    { selfIsUpdatingTokenPool :: Bool
    , freezeBaker             :: Bool
    , lqtTotal                :: Natural
    , manager                 :: Address
    , tokenAddress            :: Address
    , tokenPool               :: Natural
    , xtzPool                 :: Mutez
    } deriving (Generic, IsoValue, Eq, Show, HasAnnotation)

instance HasFieldOfType StorageFields name field => StoreHasField StorageFields name field where
  storeFieldOps = storeFieldOpsADT

instance TypeHasDoc StorageFields where
  typeDocMdDescription = "Dexter storage fields."
  typeDocHaskellRep = homomorphicTypeDocHaskellRep
  typeDocMichelsonRep = homomorphicTypeDocMichelsonRep

data Storage =
  Storage
    { accounts :: BigMap Address Account
    , fields   :: StorageFields
    } deriving (Generic, IsoValue, Eq, Show, HasAnnotation)

instance (StoreHasField StorageFields fname ftype) => StoreHasField Storage fname ftype where
  storeFieldOps = storeFieldOpsDeeper #fields

instance StoreHasSubmap Storage "accounts" Address Account where
  storeSubmapOps = storeSubmapOpsDeeper #accounts

instance TypeHasDoc Storage where
  typeDocMdDescription = "Dexter storage."
  typeDocHaskellRep = homomorphicTypeDocHaskellRep
  typeDocMichelsonRep = homomorphicTypeDocMichelsonRep

-- | Manually annotate the names of values in Dexter Storage since morley cannot
-- do it yet
storageNotes :: Notes (ToT Storage)
storageNotes =
  NTPair noAnn (ann "accounts") noAnn
    (NTBigMap
     noAnn
     (NTAddress $ ann "owner")
     (NTPair noAnn noAnn noAnn
      (NTNat $ ann "balance")
      (NTMap noAnn (NTAddress $ ann "spender") (NTNat $ ann "allowance"))
     )
    )
    (NTPair noAnn noAnn noAnn
      (NTPair noAnn noAnn noAnn
       (NTBool $ ann "selfIsUpdatingTokenPool")
       (NTPair noAnn noAnn noAnn
        (NTBool $ ann "freezeBaker")
        (NTNat $ ann "lqtTotal")
       )
      )
      (NTPair noAnn noAnn noAnn
       (NTPair noAnn noAnn noAnn
        (NTAddress $ ann "manager")
        (NTAddress $ ann "tokenAddress")
       )
       (NTPair noAnn noAnn noAnn
        (NTNat $ ann "tokenPool")
        (NTMutez $ ann "xtzPool")
       )
      )
    )

-- =============================================================================
-- FA1.2
-- =============================================================================

-- (address :from, (address :to, nat :value))    %transfer
type TransferParams   = (Address, (Address, Natural))

-- (address :owner, contract nat)              %getBalance
type GetBalanceParams = (Address, ContractRef Natural)

data FA12
  = Transfer   TransferParams
  | GetBalance GetBalanceParams
  deriving (Generic, IsoValue)

instance ParameterHasEntryPoints FA12 where
  type ParameterEntryPointsDerivation FA12 = EpdPlain

-- =============================================================================
-- functions
-- =============================================================================

data ErrorMessages =
  ErrorMessages
    { amountIsNotZero                          :: MText
    , amountIsZero                             :: MText    
    , bakerIsFrozen                            :: MText
    , currentAllowanceNotEqual                 :: MText
    , deadlinePassed                           :: MText
    , divByZero                                :: MText
    , initialLiquidityIsLessThanOneTez         :: MText
    , lqtBurnedIsGreaterThanOwnersBalance      :: MText
    , lqtBurnedIsZero                          :: MText
    , lqtMintedIsLessThanMin                   :: MText
    , maxTokensDepositedIsZero                 :: MText
    , minLqtMintedIsZero                       :: MText
    , minTokensBoughtIsZero                    :: MText    
    , minTokensWithdrawnIsZero                 :: MText
    , minTokensWithdrawLessThanMin             :: MText
    , minXtzBoughtIsZero                       :: MText
    , minXtzWithdrawnIsZero                    :: MText    
    , notSelfIsUpdatingTokenPool               :: MText
    , notCalledFromToken                       :: MText
    , ownerHasNoLiquidity                      :: MText
    , selfIsUpdatingTokenPoolIsNotFalse        :: MText
    , senderApprovalBalanceIsLessThanLqtBurned :: MText
    , senderHasNoApprovalBalance               :: MText
    , senderIsNotContractManager               :: MText
    , tokensBoughtLessThanMin                  :: MText
    , tokensDepositedIsZero                    :: MText    
    , tokensDepositedIsGreaterThanMax          :: MText
    , tokenPoolIsZero                          :: MText
    , tokensSoldIsZero                         :: MText
    , unsafeUpdateTokenPool                    :: MText
    , xtzBoughtLessThanMin                     :: MText
    , xtzIsZero                                :: MText
    , xtzPoolIsZero                            :: MText
    , xtzWithdrawnIsLessThanMin                :: MText
    }

emptyErrorMessages :: ErrorMessages
emptyErrorMessages =
  ErrorMessages mempty mempty mempty mempty mempty mempty mempty mempty mempty
  mempty mempty mempty mempty mempty mempty mempty mempty mempty mempty mempty
  mempty mempty mempty mempty mempty mempty mempty mempty mempty mempty mempty
  mempty mempty mempty

defaultErrorMessages :: Either Text ErrorMessages
defaultErrorMessages =
  ErrorMessages
    <$> mkMText "Amount must be zero."
    <*> mkMText "Amount must be greater than zero."    
    <*> mkMText "Cannot change the baker while freezeBaker is true."
    <*> mkMText "The current allowance parameter must equal the sender's current allowance for the owner."
    <*> mkMText "NOW is greater than deadline."
    <*> mkMText "divByZero."
    <*> mkMText "The initial liquidity amount must be greater than or equal to 1 XTZ."
    <*> mkMText "lqtBurned is greater than owner's balance."
    <*> mkMText "lqtBurned must be greater than zero."
    <*> mkMText "lqtMinted must be greater than or equal to minLqtMinted."
    <*> mkMText "maxTokensDeposited must be greater than zero."
    <*> mkMText "minLqtMinted must be greater than zero."
    <*> mkMText "minTokensBought must be greater than zero."
    <*> mkMText "minTokensWithdrawn must be greater than zero."
    <*> mkMText "tokensWithdrawn is less than minTokensWithdrawn."
    <*> mkMText "minXtzBought must be greater than zero."
    <*> mkMText "minXtzWithdrawn must be greater than zero."    
    <*> mkMText "Dexter did not initiate this operation."
    <*> mkMText "The sender is not the token contract associated with this contract."
    <*> mkMText "owner has no liquidity."
    <*> mkMText "selfIsUpdatingToken must be false."    
    <*> mkMText "sender approval balance is less than LQT burned."
    <*> mkMText "sender has no approval balance."
    <*> mkMText "sender is not the contract manager."
    <*> mkMText "tokensBought is less than minTokensBought."
    <*> mkMText "tokensDeposited is zero."
    <*> mkMText "tokensDeposited is greater than maxTokensDeposited."
    <*> mkMText "tokenPool must be greater than zero"
    <*> mkMText "tokensSold is zero"    
    <*> mkMText "unsafeUpdateTokenPool"
    <*> mkMText "xtzBought is less than minXtzBought."
    <*> mkMText "The amount of XTZ sent to Dexter must be greater than zero."
    <*> mkMText "xtzPool must be greater than zero."
    <*> mkMText "xtzWithdrawn is less than minXtzWithdrawn."

defaultMutezValues :: Maybe MutezValues
defaultMutezValues =
  MutezValues <$> mkMutez 1000000

newtype MutezValues =
  MutezValues
    { oneTez :: Mutez
    }

data RuntimeValues s =
  RuntimeValues
    { mutezValues    :: MutezValues
    , errorMessages  :: ErrorMessages
    }

defaultRuntimeValues :: (forall s. Maybe (RuntimeValues s))
defaultRuntimeValues =
  case (defaultMutezValues, defaultErrorMessages) of
    (Just mutezValues', Right errorMessages') ->
      Just $ RuntimeValues mutezValues' errorMessages'
    _ -> Nothing

-- =============================================================================
-- Shared lorentz functions
-- =============================================================================

-- | Convert Mutez to Natural. Should not fail because the range of Natural is
-- larger than Mutez.
mutezToNatural :: Mutez : s :-> Natural : s
mutezToNatural = do
  dip (push @Mutez oneMutez); ediv; ifNone (do push @MText mempty; failWith) car

-- | Convert Natural to Mutez. This will fail if Natural is larger than the
-- acceptable Mutez range.
naturalToMutez :: Natural : s :-> Mutez : s
naturalToMutez = push @Mutez oneMutez >> mul

-- | Convert Integer to Natural. Will fail if Integer is a negative number.
intToNatural :: Integer : s :-> Natural : s
intToNatural = do
  dup; push @Integer 0; le; if_ abs (do push @MText mempty; failWith)

-- | get an owner's account, if the owner does not have one, give them an
-- empty account
getAccount :: Owner : Storage : s :-> Account : Storage : s
getAccount = do
  fromNamed #owner; dip (getField #accounts); get
  ifNone
    (constructT @Account
       ( fieldCtor $ push 0 >> toNamed #balance
       , fieldCtor $ emptyMap >> toNamed #approvals
       )
    )
    nop

toAccount :: Owner : Storage : s :-> Account : s
toAccount = do
  fromNamed #owner; dip (toField #accounts); get
  ifNone
    (constructT @Account
       ( fieldCtor $ push 0 >> toNamed #balance
       , fieldCtor $ emptyMap >> toNamed #approvals
       )
    )
    nop

-- | ceilDiv
ceilDiv
  :: forall s.
  (forall s1. RuntimeValues s1)
  -> Natural : Natural : s :-> Natural : s
ceilDiv runtimeValues = do
  ediv;
  ifNone
    (do push (divByZero . errorMessages $ runtimeValues); failWith)
    (do unpair; swap; int; if IsZero then nop else (do push @Natural 1; add))

-- | get an owner's LQT balance
getBalance :: Owner : Storage : s :-> Natural : Storage : s
getBalance = getAccount >> toField #balance

-- | Transfer FA1.2 tokens owned by the sender from to Dexter
transferOwnerTokensToDexter :: Storage : Natural : Owner : s :-> Operation : Storage : s
transferOwnerTokensToDexter = do
  -- get FA1.2 address from storage
  stGetField #tokenAddress; contractCalling @FA12 (Call @"Transfer"); ifNone failWith nop

  -- send 0 mutez to the FA1.2 contract
  push @Mutez zeroMutez
  stackType @( Mutez : ContractRef TransferParams : Storage : Natural : Owner : _)
  -- (from, (to, value))
  dig @3; selfCalling @Parameter CallDefault; address; dig @5; fromNamed #owner; dip pair; pair
  transferTokens

-- | Entrypoints that are not meant to receive XTZ should fail if XTZ is sent.
failIfAmountIsNotZero
  :: forall s.
  (forall s1. RuntimeValues s1)
  -> s :-> s
failIfAmountIsNotZero runtimeValues = do
  push zeroMutez; amount; assertEq (amountIsNotZero . errorMessages $ runtimeValues) 

-- | Entrypoints that are not meant to receive XTZ should fail if XTZ is sent.
failIfAmountIsZero
  :: forall s.
  (forall s1. RuntimeValues s1)
  -> s :-> s
failIfAmountIsZero runtimeValues = do
  push zeroMutez; amount; assertGt (amountIsZero . errorMessages $ runtimeValues) 

-- | assert that a value is False
assertFalse :: IsError err => err -> Bool & s :-> s
assertFalse reason = if_ (failUsing reason) nop

-- | Entrypoints other than UpdateTokenPool and UpdateTokenPoolInternal should
-- fail if selfIsUpdatingTokenPool is true.
failIfSelfIsUpdatingTokenPool
  :: forall s.
  (forall s1. RuntimeValues s1)
  -> Storage : s :-> Storage : s
failIfSelfIsUpdatingTokenPool runtimeValues = do
  stGetField #selfIsUpdatingTokenPool; assertFalse (selfIsUpdatingTokenPoolIsNotFalse . errorMessages $ runtimeValues) 
  
