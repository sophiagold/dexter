{-|
Module      : Dexter.Contract.RemoveLiquidity
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.RemoveLiquidity where

import Dexter.Contract.Core
import Lorentz

type XtzWithdrawn    = Mutez
type LqtBalance      = Natural
type NewLqtBalance   = Natural
type TokensWithdrawn = Natural

-- | An address redeem liquidity it has provided. The exchange rate of XTZ to
-- Token fluctuates with the ratio of xtzPool to tokenPool. The owner of
-- liquidty can burn its internal dexter liquidy tokens to redeem an equivalent
-- amount of XTZ and Token.
removeLiquidity ::
  (forall s1. RuntimeValues s1) ->
  Entrypoint RemoveLiquidityParams Storage
removeLiquidity runtimeValues = do
  -- assert that selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)
  
  removeLiquidityAndPrepOperations runtimeValues
  -- run operations

  -- send xtz
  duupX @4; toField #to; fromNamed #receiver; Lorentz.contract @(); ifNone failWith nop; dig @3; push (); transferTokens
  -- nil; swap; cons
  stackType @[Operation, Storage, TokensWithdrawn, RemoveLiquidityParams]
  
  -- transfer tokens
  dip (do dip (dip (toField #to)); transferDexterTokensToReceiver);
  stackType @[Operation, Operation, Storage]
  nil; swap; cons; swap; cons; pair

-- | removeLiquidity without operations
removeLiquidityAndPrepOperations ::
  (forall s1. RuntimeValues s1) ->
  RemoveLiquidityParams : Storage : s :-> Storage : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams : s
removeLiquidityAndPrepOperations runtimeValues = do
  removeLiquidityInitialAsserts runtimeValues

  getOwnerBalanceCheckAndUpdateSenderAllowance runtimeValues
  stackType @(LqtBalance : RemoveLiquidityParams : Storage :_)

  -- xtzWithdrawn = lqtBurned * xtzPool / lqtTotal
  duupX @2; toField #lqtBurned; duupX @4; stToField #xtzPool; mutezToNatural; mul;
  dip (do duupX @3; stToField #lqtTotal); ediv; ifNone (do push (divByZero . errorMessages $ runtimeValues); failWith) car; naturalToMutez
  stackType @(XtzWithdrawn : LqtBalance : RemoveLiquidityParams : Storage: _)

  -- xtzWithdrawn >= minXtzWithdrawn
  dup; duupX @4; toField #minXtzWithdrawn; assertLe (xtzWithdrawnIsLessThanMin . errorMessages $ runtimeValues)
  stackType @(XtzWithdrawn : LqtBalance : RemoveLiquidityParams : Storage: _)

  -- tokensWithdrawn = lqtBurned * tokenPool / lqtTotal
  duupX @3; toField #lqtBurned; dip (do duupX @4; stGetField #lqtTotal; swap; stToField #tokenPool);
  mul; ediv; ifNone (do push (divByZero . errorMessages $ runtimeValues); failWith) car
  stackType @(TokensWithdrawn : XtzWithdrawn : LqtBalance : RemoveLiquidityParams : Storage: _)

  -- tokenWithdrawn >= minTokensWithdrawn
  dup; duupX @5; toField #minTokensWithdrawn; assertLe (minTokensWithdrawLessThanMin . errorMessages $ runtimeValues)
  stackType @(TokensWithdrawn : XtzWithdrawn : LqtBalance : RemoveLiquidityParams : Storage: _)

  -- NewBalance = LqtBalance - LqtBurned
  duupX @4; toField #lqtBurned; dig @3; sub; intToNatural
  stackType @(NewLqtBalance : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams : Storage: _)

  -- set owner's newBalance
  dig @4; dup; duupX @6; toField #owner; toAccount
  stackType @(Account : Storage : NewLqtBalance : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams : _)
  dig @2; setField #balance;
  stackType @(Account : Storage : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams : _)
  dip (getField #accounts); some; duupX @6; toField #owner; fromNamed #owner; update; setField #accounts
  stackType @(Storage : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams: _)
  
  -- newLqtTotal = lqtTotal - LqtBurned
  stGetField #lqtTotal; duupX @5; toField #lqtBurned; swap; sub; intToNatural; stSetField #lqtTotal
  stackType @(Storage : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams : _)

  -- newTokenPool = tokenPool - tokensWithdrawn
  stGetField #tokenPool; duupX @3; swap; sub; intToNatural; stSetField #tokenPool
  
  -- storage.xtzPool = storage.xtzPool - xtzWithdrawn
  stGetField #xtzPool; duupX @4; swap; sub; stSetField #xtzPool

-- | Initial asserts for removeLiquidity
removeLiquidityInitialAsserts
  :: forall s.
  (forall s1. RuntimeValues s1)
  -> RemoveLiquidityParams : Storage : s :-> RemoveLiquidityParams : Storage : s
removeLiquidityInitialAsserts runtimeValues = do
  -- now < deadline
  getField #deadline; now; assertLt (deadlinePassed . errorMessages $ runtimeValues)
  -- amount == 0
  failIfAmountIsNotZero runtimeValues  
  -- minXtzWithdrawn > 0
  getField #minXtzWithdrawn; push @Mutez zeroMutez; assertLt (minXtzWithdrawnIsZero . errorMessages $ runtimeValues)
  -- minTokensWithdrawn > 0
  getField #minTokensWithdrawn; push @Natural 0; assertLt (minTokensWithdrawnIsZero . errorMessages $ runtimeValues)
  -- lqtBurned > 0
  getField #lqtBurned; push @Natural 0; assertLt (lqtBurnedIsZero . errorMessages $ runtimeValues)
    
getOwnerBalanceCheckAndUpdateSenderAllowance
  :: forall s.
  (forall s1. RuntimeValues s1)
  -> RemoveLiquidityParams : Storage : s :-> Natural : RemoveLiquidityParams : Storage : s
getOwnerBalanceCheckAndUpdateSenderAllowance runtimeValues = do
  getField #owner; fromNamed #owner; dip (do duupX @2; toField #accounts); get
  ifNone (do push (ownerHasNoLiquidity . errorMessages $ runtimeValues); failWith) nop;
  stackType @(Account : RemoveLiquidityParams : Storage : _)
 
  -- assert that lqtBurned does not exceed the owner's balance
  swap; getField #lqtBurned; duupX @3; toField #balance; assertGe (lqtBurnedIsGreaterThanOwnersBalance . errorMessages $ runtimeValues)
  stackType @(RemoveLiquidityParams : Account : Storage : _)

  -- check if the sender is the owner
  getField #owner; fromNamed #owner; sender; eq;
  if_
    (do -- sender is owner, get the their liquidity balance
        stackType @(RemoveLiquidityParams : Account : Storage : _)
        swap; toField #balance
    )
    (do -- the sender is not the owner
        -- get their allowance
        stackType @(RemoveLiquidityParams : Account : Storage : _)
        swap; getField #approvals; sender; get;
        ifNone (do push (senderHasNoApprovalBalance . errorMessages $ runtimeValues); failWith) nop;
        -- approved amount for sender is on the top
        stackType @(Natural : Account : RemoveLiquidityParams : Storage : _)
        -- check that allowances is greater than or equal to lqtBurned;
        dip (do swap; getField #lqtBurned; dip (push @Integer 0)); sub; dup
        stackType @(Integer : Integer : Integer : RemoveLiquidityParams : Account : Storage : _)        
        dip (assertGe (senderApprovalBalanceIsLessThanLqtBurned . errorMessages $ runtimeValues)); abs
        stackType @(Natural : RemoveLiquidityParams : Account : Storage : _)

        -- update sender amount in approval to their balance minus liquidity burned
        some; dig @2; getField #approvals; dig @2; sender; update; setField #approvals
        stackType @(Account : RemoveLiquidityParams : Storage : _)

        getField #balance; dip (do some; dug @1; getField #owner; fromNamed #owner; swap)
        stackType @(Natural : RemoveLiquidityParams : Address : Maybe Account : Storage : _)
        dip (dip (do dip (dip (getField #accounts)); update; setField #accounts))
    )

-- | Transfer FA1.2 tokens from Dexter to a Receiver.
transferDexterTokensToReceiver :: Storage : Natural : Receiver : s :-> Operation : Storage : s
transferDexterTokensToReceiver = do
  -- get FA1.2 address from storage
  stGetField #tokenAddress; contractCalling @FA12 (Call @"Transfer"); ifNone failWith nop

  -- send 0 mutez to the FA1.2 contract
  push @Mutez zeroMutez
  stackType @( Mutez : ContractRef TransferParams : Storage : Natural : Receiver : _)
  -- (from, (to, value))
  dig @3; dig @4; fromNamed #receiver; pair; selfCalling @Parameter CallDefault; address; pair
  transferTokens
