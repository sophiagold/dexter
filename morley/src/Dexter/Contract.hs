{-|
Module      : Dexter.Contract
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract
  ( module Dexter.Contract
  , module Dexter.Contract.Core    
  , module Dexter.Contract.Approve
  , module Dexter.Contract.AddLiquidity
  , module Dexter.Contract.RemoveLiquidity
  , module Dexter.Contract.TokenToXtz
  , module Dexter.Contract.XtzToToken
  , module Dexter.Contract.TokenToToken
  , module Dexter.Contract.UpdateTokenPool    
  ) where

import Dexter.Contract.Approve
import Dexter.Contract.AddLiquidity
import Dexter.Contract.RemoveLiquidity
import Dexter.Contract.TokenToXtz
import Dexter.Contract.XtzToToken
import Dexter.Contract.TokenToToken
import Dexter.Contract.UpdateTokenPool

import Dexter.Contract.Core

import Lorentz

-- =============================================================================
-- Contract Code
-- =============================================================================

contractCode
  :: (forall s1. RuntimeValues s1)
  -> ContractCode Parameter Storage
contractCode runtimeValues = do
  unpair
  entryCaseSimple @Parameter
    ( #cApprove         /-> approve         runtimeValues
    , #cAddLiquidity    /-> addLiquidity    runtimeValues
    , #cRemoveLiquidity /-> removeLiquidity runtimeValues
    , #cXtzToToken      /-> xtzToToken      runtimeValues
    , #cTokenToXtz      /-> tokenToXtz      runtimeValues
    , #cTokenToToken    /-> tokenToToken    runtimeValues
    , #cUpdateTokenPool /-> updateTokenPool runtimeValues
    , #cUpdateTokenPoolInternal /-> updateTokenPoolInternal runtimeValues
    , #cSetBaker        /-> setBaker        runtimeValues
    , #cSetManager      /-> setManager      runtimeValues
    , #cDefault         /-> default_        runtimeValues
    )

contract
  :: (forall s1. RuntimeValues s1)
  -> Contract Parameter Storage
contract runtimeValues =
  defaultContract $ contractName "Dexter" $ do
    contractGeneralDefault
    docStorage @Storage
    -- doc $ DDescription contractDoc
    contractCode runtimeValues

-- | The contract manager can set the baker as long as freezeBaker is False.
-- This is the current solution until the virtual baker is available on Tezos.
-- When it is, the manager should set freezeBaker to True so they no longer have
-- control over who the baker is.
setBaker
  :: (forall s1. RuntimeValues s1)
  -> Entrypoint SetBakerParams Storage
setBaker runtimeValues = do
  -- assert selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)
  -- assert amount == 0
  failIfAmountIsNotZero runtimeValues
  
  -- assert that the sender is the manager
  duupX @2; stToField #manager; sender; assertEq (senderIsNotContractManager . errorMessages $ runtimeValues)
  stackType @[SetBakerParams, Storage]

  -- assert that changing the baker is not frozen
  duupX @2; stToField #freezeBaker; not; assert (bakerIsFrozen . errorMessages $ runtimeValues)
  stackType @[SetBakerParams, Storage]

  -- set the delegate
  unpair; setDelegate
  stackType @[Operation, Bool, Storage]
  
  -- set the freezeBaker value
  dip (do stSetField #freezeBaker; nil)
  stackType @[Operation, [Operation], Storage]
  cons; pair

setManager
  :: (forall s1. RuntimeValues s1)
  -> Entrypoint Address Storage
setManager runtimeValues = do
  -- assert selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)
  -- assert amount == 0
  failIfAmountIsNotZero runtimeValues
  
  -- assert that the sender is the manager
  duupX @2; stToField #manager; sender; assertEq (senderIsNotContractManager . errorMessages $ runtimeValues)
  stackType @[Address, Storage]

  stSetField #manager
  nil; pair
  

-- | Allow the baker to send staking rewards to the Dexter contract.
default_ :: (forall s1. RuntimeValues s1) -> Entrypoint () Storage
default_ runtimeValues = do  
  drop

  -- assert selfIsUpdatingTokenPool == False
  failIfSelfIsUpdatingTokenPool runtimeValues

  -- dexter is responsible for keeping track of how much XTZ it has
  stGetField #xtzPool; amount; add; stSetField #xtzPool
  nil; pair
