{-|
Module      : Main
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io

-}

module Main where

import qualified Dexter.Contract            as Dexter
import Test.Dexter.Contract.AddLiquidity    as AddLiquidity 
import Test.Dexter.Contract.Approve         as Approve
import Test.Dexter.Contract.Core            as Core
import Test.Dexter.Contract.Default         as Default
import Test.Dexter.Contract.RemoveLiquidity as RemoveLiquidity
import Test.Dexter.Contract.SetBaker        as SetBaker
import Test.Dexter.Contract.SetManager      as SetManager
import Test.Dexter.Contract.TokenToToken    as TokenToToken
import Test.Dexter.Contract.TokenToXtz      as TokenToXtz
import Test.Dexter.Contract.UpdateTokenPool as UpdateTokenPool
import Test.Dexter.Contract.UpdateTokenPoolInternal as UpdateTokenPoolInternal
import Test.Dexter.Contract.XtzToToken      as XtzToToken

       
import Test.Hspec                              (hspec, parallel)

main :: IO ()
main = do
  case (Dexter.defaultMutezValues, Dexter.defaultErrorMessages) of
    (Just mutezValues, Right errorMessages) -> do            
      let runtimeValues =
            Dexter.RuntimeValues mutezValues errorMessages

      hspec $ parallel $ AddLiquidity.spec runtimeValues      
      hspec $ parallel $ Approve.spec runtimeValues
      hspec $ parallel $ Core.spec      
      hspec $ parallel $ Default.spec runtimeValues
      hspec $ parallel $ RemoveLiquidity.spec runtimeValues
      hspec $ parallel $ SetBaker.spec runtimeValues
      hspec $ parallel $ SetManager.spec runtimeValues
      hspec $ parallel $ TokenToToken.spec runtimeValues
      hspec $ parallel $ TokenToXtz.spec runtimeValues
      hspec $ parallel $ UpdateTokenPool.spec runtimeValues
      hspec $ parallel $ UpdateTokenPoolInternal.spec runtimeValues
      hspec $ parallel $ XtzToToken.spec runtimeValues
            
    _ -> fail "Unable to make runtime values"
