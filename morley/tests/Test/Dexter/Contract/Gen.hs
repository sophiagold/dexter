{-|
Module      : Test.Dexter.Contract.Gen
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io

Generation functions that producte Test.QuickCheck.Gen values for common types.

-}

{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE ViewPatterns           #-}

module Test.Dexter.Contract.Gen where

import Test.QuickCheck (Gen, choose)

import Tezos.Core (Mutez, unsafeMkMutez)

genSmallAmount :: Gen Natural
genSmallAmount = fromInteger . ((*) 1000000) <$> choose (0, 10)

genMidAmount :: Gen Natural
genMidAmount = fromInteger . ((*) 1000000) <$> choose (100, 1000)

genWord32 :: Gen Word32
genWord32 = fromInteger . ((*) 1000000) <$> choose (1, 100000)

genMutez :: Gen Mutez
genMutez = fmap unsafeMkMutez $ choose (0, 9223372036854775807)

genNonZeroMutez :: Gen Mutez
genNonZeroMutez = fmap unsafeMkMutez $ choose (1, 9223372036854775807)

genOneTezOrGreater :: Gen Mutez
genOneTezOrGreater = fmap unsafeMkMutez $ choose (1000000, 9223372036854775807)
