{-|
Module      : Test.Dexter.Contract.XtzToToken
Copyright   : (c) camlCase, 2019-2020
Maintainer  : james@camlcase.io

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.XtzToToken where

-- general
import qualified Data.Map.Strict   as Map
import           Util.Named ((.!))

-- lorentz and morley
import           Lorentz
import           Lorentz.Test
import           Lorentz.Test.Extension
import           Michelson.Interpret  (ceAmount, ceBalance, ceNow)
import           Michelson.Test.Dummy (dummyNow)
import           Tezos.Core           (unMutez, unsafeMkMutez, timestampPlusSeconds, timestampToSeconds)

-- testing
import           Test.Hspec      (Spec, it, describe, shouldBe)
import           Test.QuickCheck (Gen, choose, forAll)

-- fa1.2
import qualified FaOnePointTwo.Contract  as FA
import qualified FaOnePointTwo.Test      as FA

-- dexter
import qualified Dexter.Contract           as Dexter
import qualified Dexter.Contract.Test      as Dexter
import qualified Test.Dexter.Contract.Mock as Mock
import qualified Test.Dexter.Contract.Gen  as Gen

spec :: (forall s. Dexter.RuntimeValues s) -> Spec
spec runtimeValues = do
  describe "xtzToToken entrypoint" $ do
    it "calculateTokensBought" $ do
      let runCalculateTokensBought params storage env = do 
            let initStack = (Identity params :& Identity storage :& RNil)
            resStack <- interpretLorentzInstr env (Dexter.calculateTokensBought runtimeValues) initStack
            let Identity res :& Identity _ :& Identity _ :& _ = resStack
            return res
          
          amount_  = unsafeMkMutez 1000000
          balance_ = unsafeMkMutez 1001000000
          later = dummyNow `timestampPlusSeconds` 100
          params0 = ( #to .! (#receiver .! Mock.alice), #minTokensBought 1, #deadline .! later)
          env0 = dummyContractEnv { ceAmount = amount_, ceBalance = balance_, ceNow = dummyNow }
          storage0 =
            Dexter.setLqtTotal 1000000000 .
            Dexter.setTokenPool 10000 $
            Dexter.setXtzPool balance_ $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)

      runCalculateTokensBought params0 storage0 env0 `shouldBe` Right 9

      let xtzIn1   = 1000000
          xtzPool1 = 1000000000
          env1 = dummyContractEnv { ceAmount = unsafeMkMutez xtzIn1, ceBalance = unsafeMkMutez $ xtzIn1 + xtzPool1, ceNow = dummyNow }
          storage1 =
            Dexter.setLqtTotal 1000000000 .
            Dexter.setTokenPool 250000 $
            Dexter.setXtzPool (unsafeMkMutez $ xtzPool1) $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)            
      runCalculateTokensBought params0 storage1 env1 `shouldBe` Right 249

      let xtzIn2   = 2000000
          xtzPool2 = 1000000000
          env2 = dummyContractEnv { ceAmount = unsafeMkMutez xtzIn2, ceBalance = unsafeMkMutez $ xtzIn2 + xtzPool2, ceNow = dummyNow }
          storage2 =
            Dexter.setLqtTotal 1000000000 .
            Dexter.setTokenPool 250000 $
            Dexter.setXtzPool (unsafeMkMutez $ xtzPool2) $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)            
      runCalculateTokensBought params0 storage2 env2 `shouldBe` Right 497

      let xtzIn3   = 5000000
          xtzPool3 = 1000000000
          env3 = dummyContractEnv { ceAmount = unsafeMkMutez xtzIn3, ceBalance = unsafeMkMutez $ xtzIn3 + xtzPool3, ceNow = dummyNow }
          storage3 =
            Dexter.setLqtTotal 1000000000 .
            Dexter.setTokenPool 250000 $
            Dexter.setXtzPool (unsafeMkMutez $ xtzPool3) $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)            
      runCalculateTokensBought params0 storage3 env3 `shouldBe` Right 1240

      let xtzIn4   = 10000000
          xtzPool4 = 1000000000
          env4 = dummyContractEnv { ceAmount = unsafeMkMutez xtzIn4, ceBalance = unsafeMkMutez $ xtzIn4 + xtzPool4, ceNow = dummyNow }
          storage4 =
            Dexter.setLqtTotal 1000000000 .
            Dexter.setTokenPool 250000 $
            Dexter.setXtzPool (unsafeMkMutez $ xtzPool4) $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)
          
      runCalculateTokensBought params0 storage4 env4 `shouldBe` Right 2467

      let xtzIn5   = 1000000
          xtzPool5 = 100000000
          env5 = dummyContractEnv { ceAmount = unsafeMkMutez xtzIn5, ceBalance = unsafeMkMutez $ xtzIn5 + xtzPool5, ceNow = dummyNow }
          storage5 =
            Dexter.setLqtTotal 1000000000 .
            Dexter.setTokenPool 100000 $
            Dexter.setXtzPool (unsafeMkMutez $ xtzPool5) $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)
          
      runCalculateTokensBought params0 storage5 env5 `shouldBe` Right 987

      let xtzIn6   = 10000000
          xtzPool6 = 100000000
          env6 = dummyContractEnv { ceAmount = unsafeMkMutez xtzIn6, ceBalance = unsafeMkMutez $ xtzIn6 + xtzPool6, ceNow = dummyNow }
          storage6 =
            Dexter.setLqtTotal 1000000000 .
            Dexter.setTokenPool 100000 $
            Dexter.setXtzPool (unsafeMkMutez $ xtzPool6) $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)
          
      runCalculateTokensBought params0 storage6 env6 `shouldBe` Right 9066

      let xtzIn7   = 1000000
          xtzPool7 = 10000000
          env7 = dummyContractEnv { ceAmount = unsafeMkMutez xtzIn7, ceBalance = unsafeMkMutez $ xtzIn7 + xtzPool7, ceNow = dummyNow }
          storage7 =
            Dexter.setLqtTotal 1000000000 .
            Dexter.setTokenPool 250000 $
            Dexter.setXtzPool (unsafeMkMutez $ xtzPool7) $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)
          
      runCalculateTokensBought params0 storage7 env7 `shouldBe` Right 22665

    it "PROP-XTT-000: If now > deadline, this operation will fail." $ do
      forAll (genTestDataWithDeadlineLessThanNow dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testXtzToToken runtimeValues testData `catchExpectedError` lExpectError (== [mt|NOW is greater than deadline.|])

    it "PROP-XTT-001: If min_tokens_bought == 0, this operation will fail." $ do
      forAll (genTestDataWithMinTokensBoughtZero dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testXtzToToken runtimeValues testData `catchExpectedError` lExpectError (== [mt|minTokensBought must be greater than zero.|])

    it "PROP-XTT-002: If tokens_bought < min_tokens_bought, this operation will fail." $ do
      forAll (genTestDataWithTokensBoughtLessThanMin dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testXtzToToken runtimeValues testData `catchExpectedError` lExpectError (== [mt|tokensBought is less than minTokensBought.|])

    it "PROP-XTT-003: If xtz_pool or token_pool is zero, this operation will fail." $ do
      forAll (getTestDataWithSkipAddLiquidity dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testXtzToToken runtimeValues testData `catchExpectedError` lExpectError (== [mt|xtzPool must be greater than zero.|])              

    it "PROP-XTT-004: If now < deadline, min_tokens_bought > 0, min_tokens_bought <= tokens_bought, then storage will be updated such that xtz_pool += amount, token_pool -= tokens_bought and tokens_bought will be transferred to the to address." $ do
      forAll (genTestData dummyNow) $ \testData ->
        integrationalTestProperty $ do
          (dexter, dexterStorage, fa, faStorage) <- testXtzToToken runtimeValues testData
          lExpectStorageConst dexter dexterStorage
          lExpectStorageConst fa faStorage


-- =============================================================================
-- Test functions
-- =============================================================================

testXtzToToken
  :: (forall s. Dexter.RuntimeValues s)
  -> TestData
  -> IntegrationalScenarioM (TAddress Dexter.Parameter, Dexter.Storage, TAddress FA.Parameter, FA.Storage)
testXtzToToken runtimeValues testData = do
  -- set current time to dummyNow
  setNow dummyNow

  -- get the balance of the genesis address
  genesisAddressBalance <- getGenesisAddressBalance

  -- get the XTZ values to send to AddLiquidity and RemoveLiquidity capped
  -- by the amount that genesis address has divied by three
  let xtzSentToAddLiquidity = minMutez (tdXtzSentToAddLiquidity testData) (unsafeMkMutez $ ((unMutez genesisAddressBalance) `div` 3))
      xtzSentToXtzToToken   = minMutez (tdXtzSentToXtzToToken testData) (unsafeMkMutez $ ((unMutez genesisAddressBalance) `div` 3))

  -- originate fa with totalSupply all belonging to genesis address
  let faStorage = FA.initStorage genesisAddress (tdTotalSupply testData)
  fa <- lOriginate FA.contract "FA1.2" faStorage zeroMutez
  
  -- originate dexter for the fa
  let dexterStorage = Dexter.initStorage Mock.camlCase $ unTAddress fa
  dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

  -- approve dexter to spend genesis address's tokens
  lCallEPWithMutez
    genesisAddress
    fa
    (Call @"Approve")
    (FA.mkApproveParams (unTAddress dexter) (tdTotalSupply testData))
    zeroMutez
  
  -- after AddLiquidity: xtzPool is xtzSentToAddLiquidity, lqtTotal is xtzSentToAddLiquidity
  -- and tokenPool is tdMaxTokensDeposited.
  if Prelude.not (tdSkipAddLiquidity testData)
  then
    lCallEPWithMutez
      genesisAddress
      dexter
      (Call @"AddLiquidity")
      (Dexter.mkAddLiquidityParams genesisAddress
        1
        (tdMaxTokensDeposited testData)
        (tdDeadline testData)
      )
      xtzSentToAddLiquidity
  else pure ()

  -- minTokensBought
  -- only try to buy a percentage of tokens from the tokenPool
  let xtzIn           = fromIntegral . unMutez $ xtzSentToXtzToToken
      tokenPool       = tdMaxTokensDeposited testData
      xtzPool         = xtzSentToAddLiquidity
      minTokensBought =
        case tdAdjustMinTokensBoughtToFail testData of
          Just factor -> tdTotalSupply testData * factor
          Nothing -> 
             min (tdMinTokensBought testData)
                 ((xtzIn * 997 * tokenPool) `div` ((fromIntegral . unMutez $ xtzPool) * 1000 + xtzIn * 997))

  lCallEPWithMutez
    genesisAddress
    dexter
    (Call @"XtzToToken")
    (Dexter.mkXtzToTokenParams
      (tdReceiver        testData)
      minTokensBought
      (tdDeadline        testData)      
    )
    xtzSentToXtzToToken

  -- calculate tokens bought to update the storage and compare it with the
  -- storage from the env
  let lqtTotal     = fromIntegral $ unMutez xtzSentToAddLiquidity
      tokensBought =
        if tdMinTokensBought testData == 0
        then 0
        else
          -- tokensBought = (xtzIn * 997 * tokenPool) / (xtzPool * 1000 + xtzIn * 997)
          ((fromIntegral $ unMutez xtzSentToXtzToToken) * 997 * tokenPool) `div`
          ((fromIntegral $ unMutez xtzPool) * 1000 + (fromIntegral $ unMutez xtzSentToXtzToToken) * 997)

  let dexterUpdatedStorage =
        Dexter.setTokenPool (tokenPool - tokensBought) $
        Dexter.setXtzPool   (unsafeMkMutez $ (unMutez xtzPool) + (unMutez xtzSentToXtzToToken)) $
        Dexter.setLqtTotal  lqtTotal $        
        Dexter.insertLiquidityOwner genesisAddress lqtTotal Map.empty $        
        dexterStorage

  -- receiver has tokens bought
  -- dexter has tokenPool - tokensBought  
  -- genesis address has totalSupply - tdMaxTokensDeposited, and allowance for dexter
  let faUpdatedStorage =
        FA.insertAccount
          genesisAddress
          (FA.setAccountBalance (tdTotalSupply testData - tokenPool) $ FA.insertAllowance (unTAddress dexter) (tdTotalSupply testData - tokenPool) $ FA.emptyAccount) $
        FA.insertAccount (unTAddress dexter) (FA.setAccountBalance (tokenPool - tokensBought) $ FA.emptyAccount) $        
        FA.insertAccount (tdReceiver testData) (FA.setAccountBalance tokensBought $ FA.emptyAccount) $
        faStorage

  pure (dexter, dexterUpdatedStorage, fa, faUpdatedStorage)

-- =============================================================================
-- Test data and generators
-- =============================================================================

data TestData =
  TestData
    { tdTotalSupply                 :: Natural -- | total amount of token FA1.2
    , tdOwnerBalance                :: Natural -- | how much the genesisAddress owns of the FA1.2 total supply
    , tdMaxTokensDeposited          :: Natural
    , tdXtzSentToAddLiquidity       :: Mutez
    , tdXtzSentToXtzToToken         :: Mutez
    , tdReceiver                    :: Address
    , tdMinTokensBought             :: Natural
    , tdDeadline                    :: Timestamp
    , tdSkipAddLiquidity            :: Bool   -- | this is to emulate a trade when xtzPool and tokenPool are empty
    , tdAdjustMinTokensBoughtToFail :: Maybe Natural
    } deriving (Show)

genTestData :: Timestamp -> Gen TestData
genTestData now_ = do
  totalSupply           <- choose (10000, 99999999999) :: Gen Integer
  maxTokensDeposited    <- fromIntegral <$> (choose (1000, totalSupply) :: Gen Integer)
  xtzSentToAddLiquidity <- Gen.genOneTezOrGreater
  xtzSentToXtzToToken   <- Gen.genOneTezOrGreater  
  receiver              <- Mock.genAddress
  minTokensBought       <- fromIntegral <$> (choose (1, 10000) :: Gen Integer)
  deadline              <- timestampFromSeconds <$> choose (timestampToSeconds now_ + 1, timestampToSeconds now_ + 9999999999)

  pure $
    TestData
      (fromIntegral totalSupply)
      (fromIntegral totalSupply)
      maxTokensDeposited
      xtzSentToAddLiquidity
      xtzSentToXtzToToken
      receiver
      minTokensBought
      deadline
      False
      Nothing

-- | run genTestData, but set tdDeadline to a value
-- less than or equal to NOW. This will cause XtzToToken to fail.
genTestDataWithDeadlineLessThanNow :: Timestamp -> Gen TestData
genTestDataWithDeadlineLessThanNow now_ = do
  testData <- genTestData now_
  deadline <- timestampFromSeconds <$> choose (1, timestampToSeconds dummyNow)
  pure $ testData { tdDeadline = deadline }
    
genTestDataWithMinTokensBoughtZero :: Timestamp -> Gen TestData
genTestDataWithMinTokensBoughtZero now_ = do
  testData <- genTestData now_
  pure $ testData { tdMinTokensBought = 0 }

-- | The amount of tokens bought is decided by dexter based on the following
-- formula
--
-- tokensBought = (xtzIn * 997 * tokenPool) / (xtzPool * 1000 + xtzIn * 997)
--
-- The buyer can protect themselves with the minTokensBought parameter. If
-- tokensBought is less than minTokensBought, then the trade will not go
-- through and the user will keep their XTZ.
genTestDataWithTokensBoughtLessThanMin :: Timestamp -> Gen TestData
genTestDataWithTokensBoughtLessThanMin now_ = do
  testData        <- genTestData now_
  factor          <- choose (2,5) :: Gen Integer
  pure $ testData { tdAdjustMinTokensBoughtToFail = Just $ fromIntegral factor }
  
genTestDataWithXtzSentToXtzToTokenZero :: Timestamp -> Gen TestData
genTestDataWithXtzSentToXtzToTokenZero now_ = do
  testData        <- genTestData now_
  pure $ testData { tdXtzSentToXtzToToken = zeroMutez }

getTestDataWithSkipAddLiquidity :: Timestamp -> Gen TestData
getTestDataWithSkipAddLiquidity now_ = do
  testData        <- genTestData now_
  pure $ testData { tdSkipAddLiquidity = True }
