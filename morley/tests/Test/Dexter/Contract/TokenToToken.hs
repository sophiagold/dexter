{-|
Module      : Test.Dexter.Contract.TokenToToken
Copyright   : (c) camlCase, 2019-2020
Maintainer  : james@camlcase.io

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.TokenToToken where

-- lorentz and morley
import           Lorentz
import           Lorentz.Test
import           Lorentz.Test.Extension
import           Michelson.Test.Dummy (dummyNow)
import           Tezos.Core           (unMutez, unsafeMkMutez, timestampPlusSeconds, timestampToSeconds)

-- testing
import           Test.Hspec      (Spec, it, describe)
import           Test.QuickCheck (Gen, choose, forAll)

-- fa1.2
import qualified FaOnePointTwo.Contract  as FA
import qualified FaOnePointTwo.Test      as FA

-- dexter
import qualified Dexter.Contract           as Dexter
import qualified Dexter.Contract.Test      as Dexter
import qualified Test.Dexter.Contract.Mock as Mock
import qualified Test.Dexter.Contract.Gen  as Gen

spec :: (forall s. Dexter.RuntimeValues s) -> Spec
spec runtimeValues = do
  describe "tokenToToken entrypoint" $ do
    it "PROP-TTT-000: If now > deadline, then this operation will fail." $ do
      forAll (genTestDataWithDeadlineLessThanNow dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testTokenToToken runtimeValues testData  `catchExpectedError` lExpectError (== [mt|NOW is greater than deadline.|])

    it "PROP-TTT-001: If output_dexter_address is not a Dexter contract, this operation will fail." $ do
      forAll (genTestDataWithNonDexterAddress dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testTokenToToken runtimeValues testData  `catchExpectedError` lExpectUnspecifiedFailWith

    it "PROP-TTT-002: If min_tokens_bought == 0, this operation will fail." $ do
      forAll (genTestDataWithMinTokensBoughtZero dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testTokenToToken runtimeValues testData  `catchExpectedError` lExpectError (== [mt|minTokensBought must be greater than zero.|])

    it "PROP-TTT-003: If tokens_bought < min_tokens_bought, this operation will fail." $ do
      forAll (genTestDataWithAdjustMinTokensBoughtToFail dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testTokenToToken runtimeValues testData  `catchExpectedError` lExpectError (== [mt|tokensBought is less than minTokensBought.|])

    it "PROP-TTT-004: If tokens_bought >= min_tokens_bought, dexter has permission for owner’s tokens_sold and tokens_sold <= owner’s balance, now < deadline, then this operation will succeed." $ do
      forAll (genTestData dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testTokenToToken runtimeValues testData
      
-- =============================================================================
-- Test functions
-- =============================================================================

testTokenToToken
  :: (forall s. Dexter.RuntimeValues s)
  -> TestData
  -> IntegrationalScenarioM () -- (TAddress Dexter.Parameter, Dexter.Storage, TAddress FA.Parameter, FA.Storage)
testTokenToToken runtimeValues testData = do
  -- set current time to dummyNow
  setNow dummyNow
  let later = dummyNow `timestampPlusSeconds` 1000

  -- get the balance of the genesis address
  genesisAddressBalance <- getGenesisAddressBalance

  -- get the XTZ values to send to AddLiquidity and RemoveLiquidity capped
  -- by the amount that genesis address has divied by three
  let xtzSentToAddLiquidity1 = minMutez (tdXtzSentToAddLiquidity1 testData) (unsafeMkMutez $ ((unMutez genesisAddressBalance) `div` 5))
      xtzSentToAddLiquidity2 = minMutez (tdXtzSentToAddLiquidity2 testData) (unsafeMkMutez $ ((unMutez genesisAddressBalance) `div` 5))
      xtzSentToTokenToToken  = minMutez (tdXtzSentToTokenToToken  testData) (unsafeMkMutez $ ((unMutez genesisAddressBalance) `div` 3))

  -- create first FA1.2-dexter pair
  -- originate fa with totalSupply all belonging to genesis address
  let faStorage1     =  FA.initStorage genesisAddress (tdTotalSupply1 testData)
  fa1                <- lOriginate FA.contract "FA1.2" faStorage1 zeroMutez
  let dexterStorage1 =  Dexter.initStorage Mock.camlCase $ unTAddress fa1
  dexter1            <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage1 zeroMutez

  -- create second FA1.2-dexter pair
  -- originate fa with totalSupply all belonging to genesis address
  let faStorage2     =  FA.initStorage genesisAddress (tdTotalSupply2 testData)
  fa2                <- lOriginate FA.contract "FA1.2" faStorage2 zeroMutez
  let dexterStorage2 =  Dexter.initStorage Mock.camlCase $ unTAddress fa2
  dexter2            <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage2 zeroMutez

  -- approval and addLiquidity for first FA1.2-dexter pair
  let approvalAmount1 = tdTotalSupply1 testData
  
  lCallEPWithMutez
    genesisAddress
    fa1
    (Call @"Approve")
    (FA.mkApproveParams (unTAddress dexter1) approvalAmount1)
    zeroMutez
  
  lCallEPWithMutez
    genesisAddress
    dexter1
    (Call @"AddLiquidity")
    (Dexter.mkAddLiquidityParams genesisAddress
      1
      (tdMaxTokensDeposited1 testData)
      later
    )
    xtzSentToAddLiquidity1

  -- approval and addLiquidity for second FA1.2-dexter pair
  let approvalAmount2 = tdTotalSupply2 testData
  
  lCallEPWithMutez
    genesisAddress
    fa2
    (Call @"Approve")
    (FA.mkApproveParams (unTAddress dexter2) approvalAmount2)
    zeroMutez
  
  lCallEPWithMutez
    genesisAddress
    dexter2
    (Call @"AddLiquidity")
    (Dexter.mkAddLiquidityParams genesisAddress
      1
      (tdMaxTokensDeposited2 testData)
      later
    )
    xtzSentToAddLiquidity2

  -- TokenToToken
  -- performs TokenToXtz in dexter1, then dexter1 does XtzToToken in dexter2
  -- (dexter1) xtzBought    = (tokens_sold * 997 * xtz_pool) / (storage.s.token_pool * 1000 + (tokens_sold * 997))
  -- (dexter2) tokensBought = (xtzIn * 997 * tokenPool) / (xtzPool * 1000 + xtzIn * 997)
  let xtzPool1        = fromIntegral $ unMutez xtzSentToAddLiquidity1
      tokenPool1      = tdMaxTokensDeposited1 testData
      xtzBought       = ((tdTokensSold testData) * 997 * xtzPool1) `div` (tokenPool1 * 1000 + ((tdTokensSold testData) * 997))
      xtzPool2        = fromIntegral $ unMutez xtzSentToAddLiquidity2
      tokenPool2      = tdMaxTokensDeposited2 testData
      minTokensBought =
        if (tdMinTokensBought testData) == 0
        then 0
        else
          (xtzBought * 997 * tokenPool2) `div`
          (xtzPool2 * 1000 + xtzBought * 997) *
          (maybe 1 id (tdAdjustMinTokensBoughtToFail testData))
  
  lCallEPWithMutez
    genesisAddress
    dexter1
    (Call @"TokenToToken")
    (Dexter.mkTokenToTokenParams
      (if tdUseNonDexterAddress testData then (tdReceiver testData) else (unTAddress dexter2))
      minTokensBought
      genesisAddress
      (tdReceiver   testData)
      (tdTokensSold testData)
      (tdDeadline   testData)      
    )
    xtzSentToTokenToToken

  pure ()

-- =============================================================================
-- TestData
-- =============================================================================

data TestData =
  TestData
    { tdTotalSupply1           :: Natural   -- | total amount of first token FA1.2
    , tdTotalSupply2           :: Natural   -- | total amount of second token FA1.2
    , tdMaxTokensDeposited1    :: Natural   -- | amount of tokens deposited in the first dexter
    , tdMaxTokensDeposited2    :: Natural   -- | amount of tokens deposited in the second dexter   
    , tdXtzSentToAddLiquidity1 :: Mutez     -- | amount of XTZ deposited in the first dexter
    , tdXtzSentToAddLiquidity2 :: Mutez     -- | amount of XTZ deposited in the second dexter
    , tdXtzSentToTokenToToken  :: Mutez     -- | amount of XTZ sent to TokenToToken
    , tdReceiver               :: Address   -- | who will receive the token from TokenToToken
    , tdTokensSold             :: Natural   -- | amount of token sold to TokenToToken
    , tdMinTokensBought        :: Natural   -- | minTokensBought from TokenToToken
    , tdDeadline               :: Timestamp -- | deadline for TokenToToken
    , tdUseNonDexterAddress    :: Bool      -- | to prove that sending to a non dexter address will crash
    , tdAdjustMinTokensBoughtToFail :: Maybe Natural
    } deriving (Show)

-- =============================================================================
-- Generators
-- =============================================================================

-- | Generator TestData that guarantees that TokenToToken will succeed
genTestData :: Timestamp -> Gen TestData
genTestData now_ = do
  totalSupply1           <- choose (10000, 99999999999) :: Gen Integer
  totalSupply2           <- choose (10000, 99999999999) :: Gen Integer  

  maxTokensDeposited1    <- choose (1000, totalSupply1 `div` 2) :: Gen Integer
  maxTokensDeposited2    <- choose (1000, totalSupply2 `div` 2) :: Gen Integer  

  xtzSentToAddLiquidity1 <- Gen.genOneTezOrGreater
  xtzSentToAddLiquidity2 <- Gen.genOneTezOrGreater

  receiver              <- Mock.genAddress

  tokensSold            <- choose (100, (totalSupply1 - maxTokensDeposited1) `div` 3) :: Gen Integer
  minTokensBought       <- choose (100, (totalSupply2 - maxTokensDeposited2) `div` 3) :: Gen Integer

  deadline              <- timestampFromSeconds <$> choose (timestampToSeconds now_ + 1, timestampToSeconds now_ + 9999999999)

  pure $
    TestData
      (fromIntegral totalSupply1)
      (fromIntegral totalSupply2)
      (fromIntegral maxTokensDeposited1)
      (fromIntegral maxTokensDeposited2)      
      xtzSentToAddLiquidity1
      xtzSentToAddLiquidity2
      zeroMutez
      receiver
      (fromIntegral tokensSold)
      (fromIntegral minTokensBought)
      deadline
      False
      Nothing

-- | run genTestData, but set tdDeadline to a value
-- less than or equal to NOW. This will cause XtzToToken to fail.
genTestDataWithDeadlineLessThanNow :: Timestamp -> Gen TestData
genTestDataWithDeadlineLessThanNow now_ = do
  testData <- genTestData now_
  deadline <- timestampFromSeconds <$> choose (1, timestampToSeconds dummyNow)
  pure $ testData { tdDeadline = deadline }

genTestDataWithNonDexterAddress :: Timestamp -> Gen TestData
genTestDataWithNonDexterAddress now_ = do
  testData <- genTestData now_
  pure $ testData { tdUseNonDexterAddress = True }

genTestDataWithMinTokensBoughtZero :: Timestamp -> Gen TestData
genTestDataWithMinTokensBoughtZero now_ = do
  testData <- genTestData now_
  pure $ testData { tdMinTokensBought = 0 }  

genTestDataWithAdjustMinTokensBoughtToFail :: Timestamp -> Gen TestData
genTestDataWithAdjustMinTokensBoughtToFail now_ = do
  testData <- genTestData now_
  factor          <- choose (2,5) :: Gen Integer
  pure $ testData { tdAdjustMinTokensBoughtToFail = Just $ fromIntegral factor }  
