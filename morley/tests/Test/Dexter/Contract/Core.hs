{-|
Module      : Test.Dexter.Contract.AddLiquidity
Copyright   : (c) camlCase, 2019-2020
Maintainer  : james@camlcase.io

Unit and property tests for shared lorentz functions.

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.Core where

-- general
import           GHC.Natural (naturalFromInteger)
import           Util.Named ((.!))

-- lorentz and morley
import           Lorentz
import           Lorentz.Test

import           Tezos.Core           (mkMutez)

-- testing
import           Test.Hspec      (Spec, it, describe, shouldBe)
import           Test.QuickCheck (property)

-- dexter
import qualified Dexter.Contract           as Dexter
import qualified Dexter.Contract.Test      as Dexter
import qualified Test.Dexter.Contract.Mock as Mock

spec :: Spec
spec =
  describe "Dexter.Contract.Core shared functions" $ do
    it "mutezToNatural properties" $ do
      let runMutezToNatural mutez = do 
            let initStack = (Identity mutez :& RNil)
            resStack <- interpretLorentzInstr dummyContractEnv Dexter.mutezToNatural initStack
            let Identity res :& _ = resStack
            return res
    
      property $ \x ->
        case mkMutez x of
          Nothing -> True
          Just m  -> runMutezToNatural m == Right (naturalFromInteger . fromIntegral $ x)

    it "naturalToMutez properties" $ do
      let runNaturalToMutez mutez = do 
            let initStack = (Identity mutez :& RNil)
            resStack <- interpretLorentzInstr dummyContractEnv Dexter.naturalToMutez initStack
            let Identity res :& _ = resStack
            return res
    
      property $ \x ->
        runNaturalToMutez (naturalFromInteger . fromIntegral $ x) == Right (toMutez x)

    it "getAccount" $ do
      let runGetAccount address_ storage = do
            let initStack = (Identity address_ :& Identity storage :& RNil)
            resStack <- interpretLorentzInstr dummyContractEnv Dexter.getAccount initStack
            let Identity res :& _ = resStack
            return res

      runGetAccount (#owner .! Mock.alice) (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12) `shouldBe` Right Dexter.emptyAccount
      runGetAccount (#owner .! Mock.alice) Mock.minimalStorage `shouldBe` Right Mock.aliceAccount
