{-|
Module      : Test.Dexter.Contract.RemoveLiquidity
Copyright   : (c) camlCase, 2020
Maintainer  : james@camlcase.io

Unit and property tests for the dexter%approve.

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.Approve where

-- general
import qualified Data.Map.Strict           as Map
-- lorentz and morley
import           Lorentz
import           Lorentz.Test
import           Lorentz.Test.Extension
import           Michelson.Test.Dummy (dummyNow)

-- testing
import           Test.Hspec      (Spec, it, describe)
import           Test.QuickCheck (Gen, choose, forAll, suchThat)

-- fa1.2
import qualified FaOnePointTwo.Contract    as FA
import qualified FaOnePointTwo.Test        as FA

-- dexter
import qualified Dexter.Contract           as Dexter
import qualified Dexter.Contract.Test      as Dexter
import qualified Test.Dexter.Contract.Mock as Mock
import qualified Test.Dexter.Contract.Gen  as Gen

-- | spec function for dexter%approve
spec :: (forall s. Dexter.RuntimeValues s) -> Spec
spec runtimeValues = do
  describe "approve entrypoint" $ do
    it "PROP-APR-000: If amount > 0, the operation will fail." $ do
      forAll genTestDataWithNonZeroMutez $ \testData ->
        integrationalTestProperty $
          void $ testApprove runtimeValues testData
          `catchExpectedError` lExpectError (== [mt|Amount must be zero.|])

    it "PROP-APR-001: If accounts[sender].allowance[spender] != current_allowance, the operation will fail." $ do
      forAll genTestDataWithIncorrectSecondCurrentAllowance $ \testData ->    
        integrationalTestProperty $ do
          (dexter, dexterStorage) <- testApprove runtimeValues testData
          void $ testSecondApprove dexter dexterStorage Mock.alice (tdSecondApproveAmount testData) (tdSecondCurrentAllowance testData)
            `catchExpectedError` lExpectError (== [mt|The current allowance parameter must equal the sender's current allowance for the owner.|])

    it "PROP-APR-002: If accounts[sender] is undefined and current_allowance != 0, the operation will fail." $ do
      forAll genTestDataWithIncorrectCurrentAllowance $ \testData ->
        integrationalTestProperty $
          void $ testApprove runtimeValues testData
          `catchExpectedError` lExpectError (== [mt|The current allowance parameter must equal the sender's current allowance for the owner.|])

    it "PROP-APR-003: If accounts[sender] is undefined and current_allowance == 0, then storage will be updated such that accounts[sender].allowance[spender] := allowance." $ do
      forAll genTestData $ \testData ->
        integrationalTestProperty $ do
          (dexter, dexterStorage) <- testApprove runtimeValues testData
          lExpectStorageConst dexter dexterStorage          

    it "PROP-APR-004: If accounts[sender] is defined and accounts[sender].allowance[spender] == current_allowance, then storage will be updated such that accounts[sender].allowance[spender] := allowance." $ do
      forAll genTestData $ \testData ->    
        integrationalTestProperty $ do
          (dexter, dexterStorage)   <- testApprove runtimeValues testData
          dexterStorage2 <-
            testSecondApprove dexter dexterStorage
            (tdSpender testData)
            (tdSecondApproveAmount testData)
            (tdSecondCurrentAllowance testData)
          lExpectStorageConst dexter dexterStorage2

    it "PROP-APR-005: Assume PROP-APR-003, but spender is sender. The properties should still be true." $ do
      forAll genTestDataSpenderIsSender $ \testData ->
        integrationalTestProperty $ do
          (dexter, dexterStorage) <- testApprove runtimeValues testData
          lExpectStorageConst dexter dexterStorage          
    
-- =============================================================================
-- Test functions
-- =============================================================================

-- | 1. originate FA1.2, totalSupply given to genesisAddress
--   2. originate Dexter
--   3. Approve to spender for owner in Dexter
testApprove
  :: (forall s. Dexter.RuntimeValues s)
  -> TestData
  -> IntegrationalScenarioM (TAddress Dexter.Parameter, Dexter.Storage)
testApprove runtimeValues testData = do
  -- set current time to dummyNow
  setNow dummyNow

  -- originate fa with totalSupply all belonging to owner address
  let faStorage = FA.initStorage genesisAddress (tdTotalSupply testData)
  fa     <- lOriginate FA.contract "FA1.2" faStorage zeroMutez
  
  -- originate dexter for the fa
  let dexterStorage = Dexter.initStorage Mock.camlCase $ unTAddress fa
  dexter <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

  -- get the balance of the genesis address
  genesisAddressBalance <- getGenesisAddressBalance
  let xtzSent = minMutez (tdXtzSent testData) genesisAddressBalance
  
  lCallEPWithMutez
    genesisAddress
    dexter
    (Call @"Approve")
    (Dexter.mkApproveParams (tdSpender testData) (tdApproveAmount testData) (tdCurrentAllowance testData))
    xtzSent

  let dexterUpdatedStorage =
        Dexter.insertLiquidityOwner genesisAddress 0 (Map.fromList [((tdSpender testData), (tdApproveAmount testData))]) $
        dexterStorage

  pure (dexter, dexterUpdatedStorage)

-- | 1. Pass an originated dexter contract that for the spender already has an approval.
testSecondApprove
  :: TAddress Dexter.Parameter
  -> Dexter.Storage
  -> Address
  -> Natural
  -> Natural
  -> IntegrationalScenarioM (Dexter.Storage)
testSecondApprove dexter dexterStorage spender newAllowance currentAllowance = do
  lCallEPWithMutez
    genesisAddress
    dexter
    (Call @"Approve")
    (Dexter.mkApproveParams spender newAllowance currentAllowance)
    zeroMutez

  let dexterUpdatedStorage =
        Dexter.insertLiquidityOwner genesisAddress 0 (Map.fromList [(spender, newAllowance)]) $
        dexterStorage

  pure dexterUpdatedStorage

-- =============================================================================
-- Test data and generators
-- =============================================================================

-- | Data generated by QuickCheck in order to originate necessary contracts
-- and execute preliminary in order to call and test dexter%approve.
data TestData =
  TestData
    { tdTotalSupply            :: Natural -- | total amount of token FA1.2
    , tdOwnerBalance           :: Natural -- | how much the owner owns of the FA1.2 total supply
    , tdSpender                :: Address
    , tdApproveAmount          :: Natural -- | how much the spender will be approved for
    , tdCurrentAllowance       :: Natural -- | 
    , tdSecondApproveAmount    :: Natural -- | how much the spender will be approved for the second time
    , tdSecondCurrentAllowance :: Natural -- |
    , tdXtzSent                :: Mutez   -- | xtz sent to dexter%approve.
    } deriving (Show)

-- | generate TestData for RemoveLiquidity that will cause the entrypoint to be
-- succesfully called
genTestData :: Gen TestData
genTestData = do
  totalSupply         <- choose (10000, 99999999999) :: Gen Integer
  spender             <- Mock.genAddress
  approveAmount       <- choose (10000, 99999999999) :: Gen Integer
  secondApproveAmount <- choose (10000, 99999999999) :: Gen Integer

  pure $
    TestData
      (fromIntegral totalSupply)      
      (fromIntegral totalSupply)
      spender
      (fromIntegral approveAmount)
      0
      (fromIntegral secondApproveAmount)
      (fromIntegral approveAmount)      
      zeroMutez

genTestDataWithNonZeroMutez :: Gen TestData
genTestDataWithNonZeroMutez = do
  testData <- genTestData
  xtzSent  <- Gen.genOneTezOrGreater
  pure $ testData { tdXtzSent = xtzSent }

genTestDataWithIncorrectCurrentAllowance :: Gen TestData
genTestDataWithIncorrectCurrentAllowance = do
  testData <- genTestData
  currentAllowance <- choose (1, 99999999999) :: Gen Integer
  pure $ testData { tdCurrentAllowance = fromIntegral currentAllowance }

genTestDataWithIncorrectSecondCurrentAllowance :: Gen TestData
genTestDataWithIncorrectSecondCurrentAllowance = do
  testData <- genTestData
  currentAllowance <- suchThat (choose (10000, 99999999999)) (\x -> fromIntegral x /= tdApproveAmount testData) :: Gen Integer
  pure $ testData { tdSecondCurrentAllowance = fromIntegral currentAllowance }

genTestDataSpenderIsSender :: Gen TestData
genTestDataSpenderIsSender = do
  testData <- genTestData
  pure $ testData { tdSpender = genesisAddress }
