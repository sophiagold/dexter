{-|
Module      : Test.Dexter.Contract.Default
Copyright   : (c) camlCase, 2020
Maintainer  : james@camlcase.io

-}

{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.SetManager where

-- dexter
import qualified Dexter.Contract as Dexter
import qualified Dexter.Contract.Test as Dexter
import qualified Test.Dexter.Contract.Gen as Gen
import qualified Test.Dexter.Contract.Mock as Mock

-- fa1.2
import qualified FaOnePointTwo.Contract  as FA
import qualified FaOnePointTwo.Test      as FA

-- morley and lorentz
import           Lorentz
import           Lorentz.Test
import           Lorentz.Test.Extension
import           Michelson.Test.Dummy (dummyNow)
import           Tezos.Core      (unsafeMkMutez, unMutez)

-- testing
import           Test.Hspec      (Spec, it, describe)
import           Test.QuickCheck (Gen, choose, forAll)

spec :: (forall s. Dexter.RuntimeValues s) -> Spec
spec runtimeValues = do  
  describe "setManager entrypoint" $ do
    it "PROP-SM-000: If amount == zero, this operation will fail." $ do
      forAll genTestDataWithAmountGreaterThanZero $ \testData ->
        integrationalTestProperty $
          void $ testSetManager runtimeValues testData `catchExpectedError` lExpectError (== [mt|Amount must be zero.|])

    it "PROP-SM-001: If sender is not manager, this operation will fail." $ do
      forAll genTestDataWithSenderNotManager $ \testData ->
        integrationalTestProperty $
          void $ testSetManager runtimeValues testData `catchExpectedError` lExpectError (== [mt|sender is not the contract manager.|])

    it "PROP-SM-002: If sender is the manager, the storage will be updated such that manager is newManager." $ do
      forAll genTestData $ \testData ->
        integrationalTestProperty $ do
          (dexter, dexterStorage) <- testSetManager runtimeValues testData
          lExpectStorageConst dexter dexterStorage

-- =============================================================================
-- Test functions
-- =============================================================================

testSetManager
  :: (forall s. Dexter.RuntimeValues s)
  -> TestData
  -> IntegrationalScenarioM (TAddress Dexter.Parameter, Dexter.Storage)
testSetManager runtimeValues testData = do
  -- set current time to dummyNow
  setNow dummyNow

  -- get the balance of the genesis address
  genesisAddressBalance <- getGenesisAddressBalance

  let xtzSentToSetManager   = minMutez (tdXtzSentToSetManager testData) (unsafeMkMutez $ unMutez genesisAddressBalance)

  -- originate fa with totalSupply all belonging to genesis address
  let faStorage = FA.initStorage genesisAddress (tdTotalSupply testData)
  fa <- lOriginate FA.contract "FA1.2" faStorage zeroMutez
  
  -- originate dexter for the fa
  let dexterStorage = Dexter.initStorage (tdManager testData) $ unTAddress fa
  dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

  lCallEPWithMutez
    (tdSender testData)
    dexter
    (Call @"SetManager")
    (tdNewManager testData)
    xtzSentToSetManager

  let dexterUpdatedStorage =
        Dexter.setNewManager (tdNewManager testData) $
        dexterStorage

  pure (dexter, dexterUpdatedStorage)

-- =============================================================================
-- TestData
-- =============================================================================

data TestData =
  TestData
    { tdTotalSupply         :: Natural -- | total amount of token FA1.2
    , tdManager             :: Address
    , tdSender              :: Address
    , tdNewManager          :: Address
    , tdXtzSentToSetManager :: Mutez
    } deriving (Show)

-- =============================================================================
-- Generators
-- =============================================================================

genTestData :: Gen TestData
genTestData = do
  totalSupply  <- choose (10000, 99999999999) :: Gen Integer
  newManager   <- Mock.genAddress

  pure $
    TestData
      (fromIntegral totalSupply)
      genesisAddress
      genesisAddress
      newManager
      zeroMutez

genTestDataWithAmountGreaterThanZero :: Gen TestData
genTestDataWithAmountGreaterThanZero = do
  testData              <- genTestData
  xtzSentToSetManager   <- Gen.genOneTezOrGreater
  pure $ testData { tdXtzSentToSetManager = xtzSentToSetManager }

genTestDataWithSenderNotManager :: Gen TestData
genTestDataWithSenderNotManager = do
  testData              <- genTestData
  sender_   <- Mock.genAddress
  pure $ testData { tdSender = sender_ }
