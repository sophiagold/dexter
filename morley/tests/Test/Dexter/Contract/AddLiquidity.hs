{-|
Module      : Test.Dexter.Contract.AddLiquidity
Copyright   : (c) camlCase, 2019-2020
Maintainer  : james@camlcase.io

Unit and property tests for the add liquidity entry point in the Dexter exchange contract.

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.AddLiquidity where

-- general
import qualified Data.Map.Strict as Map

-- lorentz and morley
import           Lorentz hiding (get)
import           Lorentz.Test
import           Lorentz.Test.Extension

import           Michelson.Test.Dummy (dummyNow)
import           Tezos.Core           (unMutez, unsafeMkMutez, timestampPlusSeconds)

-- testing
import           Test.Hspec      (Spec, it, describe)
import           Test.QuickCheck (Gen, choose, forAll)

-- fa1.2
import qualified FaOnePointTwo.Contract  as FA
import qualified FaOnePointTwo.Test      as FA

-- dexter
import qualified Dexter.Contract           as Dexter
import qualified Dexter.Contract.Test      as Dexter
import qualified Test.Dexter.Contract.Mock as Mock
import qualified Test.Dexter.Contract.Gen  as Gen

spec :: (forall s. Dexter.RuntimeValues s) -> Spec
spec runtimeValues = do  
  describe "addLiquidity entrypoint" $ do
    it "PROP-AL-000: if Dexter does not have permission to transfer the owner’s FA1.2 tokens, it will fail." $ do
      forAll ((,) <$> genGreaterAndLesser <*> Gen.genOneTezOrGreater) $ \((greater, lesser), oneTezOrGreater') ->
        integrationalTestProperty $ do
          genesisAddressBalance <- getGenesisAddressBalance

          let oneTezOrGreater =
                if (unMutez oneTezOrGreater') < (unMutez genesisAddressBalance)
                then oneTezOrGreater'
                else genesisAddressBalance
            

          let storage = FA.initStorage genesisAddress greater
              later = dummyNow `timestampPlusSeconds` 100

          -- originate fa
          fa <- lOriginate FA.contract "FA1.2" storage zeroMutez
          -- originate dexter for the fa contract
          let dexterStorage = Dexter.initStorage Mock.camlCase $ unTAddress fa
          dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

          -- approve dexter to spend token
          lCallEPWithMutez genesisAddress fa (Call @"Approve") (FA.mkApproveParams (unTAddress dexter) lesser) zeroMutez

          -- add liquidity
          lCallEPWithMutez
            genesisAddress
            dexter
            (Call @"AddLiquidity")
            (Dexter.mkAddLiquidityParams genesisAddress 1 greater later)
            oneTezOrGreater `catchExpectedError` lExpectError (== [mt|The transfer amount is greater than the sender's approval.|])

    it "PROP-AL-001: if now is greater than deadline, it will fail." $ do
      forAll ((,,) <$> genGreaterAndLesser <*> Gen.genOneTezOrGreater <*> genLaterAndEarlier) $ \((greater, lesser), oneTezOrGreater', (later, earlier)) ->
        integrationalTestProperty $ do
          genesisAddressBalance <- getGenesisAddressBalance

          let oneTezOrGreater =
                if (unMutez oneTezOrGreater') < (unMutez genesisAddressBalance)
                then oneTezOrGreater'
                else genesisAddressBalance
            

          let storage = FA.initStorage genesisAddress greater          
          -- originate fa
          fa <- lOriginate FA.contract "FA1.2" storage zeroMutez
          -- originate dexter for fa, set the current time
          setNow later
          let dexterStorage = Dexter.initStorage Mock.camlCase $ unTAddress fa
          dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

          -- approve dexter for genesisAddress's tokens
          lCallEPWithMutez genesisAddress fa (Call @"Approve") (FA.mkApproveParams (unTAddress dexter) greater) zeroMutez

          -- send with minLqtMinted
          lCallEPWithMutez
            genesisAddress
            dexter
            (Call @"AddLiquidity")
            (Dexter.mkAddLiquidityParams genesisAddress 0 lesser earlier)
            oneTezOrGreater `catchExpectedError` lExpectError (== [mt|NOW is greater than deadline.|])


    it "PROP-AL-002: if min_lqt_minted is 0, it will fail." $ do
      forAll ((,) <$> genGreaterAndLesser <*> Gen.genOneTezOrGreater) $ \((greater, lesser), oneTezOrGreater') ->
        integrationalTestProperty $ do
          genesisAddressBalance <- getGenesisAddressBalance

          let oneTezOrGreater =
                if (unMutez oneTezOrGreater') < (unMutez genesisAddressBalance)
                then oneTezOrGreater'
                else genesisAddressBalance
            

          let storage = FA.initStorage genesisAddress greater
              later = dummyNow `timestampPlusSeconds` 100

          -- originate fa
          fa <- lOriginate FA.contract "FA1.2" storage zeroMutez
          -- originate dexter for fa
          let dexterStorage = Dexter.initStorage Mock.camlCase $ unTAddress fa
          dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

          -- approve dexter for genesisAddress's tokens
          lCallEPWithMutez genesisAddress fa (Call @"Approve") (FA.mkApproveParams (unTAddress dexter) greater) zeroMutez

          -- send with minLqtMinted
          lCallEPWithMutez
            genesisAddress
            dexter
            (Call @"AddLiquidity")
            (Dexter.mkAddLiquidityParams genesisAddress 0 lesser later)
            oneTezOrGreater `catchExpectedError` lExpectError (== [mt|minLqtMinted must be greater than zero.|])

    it "PROP-AL-003: if max_tokens_deposited is 0, it will fail." $ do
      forAll ((,,) <$> genNonZero <*> genNonZero <*> Gen.genOneTezOrGreater) $ \(greater, lesser, oneTezOrGreater') ->
        integrationalTestProperty $ do
          genesisAddressBalance <- getGenesisAddressBalance

          let oneTezOrGreater =
                if (unMutez oneTezOrGreater') < (unMutez genesisAddressBalance)
                then oneTezOrGreater'
                else genesisAddressBalance
            

          let storage = FA.initStorage genesisAddress greater
              later = dummyNow `timestampPlusSeconds` 100

          -- originate fa
          fa <- lOriginate FA.contract "FA1.2" storage zeroMutez
          -- originate dexter for fa
          let dexterStorage = Dexter.initStorage Mock.camlCase $ unTAddress fa
          dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

          -- approve dexter for genesisAddress's tokens
          lCallEPWithMutez genesisAddress fa (Call @"Approve") (FA.mkApproveParams (unTAddress dexter) greater) zeroMutez

          -- send with minLqtMinted
          lCallEPWithMutez
            genesisAddress
            dexter
            (Call @"AddLiquidity")
            (Dexter.mkAddLiquidityParams genesisAddress lesser 0 later)
            oneTezOrGreater `catchExpectedError` lExpectError (== [mt|maxTokensDeposited must be greater than zero.|])

    it "PROP-AL-004: if now is less than deadline, lqt_pool is zero, min_lqt_minted is greater than zero and less than or equal to amount, amount is greater than or equal to one tez, Dexter has permission to transfer max_tokens_deposited from owner’s FA1.2 tokens, owner has at least max_tokens_deposited FA1.2 tokens and token_address is a FA1.2 contract, then it should succeed."  $ do
      forAll genTestData $ \testData ->
        integrationalTestProperty $ void $ addLiquidityTestProp4 runtimeValues testData

    it "PROP-AL-005: Assume PROP-AL-004, then storage will be updated such that lqt_total := amount, token_pool += max_tokens_deposited, xtz_pool += amount." $ do
      forAll genTestData $ \testData ->
        integrationalTestProperty $ do
          (dexter, _, dexterStorage) <- addLiquidityTestProp4 runtimeValues testData
          lExpectStorageConst dexter dexterStorage

    it "PROP-AL-006:  if now is less than deadline, lqt_pool is greater than zero, min_lqt_minted is greater than zero, tokens_deposited less than or equal to max_tokens_deposited, amount is greater than zero, Dexter has permission to transfer tokens_deposited from owner’s FA1.2 tokens, owner has at least tokens_deposited FA1.2 tokens and token_address is a FA1.2 contract, then it should succeed." $ do
      forAll genTestDataWithSecondAddLiquidity $ \testData ->
        integrationalTestProperty $ void $ addLiquidityTestProp6 runtimeValues testData

    it "PROP-AL-007: Assume PROP-AL-006, then storage will be updated such that lqt_total := floor(lqt_total * amount / xtz_pool), accounts[owner].balance := floor(lqt_total * amount / xtz_pool), token_pool += tokens_deposit and xtz_pool += amount." $ do
      forAll genTestDataWithSecondAddLiquidity $ \testData ->
        integrationalTestProperty $ do
          (dexter, dexterStorage) <- addLiquidityTestProp6 runtimeValues testData
          lExpectStorageConst dexter dexterStorage
      
addLiquidityTestProp4
  :: (forall s. Dexter.RuntimeValues s)
  -> TestData
  -> IntegrationalScenarioM (TAddress Dexter.Parameter, Dexter.Storage, Dexter.Storage)
addLiquidityTestProp4 runtimeValues testData = do
  genesisAddressBalance <- getGenesisAddressBalance
  -- xtzSent is capped by the amount that genesisAddressBalance owns
  -- have to do it here because accessing genesisAddressBalance requires IO which is not available in Gen monad
  let xtzSent = minMutez (tdXtzSent testData) genesisAddressBalance
      faStorage = FA.initStorage genesisAddress (tdTotalSupply testData)

  -- originate fa
  fa <- lOriginate FA.contract "FA1.2" faStorage zeroMutez
  -- originate dexter for fa
  let dexterInitialStorage =
        Dexter.initStorage Mock.camlCase (unTAddress fa)
  dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterInitialStorage zeroMutez

  setNow (tdNow testData)
          
  -- approve dexter for genesisAddress's tokens
  lCallEPWithMutez genesisAddress fa (Call @"Approve") (FA.mkApproveParams (unTAddress dexter) (tdTotalSupply testData)) zeroMutez
  
  -- send with minLqtMinted
  lCallEPWithMutez
    genesisAddress
    dexter
    (Call @"AddLiquidity")
    (Dexter.mkAddLiquidityParams genesisAddress
      (tdMinLqtMinted testData)
      (tdMaxTokensDeposited testData)
      (tdDeadline testData))
    xtzSent


  let dexterUpdatedStorage =
        Dexter.setLqtTotal  (fromIntegral $ unMutez $ xtzSent) $
        Dexter.setXtzPool   (xtzSent) $                
        Dexter.setTokenPool (tdMaxTokensDeposited testData) $
        Dexter.insertLiquidityOwner genesisAddress (fromIntegral $ unMutez $ xtzSent) Map.empty $
        dexterInitialStorage    

  pure (dexter, dexterInitialStorage, dexterUpdatedStorage)

ceilDiv :: Natural -> Natural -> Natural
ceilDiv n d =
  let x = div n d
  in if mod n d == 0 then x else x + 1

addLiquidityTestProp6
  :: (forall s. Dexter.RuntimeValues s)
  -> TestDataWithSecondAddLiquidity
  -> IntegrationalScenarioM (TAddress Dexter.Parameter, Dexter.Storage)
addLiquidityTestProp6 runtimeValues testData2 = do
  let testData = tdTestData testData2
  genesisAddressBalance <- getGenesisAddressBalance

  -- send up to a third of XTZ and token
  let firstXtzSent            = minMutez (tdXtzSent testData) (unsafeMkMutez $ ((unMutez genesisAddressBalance) `div` 3))
      firstMaxTokensDeposited = min (tdMaxTokensDeposited testData) (tdTotalSupply testData `div` 3)

  -- prepare data for addLiquidityTestProp4
  let updatedTestData =
        (tdTestData testData2) {tdMaxTokensDeposited = firstMaxTokensDeposited, tdXtzSent = firstXtzSent}
  
  (dexter, dexterInitialStorage, _) <- addLiquidityTestProp4 runtimeValues updatedTestData

  -- send some XTZ
  let secondXtzSent = minMutez (tdSecondXtzSent testData2) firstXtzSent

  -- maxTokensDeposited = ceil(tokensDeposited = amount * tokenPool, xtzPool)
  let secondMaxTokensDeposited =        
        ((fromIntegral $ unMutez secondXtzSent) * firstMaxTokensDeposited) `ceilDiv` (fromIntegral $ unMutez firstXtzSent)

  -- AddLiquidity while LqtTotal is not zero
  lCallEPWithMutez
    genesisAddress
    dexter
    (Call @"AddLiquidity")
    (Dexter.mkAddLiquidityParams genesisAddress
      1
      secondMaxTokensDeposited
      (tdDeadline testData))
    secondXtzSent

  let xtzPool   = unsafeMkMutez $ (unMutez firstXtzSent) + (unMutez secondXtzSent)
      tokenPool = firstMaxTokensDeposited + secondMaxTokensDeposited
      lqtTotal  = fromIntegral $ (unMutez firstXtzSent) + (unMutez secondXtzSent)
      dexterUpdatedStorage =
        Dexter.setLqtTotal  lqtTotal $
        Dexter.setXtzPool   xtzPool $                
        Dexter.setTokenPool tokenPool $
        Dexter.insertLiquidityOwner genesisAddress lqtTotal Map.empty $
        dexterInitialStorage          
        
  pure (dexter, dexterUpdatedStorage)

data TestData =
  TestData
    { tdNow          :: Timestamp -- | the current time
    , tdXtzPool      :: Mutez  -- | the xtz held by dexter
    , tdTotalSupply  :: Natural -- | total supply of token FA1.2
    , tdOwnerBalance :: Natural -- | the amount of FA1.2 owned by genesis address
    , tdDeadline     :: Timestamp -- | the deadline for dexter calls
    , tdXtzSent      :: Mutez -- | xtz sent to Dexter
    , tdMinLqtMinted :: Natural -- | minimum amount of LQT generated by Dexter
    , tdMaxTokensDeposited :: Natural -- | maximum amount of tokens sender wants to deposit in dexter (if lqtTotal is zero, it will send all tdMaxTokensDeposited to Dexter)
    } deriving (Show)

genTestData :: Gen TestData
genTestData = do
  totalSupply  <- (choose (10000, 99999999999) :: Gen Integer)
  laterInt     <- choose (1,99999999999)
  xtzSent      <- Gen.genOneTezOrGreater
  minLqtMinted <- choose (1000, unMutez xtzSent)
  maxTokensDeposited <- fromIntegral <$> choose (1000, totalSupply)

  pure $
    TestData
      dummyNow
      zeroMutez
      (fromIntegral totalSupply)
      (fromIntegral totalSupply)
      (dummyNow `timestampPlusSeconds` laterInt)
      xtzSent
      (fromIntegral minLqtMinted)
      maxTokensDeposited

genGreaterAndLesser :: Gen (Natural, Natural)
genGreaterAndLesser = do
  x <- choose (2, 99999999999999) :: Gen Integer
  y <- choose (0, x-1)
  pure (fromIntegral x, fromIntegral y)


genNonZero :: Gen Natural
genNonZero = do
  x <- choose (2, 99999999999999) :: Gen Integer
  pure $ fromIntegral x

genLaterAndEarlier :: Gen (Timestamp, Timestamp)
genLaterAndEarlier = do
  later   <- choose (1000, 99999999999999) :: Gen Integer
  earlier <- choose (100, later)
  pure (timestampFromSeconds later, timestampFromSeconds earlier)

data TestDataWithSecondAddLiquidity =
  TestDataWithSecondAddLiquidity
    { tdTestData      :: TestData
    , tdSecondXtzSent :: Mutez
    } deriving (Show)

genTestDataWithSecondAddLiquidity :: Gen TestDataWithSecondAddLiquidity
genTestDataWithSecondAddLiquidity = do
  testData      <- genTestData
  xtzSent       <- unsafeMkMutez <$> choose (1000000, 10000000) -- generate 1 to 100
  secondXtzSent <- unsafeMkMutez <$> choose (1000000,3000000) -- generate just a small amount, but not too small or tokens deposited will be zero
  pure $ TestDataWithSecondAddLiquidity (testData {tdXtzSent = xtzSent}) secondXtzSent
