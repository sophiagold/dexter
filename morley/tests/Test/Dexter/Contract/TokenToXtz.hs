{-|
Module      : Test.Dexter.Contract.TokenToXtz
Copyright   : (c) camlCase, 2019-2020
Maintainer  : james@camlcase.io

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.TokenToXtz where

-- general
import qualified Data.Map.Strict   as Map

-- lorentz and morley
import           Lorentz
import           Lorentz.Test
import           Lorentz.Test.Extension
import           Michelson.Test.Dummy (dummyNow)
import           Tezos.Core           (unMutez, unsafeMkMutez, timestampToSeconds)

-- testing
import           Test.Hspec      (Spec, it, describe)
import           Test.QuickCheck (Gen, choose, forAll)

-- fa1.2
import qualified FaOnePointTwo.Contract  as FA
import qualified FaOnePointTwo.Test      as FA

-- dexter
import qualified Dexter.Contract           as Dexter
import qualified Dexter.Contract.Test      as Dexter
import qualified Test.Dexter.Contract.Mock as Mock
import qualified Test.Dexter.Contract.Gen  as Gen

spec :: (forall s. Dexter.RuntimeValues s) -> Spec
spec runtimeValues = do
  describe "tokenToXtz entrypoint" $ do
    it "PROP-TTX-000: If now > deadline, this operation will fail." $ do
      forAll (genTestDataWithDeadlineLessThanNow dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testTokenToXtz runtimeValues testData `catchExpectedError` lExpectError (== [mt|NOW is greater than deadline.|])

    it "PROP-TTX-001: If min_xtz_bought == 0, this operation will fail." $ do
      forAll (genTestDataWithMinXtzBoughtZero dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testTokenToXtz runtimeValues testData `catchExpectedError` lExpectError (== [mt|minXtzBought must be greater than zero.|])

    it "PROP-TTX-002: If xtz_bought < min_xtz_bought, this operation will fail." $ do
      forAll (genTestDataWithXtzBoughtLessThanMin dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testTokenToXtz runtimeValues testData `catchExpectedError` lExpectError (== [mt|xtzBought is less than minXtzBought.|])

    it "PROP-TTX-003: If amount == zero, this operation will fail." $ do
      forAll (genTestDataWithAmountGreaterThanZero dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testTokenToXtz runtimeValues testData `catchExpectedError` lExpectError (== [mt|Amount must be zero.|])

    it "PROP-TTX-004: If xtz_pool or token_pool is zero, this operation will fail." $ do
      forAll (getTestDataWithSkipAddLiquidity dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testTokenToXtz runtimeValues testData `catchExpectedError` lExpectError (== [mt|xtzPool must be greater than zero.|])
      
    it "PROP-TTX-005: If now < deadline, min_xtz_bought > 0, min_xtz_bought <= xtz_bought, and xtz_bought <= xtz_pool, then storage will be updated such that xtz_pool -= xtz_bought, token_pool += tokens_sold and xtz_bought will be transferred to the to address." $ do
      forAll (genTestData dummyNow) $ \testData ->
        integrationalTestProperty $ do
          (dexter, dexterStorage, fa, faStorage) <- testTokenToXtz runtimeValues testData
          lExpectStorageConst dexter dexterStorage
          lExpectStorageConst fa faStorage
      
-- =============================================================================
-- Test functions
-- =============================================================================

testTokenToXtz
  :: (forall s. Dexter.RuntimeValues s)
  -> TestData
  -> IntegrationalScenarioM (TAddress Dexter.Parameter, Dexter.Storage, TAddress FA.Parameter, FA.Storage)
testTokenToXtz runtimeValues testData = do
  -- set current time to dummyNow
  setNow dummyNow

  -- get the balance of the genesis address
  genesisAddressBalance <- getGenesisAddressBalance

  -- get the XTZ values to send to AddLiquidity and RemoveLiquidity capped
  -- by the amount that genesis address has divied by three
  let xtzSentToAddLiquidity = minMutez (tdXtzSentToAddLiquidity testData) (unsafeMkMutez $ ((unMutez genesisAddressBalance) `div` 3))
      xtzSentToTokenToXtz   = minMutez (tdXtzSentToTokenToXtz testData) (unsafeMkMutez $ ((unMutez genesisAddressBalance) `div` 3))

  -- originate fa with totalSupply all belonging to genesis address
  let faStorage = FA.initStorage genesisAddress (tdTotalSupply testData)
  fa <- lOriginate FA.contract "FA1.2" faStorage zeroMutez
  
  -- originate dexter for the fa
  let dexterStorage = Dexter.initStorage Mock.camlCase $ unTAddress fa
  dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

  -- make the approval amount large so we can test trying to buy too much token
  let approvalAmount =
        case tdAdjustMinXtzBoughtToFail testData of
          Just factor -> tdTotalSupply testData * factor
          Nothing     -> tdTotalSupply testData
  
  -- approve dexter to spend genesis address's tokens
  lCallEPWithMutez
    genesisAddress
    fa
    (Call @"Approve")
    (FA.mkApproveParams (unTAddress dexter) approvalAmount)
    zeroMutez
  
  -- after AddLiquidity: xtzPool is xtzSentToAddLiquidity, lqtTotal is xtzSentToAddLiquidity
  -- and tokenPool is tdMaxTokensDeposited.
  if Prelude.not (tdSkipAddLiquidity testData)
  then
    lCallEPWithMutez
      genesisAddress
      dexter
      (Call @"AddLiquidity")
      (Dexter.mkAddLiquidityParams genesisAddress
        1
        (tdMaxTokensDeposited testData)
        (tdDeadline testData)
      )
      xtzSentToAddLiquidity
  else pure ()

  let xtzPool      = xtzSentToAddLiquidity
      tokensSold   = tdTokensSold testData
      tokenPool    = tdMaxTokensDeposited testData
      minXtzBought =
        case tdAdjustMinXtzBoughtToFail testData of
          Just factor -> unsafeMkMutez $ (unMutez xtzSentToAddLiquidity) * fromIntegral factor
          Nothing     ->
            minMutez (tdMinXtzBought testData)
                     ((unsafeMkMutez . fromIntegral $
                       (tokensSold * 997 * (fromIntegral $ unMutez xtzPool)) `div`
                       (tokenPool * 1000 + tokensSold * 997)))

  lCallEPWithMutez
    genesisAddress
    dexter
    (Call @"TokenToXtz")
    (Dexter.mkTokenToXtzParams
      genesisAddress
      (tdReceiver  testData)
      (tdTokensSold testData)
      minXtzBought
      (tdDeadline  testData)      
    )
    xtzSentToTokenToXtz

  -- calculate tokens bought to update the storage and compare it with the
  -- storage from the env
  let xtzBought =
        (unsafeMkMutez . fromIntegral $
          (tokensSold * 997 * (fromIntegral $ unMutez xtzPool)) `div`
          (tokenPool * 1000 + tokensSold * 997))
      lqtTotal             = fromIntegral $ unMutez xtzSentToAddLiquidity
      dexterUpdatedStorage =
        Dexter.setTokenPool (tokenPool + (tdTokensSold testData)) $
        Dexter.setXtzPool   (unsafeMkMutez $ (unMutez xtzPool) - (unMutez xtzBought)) $
        Dexter.setLqtTotal  lqtTotal $        
        Dexter.insertLiquidityOwner genesisAddress lqtTotal Map.empty $
        dexterStorage

  -- receiver has tokens bought
  -- dexter has tokenPool - tokensBought  
  -- genesis address has totalSupply - tdMaxTokensDeposited, and allowance for dexter
  let faUpdatedStorage =
        FA.insertAccount
          genesisAddress
          (FA.setAccountBalance
            (tdTotalSupply testData - tokenPool - tokensSold) $
           FA.insertAllowance (unTAddress dexter) (tdTotalSupply testData - tokenPool - tokensSold) $
           FA.emptyAccount) $
        FA.insertAccount (unTAddress dexter) (FA.setAccountBalance (tokenPool + tokensSold) $ FA.emptyAccount) $
        faStorage

  pure (dexter, dexterUpdatedStorage, fa, faUpdatedStorage)

-- =============================================================================
-- Test data and generators
-- =============================================================================

data TestData =
  TestData
    { tdTotalSupply              :: Natural -- | total amount of token FA1.2
    , tdOwnerBalance             :: Natural -- | how much the genesisAddress owns of the FA1.2 total supply
    , tdMaxTokensDeposited       :: Natural
    , tdXtzSentToAddLiquidity    :: Mutez
    , tdXtzSentToTokenToXtz      :: Mutez
    , tdReceiver                 :: Address
    , tdTokensSold               :: Natural
    , tdMinXtzBought             :: Mutez
    , tdDeadline                 :: Timestamp
    , tdSkipAddLiquidity         :: Bool   -- | this is to emulate a trade when xtzPool and tokenPool are empty
    , tdAdjustMinXtzBoughtToFail :: Maybe Natural
    } deriving (Show)

genTestData :: Timestamp -> Gen TestData
genTestData now_ = do
  totalSupply           <- choose (10000, 99999999999) :: Gen Integer
  -- only deposit up to half the tokens
  maxTokensDeposited    <- choose (1000, totalSupply `div` 2) :: Gen Integer
  -- this value will be capped by genesisAddress's balance
  xtzSentToAddLiquidity <- Gen.genOneTezOrGreater
  xtzSentToTokenToXtz   <- pure zeroMutez
  receiver              <- Mock.genAddress

  -- only sell a percentage of tokens
  tokensSold            <- choose (100, (totalSupply - maxTokensDeposited) `div` 3) :: Gen Integer
  -- this value will be changed in the testBody because xtzSentToAddLiquidity gets capped
  minXtzBought          <- Gen.genOneTezOrGreater

  deadline              <- timestampFromSeconds <$> choose (timestampToSeconds now_ + 1, timestampToSeconds now_ + 9999999999)

  pure $
    TestData
      (fromIntegral totalSupply)
      (fromIntegral totalSupply)
      (fromIntegral maxTokensDeposited)
      xtzSentToAddLiquidity
      xtzSentToTokenToXtz
      receiver
      (fromIntegral tokensSold)
      minXtzBought
      deadline
      False
      Nothing

-- | run genTestData, but set tdDeadline to a value
-- less than or equal to NOW. This will cause XtzToToken to fail.
genTestDataWithDeadlineLessThanNow :: Timestamp -> Gen TestData
genTestDataWithDeadlineLessThanNow now_ = do
  testData <- genTestData now_
  deadline <- timestampFromSeconds <$> choose (1, timestampToSeconds dummyNow)
  pure $ testData { tdDeadline = deadline }

genTestDataWithMinXtzBoughtZero :: Timestamp -> Gen TestData
genTestDataWithMinXtzBoughtZero now_ = do
  testData <- genTestData now_
  pure $ testData { tdMinXtzBought = zeroMutez }

-- | Has to be updated in the test body because we cannot access genesisAddress
-- balance.
genTestDataWithXtzBoughtLessThanMin :: Timestamp -> Gen TestData
genTestDataWithXtzBoughtLessThanMin now_ = do
  testData        <- genTestData now_
  factor          <- choose (2,5) :: Gen Integer
  pure $ testData { tdAdjustMinXtzBoughtToFail = Just $ fromIntegral factor }

genTestDataWithAmountGreaterThanZero :: Timestamp -> Gen TestData
genTestDataWithAmountGreaterThanZero now_ = do
  testData              <- genTestData now_
  xtzSentToTokenToXtz   <- Gen.genOneTezOrGreater
  pure $ testData { tdXtzSentToTokenToXtz = xtzSentToTokenToXtz }
 
getTestDataWithSkipAddLiquidity :: Timestamp -> Gen TestData
getTestDataWithSkipAddLiquidity now_ = do
  testData        <- genTestData now_
  pure $ testData { tdSkipAddLiquidity = True }
