{-|
Module      : Test.Dexter.Contract.RemoveLiquidity
Copyright   : (c) camlCase, 2020
Maintainer  : james@camlcase.io

Unit and property tests for the dexter%addLiquidity.

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.RemoveLiquidity where

-- general
import qualified Data.Map.Strict as Map
import           Util.Named ((.!))

-- lorentz and morley
import           Lorentz
import           Lorentz.Test
import           Lorentz.Test.Extension
import           Michelson.Interpret  (ceAmount, ceBalance, ceNow, ceSender)
import           Michelson.Test.Dummy (dummyNow)
import           Tezos.Core           (unMutez, unsafeMkMutez, timestampPlusSeconds, timestampToSeconds)

-- testing
import           Test.Hspec      (Spec, it, describe, shouldBe)
import           Test.QuickCheck (Gen, choose, forAll)

-- fa1.2
import qualified FaOnePointTwo.Contract  as FA
import qualified FaOnePointTwo.Test      as FA

-- dexter
import qualified Dexter.Contract           as Dexter
import qualified Dexter.Contract.Test      as Dexter
import qualified Test.Dexter.Contract.Mock as Mock
import qualified Test.Dexter.Contract.Gen  as Gen

-- | spec function for RemoveLiquidity
spec :: (forall s. Dexter.RuntimeValues s) -> Spec
spec runtimeValues = do
  describe "removeLiquidity entrypoint" $ do
    it "removeLiquidityInitialAsserts" $ do
      let runRemoveLiquidityInitialAsserts params storage env = do 
            let initStack = (Identity params :& Identity storage :& RNil)
            resStack <- interpretLorentzInstr env (Dexter.removeLiquidityInitialAsserts runtimeValues) initStack
            let Identity _ :& Identity res :& _ = resStack
            return res

          balance_ = unsafeMkMutez 1000000
          amount_  = unsafeMkMutez 0
          later = dummyNow `timestampPlusSeconds` 100
          env0 = dummyContractEnv { ceAmount = amount_, ceBalance = balance_, ceNow = later }

          params0 = ( #owner .! (#owner .! Mock.alice)
                    , #to .! (#receiver .! Mock.alice)
                    , #lqtBurned .! 100
                    , #minXtzWithdrawn .! unsafeMkMutez 1, #minTokensWithdrawn .! 1
                    , #deadline .! dummyNow)
                    
          env1 = dummyContractEnv { ceAmount = amount_, ceBalance = balance_, ceNow = dummyNow }

          params1 = ( #owner .! (#owner .! Mock.alice)
                    , #to .! (#receiver .! Mock.alice)
                    , #lqtBurned .! 100
                    , #minXtzWithdrawn .! unsafeMkMutez 0, #minTokensWithdrawn .! 1
                    , #deadline .! later)

          params2 = ( #owner .! (#owner .! Mock.alice)
                    , #to .! (#receiver .! Mock.alice)
                    , #lqtBurned .! 100
                    , #minXtzWithdrawn .! unsafeMkMutez 1, #minTokensWithdrawn .! 0
                    , #deadline .! later)                    

          params3 = ( #owner .! (#owner .! Mock.alice)
                    , #to .! (#receiver .! Mock.alice)
                    , #lqtBurned .! 0
                    , #minXtzWithdrawn .! unsafeMkMutez 1, #minTokensWithdrawn .! 1
                    , #deadline .! later)

          params4 = ( #owner .! (#owner .! Mock.alice)
                    , #to .! (#receiver .! Mock.alice)
                    , #lqtBurned .! 100
                    , #minXtzWithdrawn .! unsafeMkMutez 1, #minTokensWithdrawn .! 1
                    , #deadline .! later)                                        

      -- deadline has passed, should fail
      isLeft (runRemoveLiquidityInitialAsserts params0 (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12) env0) `shouldBe` True
      -- minXtzWithdrawn is zero, should fail
      isLeft (runRemoveLiquidityInitialAsserts params1 (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12) env1) `shouldBe` True
      -- minTokensWithdrawn is zero, should fail
      isLeft (runRemoveLiquidityInitialAsserts params2 (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12) env1) `shouldBe` True
      -- lqtBurned is zero, should fail
      isLeft (runRemoveLiquidityInitialAsserts params3 (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12) env1) `shouldBe` True
      -- deadline has not passed
      -- minXtzWithdrawn is greater than zero
      -- minTokensWithdrawn is greater than zero
      -- lqtBurned is greater than zero, should pass
      runRemoveLiquidityInitialAsserts params4 (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12) env1 `shouldBe` Right (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)

    it "getOwnerBalanceAndCheckSenderAllowance" $ do
      let runGetOwnerBalanceAndCheckSenderAllowance params storage env = do 
            let initStack = (Identity params :& Identity storage :& RNil)
            resStack <- interpretLorentzInstr env (Dexter.getOwnerBalanceCheckAndUpdateSenderAllowance runtimeValues) initStack
            let Identity res :& Identity _ :& Identity _ :& _ = resStack
            return res

          balance0 = unsafeMkMutez 1000000
          amount0  = unsafeMkMutez 0
          later = dummyNow `timestampPlusSeconds` 100
          liquidity0 = 0
          liquidity1 = 1000000
          env0 = dummyContractEnv { ceAmount = amount0, ceBalance = balance0, ceNow = later, ceSender = Mock.alice }
          params0 = ( #owner .! (#owner .! Mock.alice)
                    , #to .! (#receiver .! Mock.alice)
                    , #lqtBurned .! 100
                    , #minXtzWithdrawn .! unsafeMkMutez 1, #minTokensWithdrawn .! 1
                    , #deadline .! dummyNow)
          storage0 =
            Dexter.setLqtTotal liquidity1 .
            Dexter.insertLiquidityOwner Mock.alice liquidity0 Map.empty $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)

          storage1 =
            Dexter.setLqtTotal liquidity1 .
            Dexter.insertLiquidityOwner Mock.alice liquidity1 Map.empty $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)

          params1 = Dexter.setLqtBurned 2000000 params0
          params2 = Dexter.setLqtBurned 500000 params0                    

      -- sender is owner
      -- owner's balance zero, fail
      isLeft (runGetOwnerBalanceAndCheckSenderAllowance params0 storage0 env0) `shouldBe` True
      -- owner's balance is less than lqtBurned, fail
      isLeft (runGetOwnerBalanceAndCheckSenderAllowance params1 storage1 env0) `shouldBe` True      
      -- owner's balance is greater than or equal to lqtBurned, pass
      isRight (runGetOwnerBalanceAndCheckSenderAllowance params2 storage1 env0) `shouldBe` True
      -- isRight (runGetOwnerBalanceAndCheckSenderAllowance params3 storage1 env0) `shouldBe` True      
      
      let env1 = dummyContractEnv { ceAmount = amount0, ceBalance = balance0, ceNow = later, ceSender = Mock.bob }
          storage2 =
            Dexter.setLqtTotal liquidity1 .
            Dexter.insertLiquidityOwner Mock.alice liquidity1 (Map.fromList [(Mock.bob, 50)]) $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)

          storage3 =
            Dexter.setLqtTotal liquidity1 .
            Dexter.insertLiquidityOwner Mock.alice liquidity1 (Map.fromList [(Mock.bob, 20000000)]) $
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)
            
      -- sender is not owner
      -- zero approval, fail
      isLeft (runGetOwnerBalanceAndCheckSenderAllowance params2 storage1 env1) `shouldBe` True
      -- allowance less than lqtBurned
      isLeft (runGetOwnerBalanceAndCheckSenderAllowance params2 storage2 env1) `shouldBe` True
      -- lqtBurned greater than owner's balance
      isLeft (runGetOwnerBalanceAndCheckSenderAllowance params1 storage3 env1) `shouldBe` True
      -- lqtBurned is less than or equal to the sender's allowance is less than or equal to their limit and
      -- lqtBalance
      runGetOwnerBalanceAndCheckSenderAllowance params2 storage3 env1 `shouldBe` Right liquidity1

    it "removeLiquidity" $ do
      let runRemoveLiquidity params storage env = do 
            let initStack = (Identity params :& Identity storage :& RNil)
            resStack <- interpretLorentzInstr env (Dexter.removeLiquidityAndPrepOperations runtimeValues) initStack
            let Identity storageOut :& Identity tokensWithdrawn :& Identity xtzWithdrawn :& Identity _ :& _ = resStack
            return (storageOut, tokensWithdrawn, xtzWithdrawn)

          xtzBalance0 = unsafeMkMutez 1000000
          amount0  = unsafeMkMutez 0
          later = dummyNow `timestampPlusSeconds` 100
          liquidity0 = 1000000
          env0 = dummyContractEnv { ceAmount = amount0, ceBalance = xtzBalance0, ceNow = dummyNow, ceSender = Mock.alice }
          params0 = ( #owner .! (#owner .! Mock.alice)
                    , #to .! (#receiver .! Mock.alice)
                    , #lqtBurned .! liquidity0
                    , #minXtzWithdrawn .! unsafeMkMutez 1, #minTokensWithdrawn .! 1
                    , #deadline .! later)

          storage0 =
            Dexter.setLqtTotal liquidity0 .
            Dexter.setTokenPool 100 .
            Dexter.insertLiquidityOwner Mock.alice liquidity0 Map.empty $
            Dexter.setXtzPool (unsafeMkMutez 1000000)
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)            

          storage0' =
            Dexter.setLqtTotal 0 .
            Dexter.setTokenPool 0 .
            Dexter.insertLiquidityOwner Mock.alice 0 Map.empty $
            Dexter.setXtzPool (unsafeMkMutez 0)
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)

          env1 = dummyContractEnv { ceAmount = amount0, ceBalance = unsafeMkMutez 1500000, ceNow = dummyNow, ceSender = Mock.alice }
          params1 = ( #owner .! (#owner .! Mock.alice)
                    , #to .! (#receiver .! Mock.alice)
                    , #lqtBurned .! 200000
                    , #minXtzWithdrawn .! unsafeMkMutez 1, #minTokensWithdrawn .! 1
                    , #deadline .! later)
          
          storage1 =
            Dexter.setLqtTotal 1200000 .
            Dexter.setTokenPool 200 .
            Dexter.insertLiquidityOwner Mock.alice 1200000 Map.empty $
            Dexter.setXtzPool (unsafeMkMutez 1500000)
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)
      
          storage1' =
            Dexter.setLqtTotal 1000000 .
            Dexter.setTokenPool 167 .
            Dexter.insertLiquidityOwner Mock.alice 1000000 Map.empty $
            Dexter.setXtzPool (unsafeMkMutez $ 1500000 - 250000)
            (Dexter.initStorage Mock.camlCase Mock.tezosTacosFA12)
      
      -- burn all liquidity should give back all the tokens and xtz
      runRemoveLiquidity params0 storage0 env0 `shouldBe` Right (storage0', 100, xtzBalance0)

      -- burn half liquidity should give back half the tokens and xtz
      runRemoveLiquidity params1 storage1 env1 `shouldBe` Right (storage1', 33, unsafeMkMutez 250000)

    it "PROP-RL-000: If amount > 0, the operation will fail." $ do
      forAll (genTestDataWithXtzSentToRemoveLiquidity dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testRemoveLiquidity runtimeValues testData `catchExpectedError` lExpectError (== [mt|Amount must be zero.|])

    it "PROP-RL-001: If deadline >= NOW, the operation will fail." $ do
      forAll (genTestDataWithDeadlineLessThanNow dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testRemoveLiquidity runtimeValues testData `catchExpectedError` lExpectError (== [mt|NOW is greater than deadline.|])

    it "PROP-RL-002: If min_xtz_withdrawn == 0, the operation will fail." $ do
      forAll (genTestDataWithMinXtzWithdrawnAsZero dummyNow) $ \testData ->
        integrationalTestProperty $ 
          void $ testRemoveLiquidity runtimeValues testData `catchExpectedError` lExpectError (== [mt|minXtzWithdrawn must be greater than zero.|])

    it "PROP-RL-003: If min_token_withdrawn == 0, the operation will fail." $ do
      forAll (genTestDataWithMinTokensWithdrawnAsZero dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testRemoveLiquidity runtimeValues testData `catchExpectedError` lExpectError (== [mt|minTokensWithdrawn must be greater than zero.|])

    it "PROP-RL-004: If xtz_withdrawn < min_xtz_withdrawn, the operation will fail." $ do
      forAll (genTestDataWithMinXtzWithdrawnTooLarge dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testRemoveLiquidity runtimeValues testData `catchExpectedError` lExpectError (== [mt|xtzWithdrawn is less than minXtzWithdrawn.|])

    it "PROP-RL-005: If token_withdraw < min_token_withdrawn is zero, the operation will fail." $ do
      forAll (genTestDataWithMinTokensWithdrawnTooLarge dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testRemoveLiquidity runtimeValues testData `catchExpectedError` lExpectError (== [mt|tokensWithdrawn is less than minTokensWithdrawn.|])

    it "PROP-RL-006: If lqt_burned > accounts[owner].balance, the operation will fail." $ do
      forAll (genTestDataWithLqtBurnedTooLarge dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testRemoveLiquidity runtimeValues testData `catchExpectedError` lExpectError (== [mt|lqtBurned is greater than owner's balance.|])

    it "PROP-RL-007: If sender != owner and lqt_burned > account[owner].allowances[sender], the operation will fail." $ do
      forAll (genThirdPartyBurnFail dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testRemoveLiquidity runtimeValues testData `catchExpectedError` lExpectError (== [mt|sender approval balance is less than LQT burned.|])

    it "PROP-RL-008: If to or its provided entry point do not have parameter of type unit, this operation will fail." $ do
      forAll ((,) <$> (genTestData dummyNow) <*> Mock.genArbAddress) $ \(testData, arbAddress) ->
        integrationalTestProperty $ do
          case arbAddress of
            Mock.Token -> do
              let storage = FA.initStorage Mock.alice 1000000            
              fa <- lOriginate FA.contract "FA1.2-1" storage zeroMutez
              void $ testRemoveLiquiditySendToAddress runtimeValues testData (unTAddress fa)
                `catchExpectedError` lExpectUnspecifiedFailWith

            Mock.Address ->
              void $ testRemoveLiquiditySendToAddress runtimeValues testData genesisAddress
                      
            Mock.Dummy   -> do
              dummy <- lOriginate dummyContract "Dummy" () zeroMutez
              void $ testRemoveLiquiditySendToAddress runtimeValues testData (unTAddress dummy)          
        
    it "PROP-RL-009: For any deadline value less than NOW, min_xtz_withdrawn greater than zero, min_tokens_withdrawn greater than zero, xtz_withdrawn greater than or equal to min_xtz_withdrawn, tokens_withdrawn greater than or equal to min_tokens_withdrawn, lqt_burned less than or equal to accounts[owner].balance, if to has a parameter of unit (or is an entrypoint with parameter unit), tokens_withdrawn will be given to to via the token_address contract and xtz_withdrawn will be sent to to." $ do
      forAll (genTestData dummyNow) $ \testData ->
        integrationalTestProperty $
          void $ testRemoveLiquidity runtimeValues testData

    it "PROP-RL-010: Assume PROP-RL-009, then storage will be updated such that lqt_total -= lqt_burned, xtz_pool -= xtz_withdrawn, token_pool -= tokens_withdrawn, and accounts[owner].balance -= lqt_burned." $ do
      forAll (genTestData dummyNow) $ \testData ->
        integrationalTestProperty $ do
          (dexter, dexterStorage) <- testRemoveLiquidity runtimeValues testData
          lExpectStorageConst dexter dexterStorage

-- =============================================================================
-- Test functions
-- =============================================================================

-- | 1. originate FA1.2, totalSupply given to genesisAddress
--   2. originate Dexter
--   3. Approve Dexter to spend genesisAddress's FA1.2
--   4. AddLiquidity to Dexter so it has non-zero values
--   5. RemoveLiquidity
testRemoveLiquiditySendToAddress
  :: (forall s. Dexter.RuntimeValues s)
  -> TestData
  -> Address
  -> IntegrationalScenarioM (TAddress Dexter.Parameter, Dexter.Storage)
testRemoveLiquiditySendToAddress runtimeValues testData receiver = do
  -- set current time to dummyNow
  setNow dummyNow

  -- get the balance of the genesis address
  genesisAddressBalance <- getGenesisAddressBalance

  -- get the XTZ values to send to AddLiquidity and RemoveLiquidity capped
  -- by the amount that genesis address has divied by three
  let xtzSentToAddLiquidity    = minMutez (tdXtzSentToAddLiquidity testData) (unsafeMkMutez $ ((unMutez genesisAddressBalance) `div` 3))
      xtzSentToRemoveLiquidity = minMutez (tdXtzSentToRemoveLiquidity testData) (unsafeMkMutez $ ((unMutez genesisAddressBalance) `div` 3))  

  -- originate fa with totalSupply all belonging to genesis address
  let faStorage = FA.initStorage genesisAddress (tdTotalSupply testData)
  fa <- lOriginate FA.contract "FA1.2" faStorage zeroMutez
  
  -- originate dexter for the fa
  let dexterStorage = Dexter.initStorage Mock.camlCase $ unTAddress fa
  dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

  -- approve dexter to spend genesis address's tokens
  lCallEPWithMutez
    genesisAddress
    fa
    (Call @"Approve")
    (FA.mkApproveParams (unTAddress dexter) (tdTotalSupply testData))
    zeroMutez

  
  -- after AddLiquidity: xtzPool is xtzSentToAddLiquidity, lqtTotal is xtzSentToAddLiquidity
  -- and tokenPool is tdMaxTokensDeposited.
  lCallEPWithMutez
    genesisAddress
    dexter
    (Call @"AddLiquidity")
    (Dexter.mkAddLiquidityParams genesisAddress
      1
      (tdMaxTokensDeposited testData)
      (tdDeadline testData)
    )
    xtzSentToAddLiquidity

  -- AddLiquidity when lqtTotal sets lqtTotal equal to amount of XTZ sent
  let lqtTotal  = fromIntegral $ unMutez xtzSentToAddLiquidity
  let xtzPool   = xtzSentToAddLiquidity
  let tokenPool = tdMaxTokensDeposited testData
  
  -- approve alice to burn genesis addresses dexter burn genesis address's liquidity
  case tdThirdPartyBurn testData of
    Just (approveBurn_, _) -> do
      let approveBurn =
            min approveBurn_ (lqtTotal `div` 2)
      lCallEPWithMutez
        genesisAddress
        dexter
        (Call @"Approve")
        (Dexter.mkApproveParams Mock.alice approveBurn 0)
        zeroMutez
    Nothing -> pure () 
  
  -- cap lqtBurned by the lqtTotal
  let lqtBurned          =
        case tdThirdPartyBurn testData of
          -- third party sender where we want to force a fail with a burn larger than approval amount
          Just (approvedBurn, unapprovedAmount) -> min (approvedBurn + unapprovedAmount) lqtTotal
          Nothing ->
            case tdFactorLqtBurnedBy testData of
              Just factor -> factor * lqtTotal
              Nothing -> min (tdLqtBurned testData) lqtTotal

  let xtzWithdrawn   =
        unsafeMkMutez . fromIntegral $ lqtBurned * (fromIntegral $ unMutez xtzPool) `div` lqtTotal
  -- cap by the amount of XTZ the owner has in Dexter (lqtBurned * xtzPool / lqtTotal)
  let minXtzWithdrawn    =
        case tdFactorMinXtzWithdrawnBy testData of
          Just factor -> unsafeMkMutez $ (unMutez xtzSentToAddLiquidity) * (unMutez factor)
          Nothing ->
            minMutez (tdMinXtzWithdrawn testData) $ xtzWithdrawn

  let tokensWithdrawn =
        lqtBurned * (tdMaxTokensDeposited testData) `div` (fromIntegral $ unMutez xtzSentToAddLiquidity)
  -- cap by the amount of token the owner has in Dexter (lqtBurned * tokenPool / lqtTotal)
  let minTokensWithdrawn =
        case tdFactorMinTokensWithdrawnBy testData of
          Just factor ->
            factor * (tdMaxTokensDeposited testData)
          Nothing ->          
            min (tdMinTokensWithdrawn testData) tokensWithdrawn

  -- send with minLqtMinted
  lCallEPWithMutez
    (case tdThirdPartyBurn testData of
       Just _ -> Mock.alice
       Nothing -> genesisAddress
    )    
    dexter
    (Call @"RemoveLiquidity")
    (Dexter.mkRemoveLiquidityParams
      genesisAddress
      receiver
      lqtBurned
      minXtzWithdrawn
      minTokensWithdrawn
      (tdDeadline testData))
    xtzSentToRemoveLiquidity

  let dexterUpdatedStorage =
        Dexter.setLqtTotal  (lqtTotal - lqtBurned) $
        Dexter.setXtzPool   (unsafeMkMutez $ (unMutez xtzPool) - (unMutez xtzWithdrawn)) $
        Dexter.setTokenPool (tokenPool - tokensWithdrawn) $
        Dexter.insertLiquidityOwner genesisAddress (lqtTotal - lqtBurned) Map.empty $
        dexterStorage
        
  pure (dexter, dexterUpdatedStorage)

-- | testRemoveLiquiditySendToAddress with genesisAddress as the receiver
testRemoveLiquidity
  :: (forall s. Dexter.RuntimeValues s)
  -> TestData
  -> IntegrationalScenarioM (TAddress Dexter.Parameter, Dexter.Storage)
testRemoveLiquidity runtimeValues testData =
  testRemoveLiquiditySendToAddress runtimeValues testData genesisAddress

-- =============================================================================
-- Test data and generators
-- =============================================================================

-- | Data generated by QuickCheck in order to originate necessary contracts
-- and execute preliminary in order to call and test dexter%addLiquidity.
data TestData =
  TestData
    { tdTotalSupply                :: Natural -- | total amount of token FA1.2
    , tdOwnerBalance               :: Natural -- | how much the genesisAddress owns of the FA1.2 total supply
    , tdDeadline                   :: Timestamp -- | deadline for dexter calls
    , tdMaxTokensDeposited         :: Natural -- | amount of tokens deposited in initial AddLiquidity
    , tdXtzSentToAddLiquidity      :: Mutez -- | how much Xtz to send to AddLiquidity, should be capped by genesisAddress's XTZ
    , tdXtzSentToRemoveLiquidity   :: Mutez -- | how much Xtz to send to RemoveLiquidity, should be capped by genesisAddress's XTZ
    , tdLqtBurned                  :: Natural -- | how much genesisAddress will burn, should be capped by owner's LQT
    , tdMinXtzWithdrawn            :: Mutez -- | cap by the amount of XTZ the owner has in Dexter (lqtBurned * xtzPool / lqtTotal)
    , tdMinTokensWithdrawn         :: Natural -- | cap by the amount of Token the owner has in Dexter (lqtBurned * tokenPool / lqtTotal)
    , tdFactorMinXtzWithdrawnBy    :: Maybe Mutez -- | the test function should make tdMinXtzWithdrawn larger than what can be withdrawn
    , tdFactorMinTokensWithdrawnBy :: Maybe Natural -- | the test function should make tdMinTokensWithdrawn larger than what can be withdrawn
    , tdFactorLqtBurnedBy          :: Maybe Natural
    , tdThirdPartyBurn             :: Maybe (Natural, Natural) -- | if Just then run dexter%approve for alice and alice runs RemoveLiquidity
    } deriving (Show)

-- | generate TestData for RemoveLiquidity that will cause the entrypoint to be
-- succesfully called
genTestData :: Timestamp -> Gen TestData
genTestData now_ = do
  totalSupply       <- (choose (10000, 99999999999) :: Gen Integer)
  deadline          <- timestampFromSeconds <$> choose (timestampToSeconds now_ + 1, timestampToSeconds now_ + 9999999999)
  xtzSentToAddLiquidity  <- Gen.genOneTezOrGreater
  maxTokensDeposited <- fromIntegral <$> choose (1000, totalSupply)
  lqtBurned         <- fromIntegral <$> (choose (1,  fromIntegral $ unMutez xtzSentToAddLiquidity) :: Gen Integer)
  minXtzWithdrawn   <- unsafeMkMutez <$> choose (1,  99999999999) 
  minTokensWithdrawn <- fromIntegral <$> (choose (1,  99999999999) :: Gen Integer)  

  pure $
    TestData
      (fromIntegral totalSupply)
      (fromIntegral totalSupply)
      deadline
      maxTokensDeposited      
      xtzSentToAddLiquidity
      zeroMutez
      lqtBurned
      minXtzWithdrawn
      minTokensWithdrawn
      Nothing
      Nothing
      Nothing
      Nothing

-- | run genTestData, but set tdXtzSentToRemoveLiquidity to a value greater
-- than zero. This will cause RemoveLiquidity to fail.
genTestDataWithXtzSentToRemoveLiquidity :: Timestamp -> Gen TestData
genTestDataWithXtzSentToRemoveLiquidity now_ = do
  testData <- genTestData now_
  xtzSent  <- Gen.genOneTezOrGreater
  pure $ testData { tdXtzSentToRemoveLiquidity = xtzSent }

-- | run genTestData, but set tdDeadline to a value
-- less than or equal to NOW. This will cause RemoveLiquidity to fail.
genTestDataWithDeadlineLessThanNow :: Timestamp -> Gen TestData
genTestDataWithDeadlineLessThanNow now_ = do
  testData <- genTestData now_
  deadline <- timestampFromSeconds <$> choose (1, timestampToSeconds dummyNow)
  pure $ testData { tdDeadline = deadline }

-- | run genTestData, but set tdMinXtzWithdrawn to zero.
-- This will cause RemoveLiquidity to fail.
genTestDataWithMinXtzWithdrawnAsZero :: Timestamp -> Gen TestData
genTestDataWithMinXtzWithdrawnAsZero now_ = do
  testData <- genTestData now_
  pure $ testData { tdMinXtzWithdrawn = zeroMutez }

-- | run genTestData, but set tdMinTokenWithdrawn to zero.
-- This will cause RemoveLiquidity to fail.
genTestDataWithMinTokensWithdrawnAsZero :: Timestamp -> Gen TestData
genTestDataWithMinTokensWithdrawnAsZero now_ = do
  testData <- genTestData now_
  pure $ testData { tdMinTokensWithdrawn = 0 }

-- | run genTestData, but set tdFactorMinXtzWithdrawnBy to greater than one.
-- This will cause RemoveLiquidity to fail.
genTestDataWithMinXtzWithdrawnTooLarge :: Timestamp -> Gen TestData
genTestDataWithMinXtzWithdrawnTooLarge now_ = do
  testData <- genTestData now_
  factor   <- unsafeMkMutez <$> choose (2,5)
  pure $ testData { tdFactorMinXtzWithdrawnBy = Just factor }

-- | run genTestData, but set tdFactorMinTokensWithdrawnBy to greater than one.
-- This will cause RemoveLiquidity to fail.
genTestDataWithMinTokensWithdrawnTooLarge :: Timestamp -> Gen TestData
genTestDataWithMinTokensWithdrawnTooLarge now_ = do
  testData <- genTestData now_
  factor   <- fromIntegral <$> (choose (2,5) :: Gen Integer)
  pure $ testData { tdFactorMinTokensWithdrawnBy = Just factor }

-- | run genTestData, but set tdFactorMinTokensWithdrawnBy to greater than one.
-- This will cause RemoveLiquidity to fail.
genTestDataWithLqtBurnedTooLarge :: Timestamp -> Gen TestData
genTestDataWithLqtBurnedTooLarge now_ = do
  testData <- genTestData now_
  factor   <- fromIntegral <$> (choose (2,5) :: Gen Integer)
  pure $ testData { tdFactorLqtBurnedBy = Just factor }

genThirdPartyBurnFail :: Timestamp -> Gen TestData
genThirdPartyBurnFail now_ = do
  testData    <- genTestData now_
  approve     <- choose (1000, unMutez $ tdXtzSentToAddLiquidity testData)
  unapproved  <- choose (1, unMutez $ tdXtzSentToAddLiquidity testData)
  pure $ testData { tdThirdPartyBurn = Just (fromIntegral approve, fromIntegral unapproved) }
