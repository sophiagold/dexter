{-|
Module      : Test.Lorentz.Extension
Copyright   : (c) camlCase, 2020
Maintainer  : james@camlcase.io

Add more testing functions for the morley lorentz library.

-}

{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RecordWildCards           #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Lorentz.Test.Extension where

import qualified Control.Lens           as Lens
import qualified Data.Map as Map
import           Lorentz hiding (get)
import           Lorentz.Test
import           Michelson.Test.Integrational (isGState)
import           Michelson.Runtime (ExecutorError, ExecutorError'(EEInterpreterFailed))
import           Michelson.Runtime.GState (GState(..), AddressState(..))
import           Prelude hiding (drop)
import           Tezos.Core (unMutez)
import           Util.Named ((.!))

-- =============================================================================
-- Contracts
-- =============================================================================

-- | A contract that does nothing other than receive XTZ.
dummyContract :: Contract () ()
dummyContract = defaultContract $
  drop # unit # nil # pair

-- =============================================================================
-- Call contracts
-- =============================================================================

-- | Call an entrypoint from an address and with an amount of Mutez of your
-- choice.
lCallEPWithMutez
  :: forall cp epRef epArg toAddr.
     (HasEntryPointArg cp epRef epArg, IsoValue epArg, ToTAddress cp toAddr)
  => Address -> toAddr -> epRef -> epArg -> Mutez -> IntegrationalScenarioM ()
lCallEPWithMutez fromAddr toAddr epRef param mutez =
  lTransfer @cp @epRef @epArg
    (#from .! fromAddr) (#to .! toAddr)
    mutez epRef param

-- | Call the default entrypoint from an address and with an amount of Mutez of your
-- choice.
lCallDefWithMutez
  :: forall cp defEpName defArg toAddr.
     ( HasDefEntryPointArg cp defEpName defArg
     , IsoValue defArg
     , ToTAddress cp toAddr
     )
  => Address -> toAddr -> defArg -> Mutez -> IntegrationalScenarioM ()
lCallDefWithMutez fromAddr toAddr mutez =
  lCallEPWithMutez @cp @defEpName @defArg fromAddr toAddr CallDefault mutez

-- =============================================================================
-- Global state of the emulated blockchain
-- =============================================================================

-- | Get the XTZ balance of an Address.
getGStateBalance :: Address -> GState -> Maybe Mutez
getGStateBalance addr gstate =
  case Map.lookup addr (gsAddresses gstate) of
    Just (ASSimple m) -> Just m
    _ -> Nothing

getGenesisAddressBalance :: IntegrationalScenarioM Mutez
getGenesisAddressBalance = do
  gs <- get 
  let gstate = gs Lens.^. isGState
  case getGStateBalance genesisAddress gstate of
    Just genesisAddressBalance -> pure genesisAddressBalance
    Nothing -> integrationalFail . CustomTestError $ "genesisAddress does not exist in gState."        

-- =============================================================================
-- Test expected behavior
-- =============================================================================

-- | Pass if FAILWITH is encountered. It does not try to match the FAILWITH type
-- or returned value.
lExpectUnspecifiedFailWith :: ExecutorError -> IntegrationalScenario
lExpectUnspecifiedFailWith err =
  case err of
    EEInterpreterFailed _ _ -> pass
    _ -> unexpectedInterpreterError err "expected runtime failure with `FAILWITH`"

-- =============================================================================
-- Mutez
-- =============================================================================

minMutez :: Mutez -> Mutez -> Mutez
minMutez x y =
  if (unMutez x) < (unMutez y) then x else y
